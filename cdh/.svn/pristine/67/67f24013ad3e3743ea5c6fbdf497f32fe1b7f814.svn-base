
==== model/*.js

depends on:

provides:

*	model_helper_*
*	ContentTree.*
*	ContentNode.*

reference:

*	Note

**	Private methods (which can nonetheless be called from anywhere in JavaScript) are
	documented for comprehension only and should not be called unless you know what you
	are doing!

*	Class ContentTree

**	Description:
***	The content tree is a collection of nodes that can be named or unnamed. Named nodes
	are accessible by their name and unnamed nodes are accessible via an ordered array.
	
	Every node has a static unique identifier and several transaction information which may
	change when the object gets modified.

**	Constructor:
***	ContentTree()

**	Public methods:
***	_ update() _:
	Every time elements of the object change, the update method must be called. This makes
	sure, that every parent of that element gets informed about the change. All elements with
	no parent are considered to be candidates for being monitored for changes.
***	_ Array: getChildren() _
***	_ String: getGuid() _
***	_ DateTime: getTimestamp() _
***	_ String: getTransactionID() _
***	_ String: getUserID() _
***	_ Array: toList() _: creates an ordered flat list of all unnamed children
***	_ addChild(Object child, Object position) _:
	Adds the child element to the array of unnamed children. If a position is specified,
	the element will be inserted to that particular position. Position can be specified
	as position.prev (element which should precede) or position.next (element which should
	follow) or both (whatever comes first). After insertion, the parent attribute of the
	child object is properly set.
***	_ removeChild(Object child) _

**	Private methods:
***	_  map(Object object, Object parent) _:
	This function is being used as a callback in conjunction with the Mapper class defined in
	the database library. See the documentation there.

*	Class ContentNode

**	Description:
***	Defines a node of the content tree. Inherits all attributes and methods from its parent
	class. Additionally it has a parent attribute.

**	Constructor:
***	ContentNode()

**	Public methods:
***	_ getParent() _
***	_ setParent() _
