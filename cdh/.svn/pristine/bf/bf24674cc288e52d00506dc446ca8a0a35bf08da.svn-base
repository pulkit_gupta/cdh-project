var letter_page_aspect = Math.SQRT1_2;
var letter_pages_canvas_id = "letter_pages";
var letter_wrapper_id = "letter_pages_wrapper";
var letter_pages_canvas;
var $page_canvas;
var $letter_wrapper;
var DocumentLayoutManager = new DocumentLayoutManager();

var letterDefaultWidth = 0;
var letterTextColor;
var letterFont;
var letterGradient = [];
var letterBgColor;
var letterBgMode; // gradient or color

// warning: ugly
var helper_letter_pseudocss = function()
{
	var $pseudo = $("<div id='letter_page'></div>").addClass("__pseudo__").hide();
	var bgc;
	var bg; // matches for example (123, 123, 123) 100%
	var patternfull = /\([0-9]+\s*,\s*[0-9]+\s*,\s*[0-9]+\s*\)\s*[0-9]+%/gi;
	var matchesfull;

	// append to body so css has an effect
	$('body').append($pseudo);

	// read the properties needed
	bg = $pseudo.css("background-image");
	bgc = $pseudo.css("background-color");
	letterTextColor = $pseudo.css("color");
	letterFont = $pseudo.css("font-style") + " " + $pseudo.css("font-size") + " " + $pseudo.css("font-family");

	// immediately remove it just because it was just a workaround
	$pseudo.remove();

	if (bg.indexOf("linear-gradient") === -1) {
		letterBgMode = "color";
		letterBgColor = bgc;
	} else {
		letterBgMode = "gradient";
		matchesfull = bg.match(patternfull);
		for (i = 0; i < matchesfull.length; i++) {
			// we could be sure that the syntax is ok, so we just match the 4 nums
			var values = matchesfull[i].match(/[0-9]+/g);
			letterGradient.push([values[3] / 100, "rgb(" + values[0] + "," + values[1] + "," + values[2] + ")"]);
		}
	}
};

var page_letter_resize = function()
{
	$letter_wrapper.height($(window).height() - $("body>header").height() - 4);
	letter_pages_canvas.setHeight(Math.round(letter_pages_canvas.getWidth() / letter_page_aspect) * DocumentLayoutManager.getPageCount());
	if (!letterDefaultWidth) letterDefaultWidth = letter_pages_canvas.getWidth();
};

var page_letter_redraw = function()
{
	$page_canvas = $("canvas#" + letter_pages_canvas_id);
	letterDefaultWidth = $page_canvas.width();
	letter_pages_canvas.setWidth(letterDefaultWidth);
	$(window).resize(page_letter_resize);
	page_letter_resize();
	DocumentLayoutManager.doLayout();
};

var page_letter_init_document = function()
{
	// set document key
	var documentID = "{44DE0BB2-1328-43B2-8331-40B138BFE049}";
	// build query
	var documentQuery = new Query("document_get", {
		parameters: { filterByKey: documentID },
		onSuccess: function(result) {
			var doc = result.children[0];
			// set as document
			DocumentLayoutManager.setDocument(doc);
			// setup sync map
			var syncMap = {
				"root": { contentContainer : doc, lastSynchroTimeStamp : doc.getTimeStamp() },
				"header": { contentContainer : doc.getHeader(), lastSynchroTimeStamp : doc.getHeader().getTimeStamp() },
				"body": { contentContainer : doc.getBody(), lastSynchroTimeStamp : doc.getBody().getTimeStamp() },
				"footer": { contentContainer : doc.getFooter(), lastSynchroTimeStamp : doc.getFooter().getTimeStamp() }
			};
			// setup sync Queue
			var syncQueue = new Queue(4);
			// setup sync handler
			var syncHandler = function(type, data, timeStamp) {
				var payload = {};
				if (type == "root") {
					var e; for (e in data.contentContainer) {
						if (syncMap[e]===undefined) {
							payload[e] = data.contentContainer[e];
						}
					}
				}
				else {
					payload[type] = data.contentContainer;
				}
				var query = new Query("document_sync", {
					type: "PUT",
					parameters: {
						filterByKey: doc._id,
						payload: payload
					},
					onSuccess: function(result) {
						data.lastSynchroTimeStamp = timeStamp;
					},
					queue: syncQueue
				});
				Database.executeQuery(query);
			};
			// setup sync monitor
			window.setInterval(function() {
				var e; for (e in syncMap) {
					var timeStamp = syncMap[e].contentContainer.getTimeStamp();
					if (timeStamp>syncMap[e].lastSynchroTimeStamp) {
						syncHandler(e, syncMap[e], timeStamp);
						//syncMap[e].lastSynchroTimeStamp = timeStamp;
					}
				}
			}, 1000);
			// start sync queue
			syncQueue.start();
			// do layout
			page_letter_init_layout();
		}
	});
	// load offline result in cache for debugging purposes
	var offlineResult = new ContentTree();
	offlineResult.children[0] = TestDocument();
	offlineResult.children[0].guid = documentID;
	offlineResult.children[0]._id = "Letter/429778782576";
	// Database.cache.write(documentQuery.getHash(), offlineResult, false);
	// execute query
	Database.executeQuery(documentQuery);
};

var page_letter_init_layout = function()
{
	etch.config.buttonClasses.letter = ['fontname', 'fontsize', 'fontcolor', 'bold', 'italic', 'underline', 'justify-left', 'justify-center', 'justify-right', 'clear-formatting'];
	helper_letter_pseudocss();
	$page_canvas = $("canvas#" + letter_pages_canvas_id);
	$letter_wrapper = $("div#" + letter_wrapper_id);
	$letter_wrapper.width($(window).width());
	letterDefaultWidth = $page_canvas.width();
	letter_pages_canvas = new fabric.Canvas(letter_pages_canvas_id);
	letter_pages_canvas.setWidth(letterDefaultWidth);
	$(window).resize(page_letter_resize);
	page_letter_resize();
	$(document).keypress(function(e)
	{
		if (e.ctrlKey) {
			if (e.which === 44) {
				letter_pages_canvas.setWidth(letter_pages_canvas.getWidth() * 0.9);
				page_letter_resize();
			} else if (e.which === 46) {
				letter_pages_canvas.setWidth(letter_pages_canvas.getWidth() / 0.9);
				page_letter_resize();
			} else if (e.which === 48) {
				letter_pages_canvas.setWidth(letterDefaultWidth);
				page_letter_resize();
			}
		}
	});
	DocumentLayoutManager.doLayout();
	loadutils.overlayHide();
};

var page_letter_init = function()
{
	page_letter_init_document();
};