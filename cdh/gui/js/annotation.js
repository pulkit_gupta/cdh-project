
var lastvalue=0;
var id=0;
var noteid=0;

/// Änderungen sofort speichern. Was passiert, wenn 2 Leute gleichzeitig daran arbeiten?
var widget_annotation_textbox_change=function()
{
    if (noteid>0 && id!=0 && document.getElementById("annotation_text").value.length>0)
    {
        Database.query("annotation_save", {"id": id, "text": document.getElementById("annotation_text").value, "noteid":noteid} , function (e) 
        { 
            //console.log(e);
        });
    }
    else
    {
        Database.query("annotation_save", {"id": id, "text": "", "noteid":noteid} , function (e) 
        { 
            //console.log(e);
        });
    }
}

var widget_nav_bind=function()
{

}

var widget_annotation_init=function(nid)
{
    $().ready(function() {
           
        /// load text into the textbox 
        Database.query("annotations",{"noteid":nid}, function (e) { 
            if (e.children.length>0)
            {
                if (e.children[0].text.length<1)
                    document.getElementById("annotation_text").value="";
                else
                    document.getElementById("annotation_text").value=e.children[0].text;
                id=e.children[0]._id;
                noteid=e.children[0].noteid;
            }
            else
            {
                Database.query("annotation_create",{"noteid":nid, "text": ""}, function (e) { 
                    id=e.children[0]._id;
                    noteid=e.children[0].noteid; 
                });
                
            }
        });
        /// onblur of the textbox its value will be uploaded to arangodb
        $("#annotation_text").change( widget_annotation_textbox_change );
    });

    
    return;
}
