/*
 * the clientoverview page, which provides an overviewlist with the clients and basic data
 *
 * clientlist_loadContent:
 *	load the data into the widget
 *
 * clientlist_loadHead:
 *  load the head data of the client
 *
 * clientlist_create:
 *  create a new clientlist from a general overviewlist
 *
 * deliverynotelist_loadContent:
 *  load the deliverynote data of the client
 *
 * deliverynotelist_loadHead:
 *  load the deliverynote head data of the client
 *
 * deliverynotelist_create:
 *  create a new deliverynotelist from a general overviewlist
 *
 * invoicelist_loadContent:
 *  load the invoice data of the client
 *
 * invoicelist_loadHead:
 *  load the invoice head data of the client
 *
 * invoicelist_create:
 *  create a new invoicelist from a general overviewlist
 *
 * quotelist_loadContent:
 *  load the quote data of the client
 *
 * quotelist_loadHead:
 *  load the quote head data of the client
 *
 * quotelist_create:
 *  create a new quotelist from a general overviewlist
 *
 * list_elementSelectFunction:
 *  function called if a list entry in the overviewlist is selected
 *
 * list_elementUnselectFunction:
 *  function called if a list entry in the overviewlist is unselected
 *
 * tab_load:
 *  load a tab
 *
 * tab_insertContent:
 *  display the content of a tab
 *
 * tab_show:
 *  show a tab
 *
 * showCurrentSelected:
 *  defines actions to perform after selecting an entry
 *
 * filterList:
 *  filter the clientlist
 *
 * navigationsettings:
 *	configure the navigation bar -> add page specific functionality to buttons and add specific buttons
 *
 * resize:
 *  resizes widgets on window-resize-event  
 *
 * init:
 *	the init function which should be called by the loader
 */

var page_clientoverview_client_list = null;
var page_clientoverview_quote_list = null;
var page_clientoverview_invoice_list = null;
var page_clientoverview_deliverynote_list = null;
var page_clientoverview_tab_loadedTabs = null;

/*
 * clientlist_loadContent:
 *  - method to load the data into the widget
 */
var page_clientoverview_clientlist_loadContent = function()
{
	//TODO load from database	
	
	var daten = new Array();
	for (var i=0; i < overviewlist_demodata_db.length; i++)
	{
		// create a data object which is filled with demodata
		var tmpData = 
			{
				adress : overviewlist_demodata_db[i].mainadress.adress,
				category : overviewlist_demodata_db[i].category.name,
				city : overviewlist_demodata_db[i].mainadress.city,
				clientnumber : overviewlist_demodata_db[i].clientnumber,
				companyname : overviewlist_demodata_db[i].mainadress.companyname,
				country : overviewlist_demodata_db[i].mainadress.country,
				nameaffix: overviewlist_demodata_db[i].mainadress.naeaffix,
				email : overviewlist_demodata_db[i].email,
				fax : overviewlist_demodata_db[i].fax,
				identifier : overviewlist_demodata_db[i]._id,
				telephone : overviewlist_demodata_db[i].telephone,
				mobilephone : overviewlist_demodata_db[i].mobilephone,
				website : overviewlist_demodata_db[i].website,
				zipcode : overviewlist_demodata_db[i].mainadress.zipcode,
				comment : overviewlist_demodata_db[i].comment,
				clienttyp : overviewlist_demodata_db[i].clienttyp,
				logopath : overviewlist_demodata_db[i].logopath
			}
			
			if (overviewlist_demodata_db[i].maincontact != null)
			{
				tmpData["maincontact_name"] = overviewlist_demodata_db[i].maincontact.salutation.name + " " + overviewlist_demodata_db[i].maincontact.lastname;
				tmpData["maincontact_identifier"] = overviewlist_demodata_db[i].maincontact.contactid;
			}	
			daten.push(tmpData);
	};
	page_clientoverview_client_list.insertDataArray(daten, {callback: null});
	

	// fetch the data from the database if possible and insert it
	if(!Database.getStatus())
	{
		//page_clientoverview_client_list.insertDataArray(daten, {callback:null});
		loadutils.overlayHide();
	}
	else
	{
		/*Database.query("clientoverview", {}, function(data)
		{
			page_clientoverview_client_list.insertDataArray(data, {callback:null});*/
			loadutils.overlayHide();
			//page_clientoverview_client_list.insertDataArray(data, {callback:null});
		
	}

	/*
	Database.query("masterdata", {}, function(data)
	{
		console.log(data)
		page_clientoverview_client_list.insertDataArray(data, {callback:null});
	});
	*/
	/*Database.query("customer_main", { 
		
		}, 
	function(data)
	{
		///console.log("page_clientoverview_init")
		console.log(data);
		page_clientoverview_client_list.insertDataArray(data, {callback:null});
	});*/
	
}

/*
 * clientlist_loadHead:
 *  - method to load the head data of the client
 */
var page_clientoverview_clientlist_loadHead = function()
{
	//TODO load from database
	page_clientoverview_client_list.insertHeadLine(overviewlist_demodata_client_head, {callback:page_clientoverview_clientlist_loadContent});
}

/*
 * clientlist_create:
 *  - method to create a new clientlist from a general overviewlist
 */
var page_clientoverview_clientlist_create = function()
{
		console.log("page_clientoverview_clientlist_create")
		page_clientoverview_client_list= new overviewlist(
		{
			target:"#clientoverview_list_area > div.overviewlist_wrapper",
			resizable:true,
			selectFirst:false,
			autoresize:false,
			sortable:true,
			listElemSelectFunction:page_clientoverview_list_elementSelectFunction,
			listElemSelectFunctionArgs:null,
			listElemUnselectFunction:page_clientoverview_list_elementUnselectFunction,
			listElemUnselectFunctionArgs:null
		}
	);
	page_clientoverview_clientlist_loadHead();
}

/*
 * deliverynotelist_loadContent:
 *  - method to load the deliverynote data of the client
 */
var page_clientoverview_deliverynotelist_loadContent = function()
{
	//TODO load from database
	page_clientoverview_deliverynote_list.insertDataArray(overviewlist_demodata_deliverynote_content, {callback:null});
}

/*
 * deliverynotelist_loadHead:
 *  - method to load the deliverynote head data of the client
 */
var page_clientoverview_deliverynotelist_loadHead = function()
{
	//TODO load from database
	page_clientoverview_deliverynote_list.insertHeadLine(overviewlist_demodata_deliverynote_head, {callback:page_clientoverview_deliverynotelist_loadContent});
}

/*
 * deliverynotelist_create:
 *  - method to create a new deliverynotelist from a general overviewlist
 */
var page_clientoverview_deliverynotelist_create = function()
{
	page_clientoverview_deliverynote_list = new overviewlist(
	{
		target:"#clientoverview_tab_overviewlist_deliverynote > div.overviewlist_wrapper",
		resizable:true,
		selectFirst:false,
		autoresize:false,
		sortable:false,
	});
	page_clientoverview_deliverynotelist_loadHead();
}

/*
 * invoicelist_loadContent:
 *  - method to load the invoice data of the client
 */
var page_clientoverview_invoicelist_loadContent = function()
{
	//TODO load from database
	page_clientoverview_invoice_list.insertDataArray(overviewlist_demodata_invoice_content, {callback:null});
}

/*
 * invoicelist_loadHead:
 *  - method to load the invoice head data of the client
 */
var page_clientoverview_invoicelist_loadHead = function()
{
	//TODO load from database
	page_clientoverview_invoice_list.insertHeadLine(overviewlist_demodata_invoice_head, {callback:page_clientoverview_invoicelist_loadContent});
}

/*
 * invoicelist_create:
 *  - method to create a new invoicelist from a general overviewlist
 */
var page_clientoverview_invoicelist_create = function()
{
	page_clientoverview_invoice_list = new overviewlist(
	{
		target:"#clientoverview_tab_overviewlist_invoice > div.overviewlist_wrapper",
		resizable:true,
		selectFirst:false,
		autoresize:false,
		sortable:false,
	});
	page_clientoverview_invoicelist_loadHead();
}

/*
 * quotelist_loadContent:
 *  - method to load the quote data of the client
 */
var page_clientoverview_quotelist_loadContent = function()
{
	//TODO load from database
	page_clientoverview_quote_list.insertDataArray(overviewlist_demodata_quote_content, {callback:null});
}

/*
 * quotelist_loadHead:
 *  - method to load the quote head data of the client
 */
var page_clientoverview_quotelist_loadHead = function()
{
	//TODO load from database
	page_clientoverview_quote_list.insertHeadLine(overviewlist_demodata_quote_head, {callback:page_clientoverview_quotelist_loadContent});
}

/*
 * quotelist_create:
 *  - method to create a new quotelist from a general overviewlist
 */
var page_clientoverview_quotelist_create = function()
{
	page_clientoverview_quote_list = new overviewlist(
	{
		target:"#clientoverview_tab_overviewlist_quote > div.overviewlist_wrapper",
		resizable:true,
		selectFirst:false,
		autoresize:false,
		sortable:false,
	});
	page_clientoverview_quotelist_loadHead();
}

/*
 * list_elementSelectFunction:
 *  - function called if a list entry in the overviewlist is selected
 *	- identifier {String} identifier or name
 *	- $elemClicked {jQuery} the clicked element 
 */
var page_clientoverview_list_elementSelectFunction = function(identifier, $elemClicked)
{
	console.log("page_clientoverview_list_elementSelectFunction")
	//fillin masterdata_main with the selected value in clientoverviewlist
	widget_masterdata_main_fillInValues(page_clientoverview_client_list.getSelectedData());	
}

/*
 * list_elementUnselectFunction:
 *  - function called if a list entry in the overviewlist is unselected *
 *	- identifier {String} identifier or name
 *	- $elemClicked {jQuery} the clicked element
 */
var page_clientoverview_list_elementUnselectFunction = function(identifier, $elemClicked)
{
	//clear values shown in masterdata editor and disbale editmode of the masterdata editor
	widget_masterdata_main_clearValues();
	widget_masterdata_main_editmode(false);	
}

/*
 * tab_load:
 *  - method to load a tab
 *	- widgetName {String} the name of the widget
 *	- tabID {String} the id of the tab to load
 */
var page_clientoverview_tab_load = function(widgetName, tabID, visible)
{
	if (page_clientoverview_tab_loadedTabs == null)
	{
		page_clientoverview_tab_loadedTabs = new Array();
	}
	if (page_clientoverview_tab_loadedTabs.indexOf(tabID) == -1)
	{
		loadutils.loadwidget(widgetName, $("#"+tabID), {callback: page_clientoverview_tab_insertContent, initargs:tabID});
		
		if (visible) 
		{
		    $("#"+tabID).addClass("clientoverview_tabcontent_visible").removeClass("clientoverview_tabcontent_hidden").show();
		}
		else
		{
		    $("#"+tabID).hide();
		}
	}
	else
	{
		page_clientoverview_tab_show(tabID);
	}	
}

/*
 * tab_insertContent:
 *  - method to display the content of a tab
 *	- tabID {String} the id of the current tab
 */
var page_clientoverview_tab_insertContent = function(tabID)
{
	//page_clientoverview_tab_loadedTabs.push(tabID);
	
	if(tabID == "clientoverview_tab_overviewlist_quote")
	{
		//page_clientoverview_quotelist_create();
	}
	else if(tabID == "clientoverview_tab_overviewlist_invoice")
	{
		//page_clientoverview_invoicelist_create();
	}
	else if(tabID == "clientoverview_tab_overviewlist_deliverynote")
	{
		//page_clientoverview_deliverynotelist_create();
	}
	else if(tabID == "clientoverview_tab_overviewlist_listofsale")
	{
		//TODO 
		//$("#clientoverview_tab_overviewlist_listofsale").text("Hier sind die Umsätze in einer schönen Grafik dargestellt");
	}	
	//page_clientoverview_tab_show(tabID);	
}

/*
 * tab_show:
 *  - method to show a tab
 *	- tabID {String} the id of the current tab
 */
var page_clientoverview_tab_show = function(tabID)
{
	$(".clientoverview_client_tabmenu_entry").parent().children("li").removeClass("clientoverview_client_tabmenu_entry_active");
	$(".clientoverview_client_tabmenu_entry#clientoverview_client_tabmenu_entry_"+tabID.split("clientoverview_tab_")[1]).addClass('clientoverview_client_tabmenu_entry_active');
 	$(".clientoverview_tabcontent").addClass('clientoverview_tabcontent_hidden');
 	$("#"+tabID).removeClass('clientoverview_tabcontent_hidden');
	page_clientoverview_showCurrentSelected(tabID); 
}

/*
 * showCurrentSelected:
 *  - method which defines actions to perform after selecting an entry
 *  - at the moment simply display the dataset
 *	- tabID {String} the id of the selected tab
 */
var page_clientoverview_showCurrentSelected = function(tabID)
{	
 	if(tabID == "clientoverview_tab_overviewlist_quote")
	{
		//TODO
	}
	else if(tabID == "clientoverview_tab_overviewlist_invoice")
	{
		//TODO
	}
	else if(tabID == "clientoverview_tab_overviewlist_deliverynote")
	{
		//TODO
	}
	else if(tabID == "clientoverview_tab_overviewlist_listofsale")
	{
		//TODO
	}
	else
	{
		if (page_clientoverview_client_list.getSelectedData() != null)
		{
		    console.log(page_clientoverview_client_list.getSelectedData());
			widget_masterdata_main_fillInValues(page_clientoverview_client_list.getSelectedData());
		}
		else
		{
			widget_masterdata_main_clearValues();
		}		
	}
}

/*
 * filterList:
 *  - method to filter the clientlist
 *	- filter {String} (see overviewlist - filterlist)
 *	- filterkey {String} the key of the coloumn to filter
 */
var page_clientoverview_filterList = function(filter, filterkey)
{
	page_clientoverview_client_list.filterList(filter, filterkey);

	page_clientoverview_showCurrentSelected();	
}

/*
 * navigationsettings:
 *  - method to configure the navigation bar -> add page specific functionality to buttons
 */
var page_clientoverview_navigationsettings = function()
{
    // show the masterdata menus
	widget_nav_toggle("main",true);
	widget_nav_toggle("clientoverview_main",true);
	widget_nav_toggle("clientoverview_context",true);
	
	
    // main
	widget_nav_bind('clientoverview_show_clientdata', function()
	{
		page_clientoverview_tab_load("masterdata", "clientoverview_tab_masterdata_main");
		return false;
	});
	
	// quote
	widget_nav_bind('clientoverview_show_quotes', function()
	{
		page_clientoverview_tab_load("overviewlist", "clientoverview_tab_overviewlist_quote");
		return false;
	});
	
	// invoice
	widget_nav_bind('clientoverview_show_invoices', function()
	{
		page_clientoverview_tab_load("overviewlist", "clientoverview_tab_overviewlist_invoice");
		return false;
	});
	
	// deliverynote
	widget_nav_bind('clientoverview_show_deliverynotes', function()
	{
		page_clientoverview_tab_load("overviewlist", "clientoverview_tab_overviewlist_deliverynote");
		return false;
	});
	
	// sales
	widget_nav_bind('clientoverview_show_allsales', function()
	{
		page_clientoverview_tab_load("overviewlist", "clientoverview_tab_overviewlist_listofsale");
		return false;
	});
    /*
    $("#nav_toclipboard").unbind("click", widget_nav_defaultaction);
    $("#nav_toclipboard").bind("click", function(event){
        page_masterdata_storevalues();
        widget_nav_defaultaction;
        window.location.hash="!clipboard";

        // unbind other changed event-handlers
        $("#nav_reload").click(widget_nav_defaultaction);

        // unbind the customized action of this button itself
        $(this).unbind(event);
        return false;
    });*/
};

/*
 * will be called by window-resize-handler
 */
var page_clientoverview_resize = function () 
{
    var h=$(window).height();
    var w=$(window).width();
    
    var owy=h-23;
    $("#clientoverview_overflow_wrapper").css("height", owy);
    
    var wy=((h-25)/100)*95;
    if (wy < 620) { wy = 620; };
    $("#clientoverview_wrapper").css("height", wy);
    
    var lx=$("#clientoverview_wrapper").width()-300;
    $("#clientoverview_list_area").css("width", lx);
    
    var cx=$("#clientoverview_wrapper").width()-42;
    $("#clientoverview_content_area").css("width", cx);
    
    var cy=$("#clientoverview_wrapper").height() - $("#clientoverview_list_area").height() - 110;
    $("#clientoverview_content_area").css("height", cy);
}

/*
 * calculates width for headlines in overviewlist once
 */
var page_clientoverview_enlarge_list_headline=function()
{
    var w=$("#clientoverview_list_area").width();
    var $ul=$("div.overviewlist_wrapper ul.overviewlist_list_headline li ul.overviewlist_list_entry");
    var $li=$ul.children;
    
    var length=Math.floor($li.length/2)+1;
        
    //var length=$(".overviewlist_wrapper div ul li ul#overviewlist_list_entry").length;
    
    var hx=(w/length)-5;
    console.log(hx);
    $(".overviewlist_list_enlargable").css("width", hx);
    $(".overviewlist_list_entry_enlarge").css("width", hx);
    
    console.log(ey);
    var ey=$("#clientoverview_list_area").height() - $(".overviewlist_list_headline").height() - 1;
    $(".overviewlist_list_content").css("height", ey);
    
}


/*
 * init:
 *  - the init function which should be called by the loader
 */
var page_clientoverview_init = function()
{
    // draw header / footer canvas
	var cgui = new canvasgui();
    cgui.pseudocss();
    cgui.drawheader("clientoverview_headerback");
    	
    // resize widgets on window-resize
    $(window).resize( page_clientoverview_resize );
    page_clientoverview_resize();
    setTimeout(page_clientoverview_enlarge_list_headline, 100);
    	
	//load listwidget to show all adresses
	loadutils.loadwidget("overviewlist", $("#clientoverview_list_area"), {callback: page_clientoverview_clientlist_create});
	
	//load all tabs
	page_clientoverview_tab_load("masterdata_main", "clientoverview_tab_masterdata_main", true);    
    page_clientoverview_tab_load("overviewlist_quote", "clientoverview_tab_overviewlist_quote", false);    
    page_clientoverview_tab_load("overviewlist_invoice", "clientoverview_tab_overviewlist_invoice", false);    
    page_clientoverview_tab_load("overviewlist_deliverynote", "clientoverview_tab_overviewlist_deliverynote", false);    
    page_clientoverview_tab_load("overviewlist_listofsale", "clientoverview_tab_overviewlist_listofsale", false);    

    $(".overviewlist_list_entry_enlarge").unbind("click").bind("click", function () {
        
    });

	// event listener for the data entries
    $("ul#clientoverview_filter_list > li").click(function() 
    {
        if(!$(this).hasClass('clientoverview_filter_listelement_active'))
        {
            $(this).parent().children("li").removeClass("clientoverview_filter_listelement_active");
            $(this).addClass('clientoverview_filter_listelement_active');
            page_clientoverview_filterList($(this).attr("value"), "companyname");
        }	
    });	
	
	// event listener for the tabs
    $(".clientoverview_client_tabmenu_entry").click(function() 
    {
        if(!$(this).hasClass('clientoverview_client_tabmenu_entry_active'))
        {
            $(".clientoverview_tabcontent_visible").hide();
            $(".clientoverview_tabcontent_visible").removeClass("clientoverview_tabcontent_visible").addClass("clientoverview_tabcontent_hidden");
            
            $(".clientoverview_client_tabmenu_entry").removeClass("clientoverview_client_tabmenu_entry_active");
            $(this).addClass("clientoverview_client_tabmenu_entry_active");
            var id=$(this)[0].id;
            
            switch (id){
                case "clientoverview_client_tabmenu_entry_masterdata_main":
                    $("#clientoverview_tab_masterdata_main").addClass("clientoverview_tabcontent_visible").removeClass("clientoverview_tabcontent_hidden").show();
                    break;
                case "clientoverview_client_tabmenu_entry_overviewlist_quote":
                    $("#clientoverview_tab_overviewlist_quote").addClass("clientoverview_tabcontent_visible").removeClass("clientoverview_tabcontent_hidden").show();
                    break;
                case "clientoverview_client_tabmenu_entry_overviewlist_invoice":
                    $("#clientoverview_tab_overviewlist_invoice").addClass("clientoverview_tabcontent_visible").removeClass("clientoverview_tabcontent_hidden").show();
                    break;
                case "clientoverview_client_tabmenu_entry_overviewlist_deliverynote":
                    $("#clientoverview_tab_overviewlist_deliverynote").addClass("clientoverview_tabcontent_visible").removeClass("clientoverview_tabcontent_hidden").show();
                    break;
                case "clientoverview_client_tabmenu_entry_overviewlist_listofsale":
                    $("#clientoverview_tab_overviewlist_listofsale").addClass("clientoverview_tabcontent_visible").removeClass("clientoverview_tabcontent_hidden").show();
                    break;
                default:
                    $("#clientoverview_tab_masterdata_main").addClass("clientoverview_tabcontent_visible").removeClass("clientoverview_tabcontent_hidden").show();
                    break;
            }
        }		
    })

	// event listener for instant search functionality
	$("#clientoverview_search_keyword").keyup(function(event) 
	{
		page_clientoverview_client_list.search($(this).val(), $("#clientoverview_searchtype_selection > option:selected").attr("id").split("clientoverview_searchtype_")[1]);
	});
	
	// change some button actions
	page_clientoverview_navigationsettings();
};
