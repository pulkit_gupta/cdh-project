/*
 * the entry page into the application which provides:
 *  - the personal deposit
 *  - the group deposit
 *  - the main navigation to the other modules 
 *
 * resize:
 *  - handles a window resize and fits the clipboard panes
 *
 * init:
 *  - the init function which should be called by the loader
 */

/*
 * resize:
 *  - handles a window resize and fits the clipboard panes
 */
var page_clipboard_resize = function(){
    $(".clipboard_content").height(
        $(window).height()- 142 // todo: should be calculated because the nav 
                                // could not be resized atm
    );
};

////*****************************************////
////**** DOM MANIPLUATION. INSERTS TASKS ****////
var page_clipboard_fillul=function(id, type, title, text, author, deadline, dbid, nid, group, date, owner, prio)
{
    var string='<li class="clipboard_list_element"  id="'+ nid + '">\
        	<table>\
        		<tr>\
        			<td style="vertical-align: top">';
    if (type=="task")
        string+='<img width="70px" src="images/task.png">';
    else if (type=="note")
        string+='<img width="70px" src="images/note.png">';
    else if (type=="document")
        string+='<img width="70px" src="images/quote.png">';
    else
        return;
    string+='</td>\
        			<td>\
        				<table>\
        					<tr>\
        						<td style="font-weight: bold;" class="clipboard_list_element_left clipboard_tool_title">Titel</div>:</td>\
        						<td colspan="2">' + title + '</td>\
        					</tr>\
    						<tr>\
        						<td colspan="3"><div id="description' + nid + '"><div class="clipboard_tool_descr">Beschreibung:</div>\
        						<small>' + text + '</small></div>\
        					</tr>\
							<tr>';
    var time=0;
    var t=new Date();
    t.setTime(deadline);
    time=t.getDate() + '.' + (t.getMonth()+1) + '.' + (1900+t.getYear());
    string+='\
        						<td style="font-weight: bold;" class="clipboard_list_element_left clipboard_tool_deadline">Deadline:</td>\
        						<td>' + time + '</td>\
        						<td class="align_right"><small><a href="#" id="lseeDescri' + nid + '" class="clipboard_tool_viewdescr">[Beschreibung]</a></small></td>\
        					</tr>\
        					<tr>\
        					    <td style="font-weight: bold;" class="clipboard_list_element_left clipboard_tool_author">Author: </td>\
        					    <td id="clipboard_task'+nid+'_author">loading... </td>\
        					    <td class="align_right"><small><a href="#" id="done' + nid + '" class="clipboard_tool_done">[Erledigt]</a></small></td>\
        					</tr>\
        				</table>\
        			</td>\
        		</tr>\
        	</table>\
        	<input type="hidden" id="title" value="' + title + '">\
        	<input type="hidden" id="text" value="' + text + '">\
        	<input type="hidden" id="deadline" value="' + deadline + '">\
        	<input type="hidden" id="author" value="' + author + '">\
        	<input type="hidden" id="type" value="' + type + '">\
        	<input type="hidden" id="dbid" value="' + dbid + '">\
        	<input type="hidden" id="nid" value="' + nid + '">\
        	<input type="hidden" id="group" value="' + group + '">\
        	<input type="hidden" id="date" value="' + date + '">\
	    </li>';
	   
        $("#"+id).append(string);
        $("#description"+nid).hide();

        var state=false;
        $("#lseeDescri"+nid).click(function () 
        {
            if (state==false)
            {
                $("#description"+nid).slideDown();
                state=true;
            }
            else
            {
                $("#description"+nid).slideUp();
                state=false;
            }
        });
        
        $("#done" + nid).click(function () 
        {
            Database.query("clipboard_done", 
                {"id":dbid, "done":1, "date":date, 
                 "group_id": group, "owner_id": owner, 
                 "deadline":deadline, "author":author,
                 "description": text, "title": title,
                 "type": type, "prio":prio}, function () 
                 {
                     $("#"+nid).fadeOut(function () 
                     { 
                         $("#"+nid).hide();
                     });
                 });
        });
        
        Database.query("user_namebyid", {"id":author}, function (e) 
        {
            if (e.children.length!=1)
                return;
            
            $("#clipboard_task"+nid+"_author")[0].innerHTML=e.children[0].name;
        });
}
////**** DOM MANIPLUATION. INSERTS TASKS ****////
////*****************************************////

var page_clipboard_newdoc_initdate=function (id)
{
    dt=new Date();
    
    month=0;
    if ((dt.getMonth()+1)<10)
        month="0";
    month+=(dt.getMonth()+1);
    
    day=0;
    if (dt.getDate()<10)
        day="0";
    day+=dt.getDate();   
      
    t=(dt.getYear()+1900) +"-"+ month +"-"+ day;
    document.getElementById(id).value=t;
}

var page_clipboard_filllist = function()
{
    $("#clipboard_personalclipboard_content_list").empty();
    $("#clipboard_publicclipboard_content_list").empty();
    Database.query("clipboard", {"owner":1}, function (e)
    {
        for (i=0; i<e.children.length; i++)
        {
            page_clipboard_fillul("clipboard_personalclipboard_content_list", 
                e.children[i].type, e.children[i].title, 
                e.children[i].description, e.children[i].author,
                e.children[i].deadline, e.children[i]._id, 
                e.children[i]._key, e.children[i].group_id,
                e.children[i].date, e.children[i].owner_id,
                e.children[i].prio);
        }
    });
    
    /// public clipboard
    Database.query("clipboard", {"group":1}, function (e)
    {
        for (i=0; i<e.children.length; i++)
        {
            
            page_clipboard_fillul("clipboard_publicclipboard_content_list", 
                e.children[i].type, e.children[i].title, 
                e.children[i].description, e.children[i].author,
                e.children[i].deadline, e.children[i]._id,
                e.children[i]._key, e.children[i].group_id,
                e.children[i].date, e.children[i].owner_id,
                e.children[i].prio);
        }
    });
}

/*
 * Takes care of getting the translations from translator.
 */
var page_clipboard_translate = function () {
    // translates the newdoc_dialog
    translator.ask("clipboard_newdoc_btnclose", "newdoc_btnclose");
    translator.ask("clipboard_newdoc_text", "newdoc_text_label");
    translator.ask("clipboard_newdoc_title", "newdoc_title_label");
    translator.ask("clipboard_newdoc_deadline", "newdoc_deadline_label");
    translator.ask("clipboard_newdoc_task", "newdoc_task");
    translator.ask("clipboard_newdoc_note", "newdoc_note");
    translator.ask("clipboard_newdoc_document", "newdoc_document");
    translator.ask("clipboard_newdoc_type", "newdoc_type_label");
    translator.ask("clipboard_newdoc_btncreate", "newdoc_btnsave");
    translator.ask("clipboard_newdoc_footerback", "newdoc_footerback");

    // translates clipboard_tool
    translator.ask_class("clipboard_tool_title", "clipboard_tool_title");
    translator.ask_class("clipboard_tool_description", "clipboard_tool_descr");
    translator.ask_class("clipboard_tool_author", "clipboard_tool_author");
    translator.ask_class("clipboard_tool_done", "clipboard_tool_done");
    translator.ask_class("clipboard_tool_deadline", "clipboard_tool_deadline");
    translator.ask_class("clipboard_tool_search", "clipboard_tool_search");
    translator.ask_class("clipboard_tool_viewdescription", "clipboard_tool_viewdescr");
    translator.ask("clipboard_tool_personalfooter", "clipboard_personalclipboard_footerback");
    translator.ask("clipboard_tool_groupfooter", "clipboard_groupclipboard_footerback");

    window.setTimeout(page_clipboard_footer_redraw, 1000);
}

var page_clipboard_footer_redraw=function () {
	var cgui=new canvasgui();
    cgui.pseudocss();
    cgui.drawheader("clipboard_personalclipboard_headerback");
    cgui.drawfooter("clipboard_personalclipboard_footerback");
    
    cgui.drawheader("clipboard_groupclipboard_headerback");
    cgui.drawfooter("clipboard_groupclipboard_footerback");
}

/*
 * init:
 * - the init function which should be called by the loader
 */
var page_clipboard_init = function(id){
    page_clipboard_translate();
    
    var cgui=new canvasgui();
    cgui.pseudocss();
    cgui.drawheader("clipboard_personalclipboard_headerback");
    cgui.drawfooter("clipboard_personalclipboard_footerback");
    
    cgui.drawheader("clipboard_groupclipboard_headerback");
    cgui.drawfooter("clipboard_groupclipboard_footerback");

    $(window).resize(page_clipboard_resize);
    
    page_clipboard_filllist();
    
    $(".clipboard_content_list").sortable(
    {
        connectWith : ".clipboard_content_list",
        placeholder : "clipboard_helper_highlight",
        tolerance : 'pointer',
        revert : true,
        start : function(event, ui)
        {
            jQuery(this).unbind('click');
        },
        update : function(event, ui)
        {
        }

    }).disableSelection();
    
    widget_nav_toggle("main",true);
    widget_nav_toggle("pages",true);
    widget_nav_toggle("document",true);
    widget_nav_toggle("edit",true);
    widget_nav_toggle("output",true);
    
    ////**************************************************////
    ////**** DOM MANIPULATION! INSERTS NEWDOC DIALOG! ****////
    $("#newdoc_container").append('<div id="newdoc" class="widget dialog closed">\
        <button id="newdoc_btnclose" class="dialog_btnclose">schließen</button>\
        <form id="newdoc_groups" class="dialog_groups">\
            <ul class="newdoc_item_group dialog_item_group">\
                <li class="newdoc_item dialog_item">\
                    <label id="newdoc_title_label">Aufgabentitel</label>\
                    <input type="text" value="" name="newdoc_title" id="newdoc_title">\
                </li>\
                <li class="newdoc_item dialog_item">\
                    <label id="newdoc_text_label">Text</label>\
                    <input type="text" value="" name="newdoc_text" id="newdoc_text">\
                </li>\
                <li class="newdoc_item dialog_item">\
                    <label id="newdoc_deadline_label">Deadline</label>\
                    <!-- n/a in firefox -->\
                    <input type="date" value="" name="newdoc_date" id="newdoc_date">\
                </li>\
            </ul>\
            <ul class="newdoc_item_group dialog_item_group">\
                <li class="newdoc_item dialog_item">\
                    <label id="newdoc_type_label">Aufgabentyp</label>\
                    <select name="newdoc_type" id="newdoc_type">\
                        <option value="task" id="newdoc_task">Aufgaben</option>\
                        <option value="note" id="newdoc_note">Notiz</option>\
                        <option value="document" id="newdoc_document">Dokument</option>\
                    </select>\
                </li>\
                <li class="newdoc_item align_right dialog_item">\
                    <button id="newdoc_btnsave">Anlegen</button>\
                </li>\
            </ul>\
        </form>\
        <canvas id="newdoc_footerback" class="newdoc_footerback dialog_footerback">\
            Neue Aufgabe erstellen\
        </canvas>\
    </div>');
    ////**** DOM MANIPULATION! ENDING HERE            ****////
    ////**************************************************////
   
    widget_dialog("newdoc");
    page_clipboard_newdoc_initdate("newdoc_date");
    
    /// Within widget_dialog "newdoc" (savebutton)
    $("#newdoc_btnsave").click(function()
    {
        sp=document.getElementById("newdoc_date").value.split("-");
        dt=new Date(sp[0], sp[1]-1, sp[2]);
        t=dt.getTime();     // got input with input type=date of widget_dialog newdoc
        dat=new Date();
        time=dat.getTime(); // current timestamp
    
        $li=$("#clipboard_personalclipboard_content_list li");
        prio=1+$li.length;
    
        if (document.getElementById("newdoc_title").value.length<1)
        {
        	widget_notification_display("Bitte geben Sie Ihrer Aufgabe einen Titel.", false, function()
            {  });
            return false;
        }
        if (document.getElementById("newdoc_text").value.length<1)
        {
            widget_notification_display("Bitte geben Sie eine Beschreibung Ihrer Aufgabe ein.", false, function()
            {  });
            return false;
        }

        Database.query("clipboard_create",
            {"title":document.getElementById("newdoc_title").value, 
             "description":document.getElementById("newdoc_text").value, 
             "owner_id":1, "group_id":1, "prio":prio, "date":time, "deadline":t,
             "author":1, "type":document.getElementById("newdoc_type").value, "done":0}, 
            function (e) {                                                      // callback

                if (e.children.length<1)
                    return false;

                page_clipboard_fillul("clipboard_personalclipboard_content_list", 
                    document.getElementById("newdoc_type").value,
                    document.getElementById("newdoc_title").value, 
                    document.getElementById("newdoc_text").value, 
                    e.children[0].author,
                    t,
                    e.children[0]._id,
                    e.children[0]._key,
                    e.children[0].group_id, 
                    time,
                    e.children[0].owner_id,
                    prio);
      
                document.getElementById("newdoc_title").value="";
                document.getElementById("newdoc_text").value="";
                document.getElementById("newdoc_type").value="task";
                page_clipboard_newdoc_initdate("newdoc_date");
            });
            
        
        widget_dialog_toggle("newdoc");
        return false;
    });
    
    
    
    /// shows widget_dialog "newdoc"
    widget_nav_bind("newdoc", function () 
    { 
        widget_dialog_toggle("newdoc");
        return false; 
    });
    
    /// will reload the content of the page
    widget_nav_bind("revertallchanges", function () 
    {
        page_clipboard_filllist();
        widget_notification_display("Änderungen zurückgesetzt.", false, function(){  });
    
        return false;
    });
    
    /// searchbar for personal clipboard
    $("#clipboard_input_personal").on("change", function()
    {
        $title=$("#clipboard_personalclipboard_content_list li #title");
        $key=$("#clipboard_personalclipboard_content_list li #nid");
        for (i=0; i<$title.length; i++)
        {   
            if ($title[i].value.search(document.getElementById("clipboard_input_personal").value)==-1)
            {
                $("#"+$key[i].value).stop().fadeOut();
            }
            else
            {
                $("#"+$key[i].value).stop().fadeIn();
            }
        };
    });
    
    /// searchbar for public clipboard
    $("#clipboard_input_public").on("change", function()
    {
        $title=$("#clipboard_publicclipboard_content_list li #title");
        $key=$("#clipboard_publicclipboard_content_list li #nid");
        for (i=0; i<$title.length; i++)
        {   
            if ($title[i].value.search(document.getElementById("clipboard_input_public").value)==-1)
            {
                $("#"+$key[i].value).stop().fadeOut();
            }
            else
            {
                $("#"+$key[i].value).stop().fadeIn();
            }
        };
    });
    
    /// will save every node currently in work
    widget_nav_bind("savedoc", function ()
    {
        $title=$("#clipboard_personalclipboard_content_list li #title");
        $text=$("#clipboard_personalclipboard_content_list li #text");
        $time=$("#clipboard_personalclipboard_content_list li #deadline");
        $author=$("#clipboard_personalclipboard_content_list li #author");
        $type=$("#clipboard_personalclipboard_content_list li #type");
        $dbid=$("#clipboard_personalclipboard_content_list li #dbid");
        $group=$("#clipboard_personalclipboard_content_list li #group");
        $date=$("#clipboard_personalclipboard_content_list li #date");
        
        for (i=0; i<$title.length; i++)
        {
            Database.query("clipboard_save", 
                {"id":$dbid[i].value, "title":$title[i].value, "description":$text[i].value, 
                "owner_id":1, "group_id":parseInt($group[i].value), "prio":(1+i), "deadline": parseInt($time[i].value), 
                "date": parseInt($date[i].value), "author":parseInt($author[i].value), "type": $type[i].value, "done": 0},
                function () {;} );
        }
    
    
        $title=$("#clipboard_publicclipboard_content_list li #title");
        $text=$("#clipboard_publicclipboard_content_list li #text");
        $time=$("#clipboard_publicclipboard_content_list li #deadline");
        $author=$("#clipboard_publicclipboard_content_list li #author");
        $type=$("#clipboard_publicclipboard_content_list li #type");
        $dbid=$("#clipboard_publicclipboard_content_list li #dbid");
        $group=$("#clipboard_publicclipboard_content_list li #group");
        $date=$("#clipboard_publicclipboard_content_list li #date");
        
        for (i=0; i<$title.length; i++)
        {
            Database.query("clipboard_save", 
                {"id":$dbid[i].value, "title":$title[i].value, "description":$text[i].value, 
                "owner_id":0, "group_id":parseInt($group[i].value), "prio":(1+i), "deadline": parseInt($time[i].value), 
                "date": parseInt($date[i].value), "author":parseInt($author[i].value), "type": $type[i].value, "done": 0},
                function () {;} );
        }
        
        widget_notification_display("Änderungen gespeichert.", false, function()
        {  });
    
        return false;
    });
    
    // some browsers trigger resize also on load some not so ...
    page_clipboard_resize();
    loadutils.overlayHide();  
};
