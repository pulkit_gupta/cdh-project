/// creates widget dialog and does basic drawing on canvas elements
var widget_dialog = function (id, callback) {
    console.log(id);
    if (typeof callback !== "function" ) callback=function(){};
    // todo: load from cache / db
    if (typeof(config) !== "undefined") {
        widget_settings_speed=config.animation_duration;
    }
    var cgui = new canvasgui();
    cgui.pseudocss();
    cgui.drawfooter(id+"_footerback");
        
    $("#"+id+" canvas").click(function () { widget_dialog_toggle(id); });
    
    $("#"+id+"_btnclose").click(function () { widget_dialog_toggle(id); });
    $("#"+id).hide();
}

/// shows and hides a widget_dialog
/// closed every other widget_dialog
var widget_dialog_toggle = function (id){
    $dialog=$("#"+id)[0];
    if ($dialog.classList.contains("closed"))
    {
        $(".opened").slideToggle(widget_settings_speed,'swing');
        $(".opened").addClass("closed");
        $(".opened").removeClass("opened");
        $dialog.classList.remove("closed");
        $dialog.classList.add("opened");
    }
    else
    {
        $dialog.classList.add("closed");
        $dialog.classList.remove("opened");
    }
    
    $("#"+id).slideToggle(widget_settings_speed,'swing');
}

