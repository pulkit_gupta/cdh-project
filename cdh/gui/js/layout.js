/*
 * layout.js
 * 
 * experimental
 * 
 */

/**
 * TestDocument
 * 
 * @returns
 */
function TestDocument() {
	// Document
	var document = new QuoteContent();
	document.setStyleProperty("width", 210);
	document.setStyleProperty("height", 297);
	// Header
	var header = new ContentNode();
	var coverHeader = new LetterHeaderContent();
	coverHeader.setStyleProperty("top", 0);
	coverHeader.setStyleProperty("left", 0);
	coverHeader.setStyleProperty("width", 210);
	coverHeader.setStyleProperty("height", 95);
	coverHeader.setPageNumber(1);
	var addressBlock = new TextElementContent();
	addressBlock.setText("Vertriebs GmbH, Am Hang 3, 99999 Augsburg\nBeispiel GmbH\nAm Gewerbegebiet 2\n11111 Berlin");
	addressBlock.setStyleProperty("left", 20);
	addressBlock.setStyleProperty("top", 27);
	addressBlock.setStyleProperty("width", 75);
	addressBlock.setStyleProperty("height", 28);
	var cn = new ContentNode(); cn.setStyleProperty("fontSize", "9pt"); //"3px");
	var i; for (i = 0; i < 41; i++) {
		addressBlock.children[i] = cn;
	}
	coverHeader.addChild(addressBlock);
	var infoBlock = new TextElementContent();
	infoBlock.setText("Ihr Zeichen: abc\nIhre Nachricht vom: 01.01.2013\nUnser Zeichen: def\nUnsere Nachricht vom: 31.01.2013\n\nName: Beate Bearbeiterin\nTelefon: 001/12345\nTelefax: 001/67890\nE-Mail: info@example.com\n\nDatum: 07.07.2013");
	infoBlock.setStyleProperty("left", 125);
	infoBlock.setStyleProperty("width", 75);
	infoBlock.setStyleProperty("top", 32);
	infoBlock.setStyleProperty("height", 55);
	coverHeader.addChild(infoBlock);
	var pageHeader = new LetterHeaderContent();
	pageHeader.setStyleProperty("top", 0);
	pageHeader.setStyleProperty("left", 0);
	pageHeader.setStyleProperty("width", 210);
	pageHeader.setStyleProperty("height", 27);
	pageHeader.setPageNumber(2);
	var addressBlock2 = new TextElementContent();
	addressBlock2.setText("Fortsetzung - Seite 2 -");
	addressBlock2.setStyleProperty("left", 20);
	addressBlock2.setStyleProperty("top", 10);
	addressBlock2.setStyleProperty("width", 50);
	addressBlock2.setStyleProperty("height", 15);
	pageHeader.addChild(addressBlock2);
	var pageHeader2 = new LetterHeaderContent();
	pageHeader2.setStyleProperty("top", 0);
	pageHeader2.setStyleProperty("left", 0);
	pageHeader2.setStyleProperty("width", 210);
	pageHeader2.setStyleProperty("height", 27);
	pageHeader2.setPageNumber(3);
	var addressBlock3 = new TextElementContent();
	addressBlock3.setText("Fortsetzung - Seite 3 -");
	addressBlock3.setStyleProperty("left", 20);
	addressBlock3.setStyleProperty("top", 10);
	addressBlock3.setStyleProperty("width", 50);
	addressBlock3.setStyleProperty("height", 15);
	pageHeader2.addChild(addressBlock3);
	header.addChild(coverHeader);
	header.addChild(pageHeader);
	header.addChild(pageHeader2);
	document.setHeader(header);
	// Body
	var body = new LetterBodyContent();
	body.setStyleProperty("left", 26);
	body.setStyleProperty("width", 165);
	body.setStyleProperty("top", 28);
	body.setStyleProperty("height", 224);
	body.setStyleProperty("cover.top", 96);
	body.setStyleProperty("cover.height", 156);
	body.setStyleProperty("cover.left", 26);
	body.setStyleProperty("cover.width", 165);
	var subject = new TextElementContent();
	var text = "Anfrage Nr. 10000"
	subject.setText(text);
	subject.setStyleProperty("left", 0);
	subject.setStyleProperty("top", 5);
	subject.setStyleProperty("width", 50);
	subject.setStyleProperty("height", 15);
	var cn = new ContentNode(); cn.setStyleProperty("fontWeight", "bold");
	var i; for (i = 0; i < subject.getText().length; i++) {
		subject.children[i] = cn;
	}
	body.addChild(subject);
	var block = new TextElementContent();
	//var text = "Dies ist die erste Zeile\ndies die zweite\ndritte\nvierte\nfünfte\nsechste\nsiebente\nachte\nneunte\nzehnte\nelfte\nzwölfte\ndreizehnte\nvierzehnte\nfünftzehnte\nsechzehnte\nsiebzehnte\nachtzehnte\nneunzehnte\nzwanzigste\neinundzwanzigste\nzweiundzwanzigste\ndreiundzwanzigste\nvierundzwanzigste\nfünfundzwanzigste\nsechsundzwanzigste\nsiebenundzwanzigste\nachtundzwanzigste\nneunundzwanzigste\ndreißigste";
	//text = text + "\n" + text;
	var text = "Lorem ipsum dolor sit amet, consectetur, adipisci velit.";
	/**
	var i; for (i = 0; i<6; i++)
		text = text + " " + text;
	}
	**/
	text = text + "\n\n" + "Mit freundlichen Grüßen,\nBeate Bearbeiterin";
	block.setText(text);
	block.setStyleProperty("left", 0);
	block.setStyleProperty("top", 20); //15
	block.setStyleProperty("width", 150); // 75
	block.setStyleProperty("height", 50); // 270 // 180
	body.addChild(block);
	var table = new ItemListContent();
	var tableHeader = new ContentNode();
	var tableColumn1 = new ItemListHeaderContent();
	tableColumn1.setType("TextElementContent");
	tableColumn1.setStyleProperty("width", 35);
	tableColumn1.setStyleProperty("height", 15);
	var tableColumn2 = new ItemListHeaderContent();
	tableColumn2.setType("TextElementContent");
	tableColumn2.setStyleProperty("width", 35);
	tableColumn2.setStyleProperty("height", 15);
	var tableColumn3 = new ItemListHeaderContent();
	tableColumn3.setType("TextElementContent");
	tableColumn3.setStyleProperty("width", 35);
	tableColumn3.setStyleProperty("height", 15);
	tableHeader.addChild(tableColumn1);
	tableHeader.addChild(tableColumn2);
	tableHeader.addChild(tableColumn3);
	var tableRow1 = new ContentNode();
	var tableRow1Cell1 = new TextElementContent();
	tableRow1Cell1.setText("Test");
	var tableRow1Cell2 = new TextElementContent();
	tableRow1Cell2.setText("Test");
	var tableRow1Cell3 = new TextElementContent();
	tableRow1Cell3.setText("Test");
	tableRow1.addChild(tableRow1Cell1);
	tableRow1.addChild(tableRow1Cell2);
	tableRow1.addChild(tableRow1Cell3);
	var tableRow2 = new ContentNode();
	var tableRow2Cell1 = new TextElementContent();
	tableRow2Cell1.setText("Test");
	var tableRow2Cell2 = new TextElementContent();
	tableRow2Cell2.setText("Test");
	var tableRow2Cell3 = new TextElementContent();
	tableRow2Cell3.setText("Test");
	tableRow2.addChild(tableRow2Cell1);
	tableRow2.addChild(tableRow2Cell2);
	tableRow2.addChild(tableRow2Cell3);
	table.header = tableHeader;
	table.addChild(tableRow1);
	table.addChild(tableRow2);
	table.setStyleProperty("top", 50);
	table.setStyleProperty("left", 80);
	table.setStyleProperty("width", 120);
	table.setStyleProperty("height", 300);
	//body.addChild(table);
	document.setBody(body);
	// Footer
	var footer = new ContentNode();
	var coverFooter = new LetterFooterContent();
	coverFooter.setStyleProperty("top", 267);
	coverFooter.setStyleProperty("left", 25);
	coverFooter.setStyleProperty("width", 165);
	coverFooter.setStyleProperty("height", 30);
	coverFooter.setPageNumber(1);
	var requiredInfo = new TextElementContent();
	requiredInfo.setText("Geschäftsräume: Vertriebs GmbH, Am Hang 3, 11111 Berlin, Kontakt: info@example.com\nBankverbindung: IBAN: DE123456123456, BIC: BAAG12345, Bank: Bank AG Berlin\nRechtsform: Gesellschaft mit beschränkter Haftung, Sitz: Berlin, Amtsgericht: Berlin, HRB: 123456\n\n");
	requiredInfo.setStyleProperty("top", 0);
	requiredInfo.setStyleProperty("left", 0);
	requiredInfo.setStyleProperty("width", 164);
	requiredInfo.setStyleProperty("height", 29);
	var cn = new ContentNode(); cn.setStyleProperty("fontSize", "9pt"); //"3px");
	var i; for (i = 0; i < requiredInfo.getText().length; i++) {
		requiredInfo.children[i] = cn;
	}
	coverFooter.addChild(requiredInfo);
	var pageFooter = new LetterFooterContent();
	pageFooter.setStyleProperty("top", 267);
	pageFooter.setStyleProperty("left", 25);
	pageFooter.setStyleProperty("width", 165);
	pageFooter.setStyleProperty("height", 30);
	pageFooter.setPageNumber(-1);
	var footInfo = new TextElementContent();
	footInfo.setText("x) Fußnote zum Text\nx) Zweite Fußnote");
	footInfo.setStyleProperty("top", 0);
	footInfo.setStyleProperty("left", 0);
	footInfo.setStyleProperty("width", 164);
	footInfo.setStyleProperty("height", 29);
	var cn = new ContentNode(); cn.setStyleProperty("fontSize", "9pt"); //"3px");
	var i; for (i = 0; i < footInfo.getText().length; i++) {
		footInfo.children[i] = cn;
	}
	pageFooter.addChild(footInfo);
	footer.addChild(coverFooter);
	footer.addChild(pageFooter);
	document.setFooter(footer);
	// return
	return document;
}

/**
 * @class LayoutContentHandlerMap
 * @description provides list of content handlers
 * @constructor
 */
function LayoutContentHandlerMap() {
	HashMap.call(this);
}

/**
 * __proto__
 */
LayoutContentHandlerMap.prototype.__proto__ = HashMap.prototype;

/**
 * register
 * 
 * @description Alias for HashMap.put
 * @param {Function|String} content
 * @param {Function} value
 */
LayoutContentHandlerMap.prototype.register = function(hash, value)
{
	this.put(hash, value);
};

/**
 * getHandler
 * 
 * @description get new instance of handler
 * @param {Function} content
 * @return {Object}
 */
LayoutContentHandlerMap.prototype.getHandler = function(content)
{
	var handler = this.get(content.constructor);
	return new handler(content);
};

/**
 * @class Widget
 * @constructor
 * @param {DocumentElementContent} content
 */
function Widget(content)
{
	this.container = null;
	this.content = content;
	this.shape = null;
}

/**
 * fillShape
 * 
 * @abstract
 */
Widget.prototype.fillShape = function()
{
};

/**
 * getContainer
 * 
 * @returns {DocumentElementContent}
 */
Widget.prototype.getContainer = function()
{
	return this.container;
};

/**
 * getContent
 * 
 * @returns {DocumentElementContent}
 */
Widget.prototype.getContent = function()
{
	return this.content;
};

/**
 * setContainer
 * 
 * @param {DocumentElementContent} value
 */
Widget.prototype.setContainer = function(value)
{
	this.container = value;
};

/**
 * setContent
 * 
 * @param {DocumentElementContent} value
 */
Widget.prototype.setContent = function(value)
{
	this.content = value;
};

/**
 * calculateNextPageOffset
 * 
 * @param {Number} lineHeight
 * @param {Number} pageLineHeight
 * @param {Number} height
 * @param {Number} pageCounter
 * @returns {Number}
 */
Widget.prototype.calculateNextPageOffset = function(lineHeight, pageLineHeight, height, pageCounter)
{
	var pageOffset = 0;
	if (DocumentLayoutManager&&(this.container instanceof LetterBodyContent)) {
		var dlm = DocumentLayoutManager;
		var elementRect = dlm.getElementBoundingRect(this.getContent());
		var documentRect = dlm.getElementBoundingRect(dlm.getDocument());
		var bodyRect = dlm.getElementBoundingRect(dlm.getDocument().getBody());
		var bodyCoverRect = dlm.getElementBoundingCoverRect(dlm.getDocument().getBody());
		var pageSeparator = dlm.getPageSeparatorHeight() / dlm.getRatioY();
		// determine start page of element
		var startPage = 1; if (elementRect.top >= bodyCoverRect.height) startPage = Math.floor((elementRect.top - bodyCoverRect.height) / bodyRect.height) + 2;
		// determine if current page is the first page of the document
		var isFirstPage = (startPage + pageCounter == 1);
		// determine if element will exceed current page (if current page is the first page on the document)
		var isBiggerThanFirstPage = ((elementRect.top + pageLineHeight + height) > bodyCoverRect.height);
		// determine if element will exceed current page (if it started on a page, which is not the first page of the document)
		var isBiggerThanPage = ((pageLineHeight + height) > bodyRect.height);
		// determine if element will exceed current page (if it started on the current page, which is not the first page of the document)
		var diffElementTop = 0;
		if (startPage>1 && pageCounter===0) {
			isBiggerThanPage = ((elementRect.top + pageLineHeight + height) > bodyRect.height);
			// set diffElementTop to element top if element starts on current page
			diffElementTop = elementRect.top;
		}
		// calculate page offset
		if ((isFirstPage && isBiggerThanFirstPage) || (!isFirstPage && isBiggerThanPage)) {
			if (isFirstPage) {
				// if current page is the first page of the document, proceed on rest of body + rest of document + page separator + body top
				pageOffset = lineHeight + ((bodyCoverRect.height - pageLineHeight - elementRect.top) + (documentRect.height - (bodyCoverRect.top + bodyCoverRect.height) + pageSeparator + bodyRect.top));
			} else {
				// if current page is not the first page of the document, proceed on rest of body + rest of document + page separator + body top
				pageOffset = lineHeight + ((bodyRect.height - pageLineHeight - diffElementTop) + (documentRect.height - (bodyRect.top + bodyRect.height) + pageSeparator + bodyRect.top));
			}
		}
	}
	return pageOffset;
};

/**
 * @class RichTextWidget
 * @constructor
 * @param {TextElementContent} content
 */
function RichTextWidget(content)
{
	Widget.call(this, content);
	this.createFabricClass();
}

/**
 * __proto__
 */
RichTextWidget.prototype.__proto__ = Widget.prototype;

/**
 * parseHtml
 *
 * @param {DOMElement} element
 */
RichTextWidget.prototype.parseHtml = function(element)
{
	var parseHtmlRecursive = function(element, inherit) {
		var format = [];
		var text = "";
		var defaultInherit = inherit;
		var i; for (i = 0; i < element.childNodes.length; i++) {
			var fragment = { text: "", format: [] };
			inherit = defaultInherit;
			if (element.childNodes[i].nodeName == "U") {
				inherit = {
					textDecoration: "underline"
				};
			}
			if (element.childNodes[i].hasChildNodes()) {
				fragment = parseHtmlRecursive(element.childNodes[i], inherit);
			}
			else {
				var fragmentText = element.childNodes[i];
				var fragmentFormat = new ContentNode();
				var fragmentStyle = window.getComputedStyle(element, null);
				fragmentFormat.setStyleProperty("fontStyle", fragmentStyle.fontStyle);
				fragmentFormat.setStyleProperty("fontFamily", fragmentStyle.fontFamily);
				fragmentFormat.setStyleProperty("fontSize", DocumentLayoutManager.scaleFontSize(fragmentStyle.fontSize));
				fragmentFormat.setStyleProperty("fontWeight", fragmentStyle.fontWeight);
				var textAlign = fragmentStyle.textAlign;
				if (textAlign == "start") textAlign = "left";
				fragmentFormat.setStyleProperty("textAlign", textAlign);
				if (inherit.textDecoration == "underline") {
					fragmentFormat.setStyleProperty("textDecoration", "underline");
				}
				else {
					fragmentFormat.setStyleProperty("textDecoration", fragmentStyle.textDecoration);
				}
				fragmentFormat.setStyleProperty("color", fragmentStyle.color);
				if (fragmentText.nodeName == "BR") {
					fragment.text = fragment.text + "\n";
					fragment.format.push(fragmentFormat);
				}
				else {
					if (fragmentText.nodeValue) {
						var j; for (j = 0; j < fragmentText.nodeValue.length; j++) {
							fragment.text = fragment.text + fragmentText.nodeValue.charAt(j);
							// @TODO: deep copy?
							fragment.format.push(fragmentFormat);
						}
					}
				}
			}
			if (element.childNodes[i].nodeName == "DIV") {
				fragment.text = fragment.text + "\n";
				fragment.format.push(fragmentFormat);
			}
			format = format.concat(fragment.format);
			text = text + fragment.text;
		}
		return { text: text, format: format };
	};
	return parseHtmlRecursive(element, {});
};

/**
 * createFabricClass
 * 
 * @description define fabric class for rich text rendering
 * @returns
 */
RichTextWidget.prototype.createFabricClass = function()
{
	if (fabric._RichText) return;
	fabric._RichText = fabric.util.createClass(fabric.Rect, {
		type : 'richText',
		textSizeCache : {},
		initialize : function(options)
		{
			/*jshint expr:true */
			options || (options = {});
			this.callSuper('initialize', options);
			this.set('text', options.text || '');
			this.set('format', options.format || {});
			this.set('originX', 'left');
			this.set('originY', 'top');
			this.set('lockScalingX', true);
			this.set('lockScalingY', true);
			this.set('hasRotatingPoint', false);
			this.widget = options.widget;
			this.calculateOffset = options.calculateOffset;
			this.set('maxWidth', options.maxWidth || 0);
		},
		toObject : function()
		{
			return fabric.util.object.extend(this.callSuper('toObject'), {
				text : this.get('text'),
				format : this.get('format'),
				calculateOffset : this.get('calculateOffset'),
				maxWidth : this.get('maxWidth'),
				widget : this.get('widget')
			});
		},
		toXml : function(type)
		{
			var canvas = fabric.document.createElement('canvas');
			var ctx = canvas.getContext('2d');
			var markup = [];
			ctx.scale(
				1/(this.scaleX),
				1/(this.scaleY)
			);
			ctx.textAlign = 'left';
			var scaledMaxWidth = this.maxWidth * DocumentLayoutManager.getRatioX();
			var x = 0;
			var y = 0;
			var pageTextHeight = 0;
			var pageCounter = 0;
			var offset = 0;
			var children = this.format;
			var lineHeight = 0;
			var maxLineWidth = 0;
			var totalOffset = 0;
			var format = {
				fontFamily : 'sans-serif',
				fontSize : '12pt',
				fontWeight : 'normal',
				fontStyle : 'normal',
				textDecoration : 'none',
				color : 'black',
				textAlign : 'left'
			};
			var defaultFont = format.fontStyle + " " + format.fontWeight + " " + format.fontSize + " " + format.fontFamily;
			var defaultStyle = "font-style: "+format.fontStyle+"; "+
						"font-weight: "+format.fontWeight+"; "+
						"font-size: "+format.fontSize+"; "+
						"font-family: "+format.fontFamily+"; "+
						"text-decoration: "+format.textDecoration+"; "+
						"fill: "+format.color+"; "+
						"color: "+format.color+";";
			var defaultTextAlign = format.textAlign;
			ctx.font = defaultFont;
			var currentFont = defaultFont;
			var openDiv = false;
			var i;
			for (i = 0; i < this.text.length; i++) {
				// set style options
				if (children[i]) {
					var fontStyle = children[i].hasStyleProperty("fontStyle") ? children[i].getStyleProperty("fontStyle") : format.fontStyle;
					var fontWeight = children[i].hasStyleProperty("fontWeight") ? children[i].getStyleProperty("fontWeight") : format.fontWeight;
					var fontSize = children[i].hasStyleProperty("fontSize") ? children[i].getStyleProperty("fontSize") : format.fontSize;
					var fontFamily = children[i].hasStyleProperty("fontFamily") ? children[i].getStyleProperty("fontFamily") : format.fontFamily;
					var textDecoration = children[i].hasStyleProperty("textDecoration") ? children[i].getStyleProperty("textDecoration") : format.textDecoration;
					var color = children[i].hasStyleProperty("color") ? children[i].getStyleProperty("color") : format.color;
					var textAlign = children[i].hasStyleProperty("textAlign") ? children[i].getStyleProperty("textAlign") : format.textAlign;
					var style = "font-style: "+fontStyle+"; "+
					            "font-weight: "+fontWeight+"; "+
								"font-size: "+fontSize+"; "+
								"font-family: "+fontFamily+"; "+
								"text-decoration: "+textDecoration+"; "+
								"fill: "+color+"; "+
								"color: "+color+";";
					var font = fontStyle + " " + fontWeight + " " + fontSize + " " + fontFamily;
					if (currentFont != font) {
						ctx.font = font;
						currentFont = font;
					}
				} else {
					var style = "font-style: "+format.fontStyle+"; "+
					            "font-weight: "+format.fontWeight+"; "+
								"font-size: "+format.fontSize+"; "+
								"font-family: "+format.fontFamily+"; "+
								"text-decoration: "+format.textDecoration+"; "+
								"fill: "+format.color+"; "+
								"color: "+format.color+";";
					var textAlign = format.textAlign;
					var font = format.fontStyle + " " + format.fontWeight + " " + format.fontSize + " " + format.fontFamily;
					if (currentFont != font) {
						ctx.font = font;
						currentFont = font;
					}
				}
				var textSize = this._measureTextSize(ctx, this.text[i], currentFont);
				var textHeightMM = textSize.height;
				lineHeight = Math.max(lineHeight, 1.5*textHeightMM);
				x = x + textSize.width;
				maxLineWidth = Math.max(x, maxLineWidth);
				if ((this.text.charCodeAt(i) == 10) || (x > scaledMaxWidth)) {
					offset = this.calculateOffset(y/this.scaleY, pageTextHeight/this.scaleY, lineHeight/this.scaleY, pageCounter);
					if (offset) {
						offset = offset*this.scaleY;
						pageTextHeight = 0;
						pageCounter++;
						totalOffset = totalOffset + (offset - y);
						y = offset;
					} else {
						pageTextHeight = pageTextHeight + lineHeight;
					}
					y = y + lineHeight;
					lineHeight = 0;
					if (type == "html") {
						if (openDiv) {
							markup.push("</div>");
							openDiv = false;
						}
						else {
							markup.push("<br\>");
						}
					}
					if (x > scaledMaxWidth) {
						var yr = (0.5 + y) | 0;
						if (type == "svg") {
							if (currentFont != defaultFont) {
								markup.push("<tspan x=\"0\" y=\""+yr+"\" style=\""+style+"\">"+this.text[i]+"</tspan>");
							}
							else {
								markup.push("<tspan x=\"0\" y=\""+yr+"\">"+this.text[i]+"</tspan>");
							}
						}
						if (type == "html" && textAlign != "left") {
							markup.push("<div style=\"textAlign: \""+textAlign+">");
							openDiv = true;
						}
						if (type == "html") markup.push("<span style=\""+style+"\">"+this.text[i]+"</span>");
						x = textSize.width;
					}
					else {
						x = 0;
					}
				} else {
					var yr = (0.5 + y) | 0;
					var xr = (0.5 + x - textSize.width) | 0;
					if (type == "svg") {
						if (currentFont != defaultFont) {
							markup.push("<tspan x=\""+xr+"\" y=\""+yr+"\" style=\""+style+"\">"+this.text[i]+"</tspan>");
						}
						else {
							markup.push("<tspan x=\""+xr+"\" y=\""+yr+"\">"+this.text[i]+"</tspan>");
						}
					}
					if (type == "html" && textAlign != "left" && xr == 0) {
						markup.push("<div style=\"text-align: "+textAlign+"\">");
						openDiv = true;
					}
					if (type == "html") markup.push("<span style=\""+style+"\">"+this.text[i]+"</span>");
				}
			}
			var group;
			if (type == "svg") group = "<g transform=\"translate("+this.getLeft()+","+this.getTop()+")\"><text style=\""+defaultStyle+"\">"+(markup.join(''))+"</text></g>";
			if (type == "html") group = (markup.join(''));
			return group;
		},
		toHTML : function()
		{
			return this.toXml("html");
		},
		toSVG : function()
		{
			return this.toXml("svg");
		},
		_measureTextSize : function(ctx, text, font) {
			var textSize = {};
			var hash = text + "::" + font;
			if (this.textSizeCache[hash]) {
				textSize = this.textSizeCache[hash];
			}
			else {
				textSize.width = ctx.measureText(text).width;
				textSize.height = ctx.measureText("M").width;
				this.textSizeCache[hash] = textSize;
			}
			return textSize;
		},
		_render : function(ctx)
		{
			/**
			if (DocumentLayoutManager.getCanvas().getActiveObject() !== null) {
				if (DocumentLayoutManager.getCanvas().getActiveObject() != this) {
					return;
				}
			}
			**/
			//this.callSuper('_render', ctx);
			var scaledMaxWidth = this.maxWidth * DocumentLayoutManager.getRatioX();
			var x = 0;
			var y = 0;
			var fillCount = 0;
			var pageTextHeight = 0;
			var pageCounter = 0;
			var offset = 0;
			var children = this.format;
			var lineHeight = 0;
			var maxLineWidth = 0;
			var totalOffset = 0;
			var format = {
				fontFamily : 'sans-serif',
				fontSize : '12pt', //'12pt', //Math.ceil(12*0.35275*DocumentLayoutManager.getScaleY())+'px',
				fontWeight : 'normal',
				fontStyle : 'normal',
				color : 'black',
				textDecoration : 'none',
				textAlign: 'left'
			};
			var defaultFont = format.fontStyle + " " + format.fontWeight + " " + format.fontSize + " " + format.fontFamily;
			ctx.font = defaultFont;
			var currentFont = defaultFont;
			var defaultFillStyle = format.color;
			ctx.fillStyle = defaultFillStyle;
			ctx.strokeStyle = defaultFillStyle;
			var currentFillStyle = defaultFillStyle;
			// TODO: why is there a gap at all?
			ctx.save();
			ctx.translate(-this.width / 2, -this.height / 2 + (12*0.353)*DocumentLayoutManager.getScaleY());
			ctx.scale(
				1/(this.scaleX),
				1/(this.scaleY)
			);
			ctx.beginPath();
			ctx.textAlign = 'left';
			var drawStack = [];
			var lineStack = [];
			var i;
			for (i = 0; i < this.text.length; i++) {
				// set style options
				if (children[i]) {
					var fontStyle = children[i].hasStyleProperty("fontStyle") ? children[i].getStyleProperty("fontStyle") : format.fontStyle;
					var fontWeight = children[i].hasStyleProperty("fontWeight") ? children[i].getStyleProperty("fontWeight") : format.fontWeight;
					var fontSize = children[i].hasStyleProperty("fontSize") ? children[i].getStyleProperty("fontSize") : format.fontSize;
					var fontFamily = children[i].hasStyleProperty("fontFamily") ? children[i].getStyleProperty("fontFamily") : format.fontFamily;
					var textDecoration = children[i].hasStyleProperty("textDecoration") ? children[i].getStyleProperty("textDecoration") : format.textDecoration;
					var textAlign = children[i].hasStyleProperty("textAlign") ? children[i].getStyleProperty("textAlign") : format.textAlign;
					var font = fontStyle + " " + fontWeight + " " + fontSize + " " + fontFamily;
					if (currentFont != font) {
						ctx.font = font;
						currentFont = font;
					}
					var fillStyle = children[i].hasStyleProperty("color") ? children[i].getStyleProperty("color") : format.color;
					if (currentFillStyle != fillStyle && !DocumentLayoutManager.isInteracting) {
						ctx.fillStyle = fillStyle;
						if (textDecoration == "underline") ctx.strokeStyle = fillStyle;
						currentFillStyle = fillStyle;
					}
				} else {
					var textAlign = format.textAlign;
					var textDecoration = format.textDecoration;
					var font = format.fontStyle + " " + format.fontWeight + " " + format.fontSize + " " + format.fontFamily;
					if (currentFont != font) {
						ctx.font = font;
						currentFont = font;
					}
					var fillStyle = format.color;
					if (currentFillStyle != fillStyle && !DocumentLayoutManager.isInteracting) {
						ctx.fillStyle = fillStyle;
						if (textDecoration == "underline") ctx.strokeStyle = fillStyle;
						currentFillStyle = fillStyle;
					}
				}
				var textSize = this._measureTextSize(ctx, this.text[i], currentFont);
				var textHeightMM = textSize.height;
				//var textHeightMM = parseInt(fontSize ? fontSize : format.fontSize, 10)/0.3527;
				lineHeight = Math.max(lineHeight, 1.5*textHeightMM);
				x = x + textSize.width;
				maxLineWidth = Math.max(x, maxLineWidth);
				lineStack[y] = x;
				if ((this.text.charCodeAt(i) == 10) || (x > scaledMaxWidth)) {
					offset = this.calculateOffset(y/this.scaleY, pageTextHeight/this.scaleY, lineHeight/this.scaleY, pageCounter);
					if (offset) {
						offset = offset*this.scaleY;
						pageTextHeight = 0;
						pageCounter++;
						totalOffset = totalOffset + (offset - y);
						y = offset;
					} else {
						pageTextHeight = pageTextHeight + lineHeight;
					}
					y = y + lineHeight;
					lineHeight = 0;
					if (x > scaledMaxWidth) {
						var yr = (0.5 + y) | 0;
						if (textAlign != "left") {
							drawStack.push({ text: this.text[i], x: 0, y: yr, font: ctx.font, line: y, align: textAlign, fill: fillStyle, textDecoration: textDecoration, textHeightMM: textHeightMM, textWidth: textSize.width});
						}
						else {
							ctx.fillText(this.text[i], 0, yr);
							if (textDecoration == "underline" && !DocumentLayoutManager.isInteracting) {
								ctx.save();
								ctx.beginPath();
								ctx.scale(this.scaleX, 1/this.scaleY);
								ctx.moveTo(0, Math.ceil((yr + textHeightMM/8)*this.scaleY));
								ctx.lineTo(Math.ceil((textSize.width)/this.scaleX), Math.ceil((yr + textHeightMM/8)*this.scaleY));
								ctx.lineWidth = this.scaleY;
								ctx.stroke();
								ctx.restore();
							}
						}
						x = textSize.width;
					}
					else {
						x = 0;
					}
				} else {
					var yr = (0.5 + y) | 0;
					var xr = (0.5 + x - textSize.width) | 0;
					if (textAlign != "left") {
						drawStack.push({ text: this.text[i], x: xr, y: yr, font: ctx.font, line: y, align: textAlign, fill: fillStyle, textDecoration: textDecoration, textHeightMM: textHeightMM, textWidth: textSize.width});
					}
					else {
						ctx.fillText(this.text[i], xr, yr);
						if (textDecoration == "underline" && !DocumentLayoutManager.isInteracting) {
							ctx.save();
							ctx.beginPath();
							ctx.scale(this.scaleX, 1/this.scaleY);
							ctx.lineWidth = this.scaleY;
							ctx.moveTo(Math.ceil(xr/this.scaleX), Math.ceil((yr + textHeightMM/8)*this.scaleY));
							ctx.lineTo(Math.ceil((xr + textSize.width)/this.scaleX), Math.ceil((yr + textHeightMM/8)*this.scaleY));
							ctx.stroke();
							ctx.restore();
						}
					}
				}
			}

			if (drawStack.length>0) {
				for (e in drawStack) {
					var el = drawStack[e];
					ctx.font = el.font;
					ctx.fillStyle = el.fill;
					ctx.strokeStyle = el.fill;
					var offset = 0;
					if (el.align == "center") {
						offset = (maxLineWidth/2 - lineStack[el.line]/2);
					}
					if (el.align == "right") {
						offset = maxLineWidth - lineStack[el.line];
					}
					var offset = (0.5 + offset) | 0;
					ctx.fillText(el.text, offset + el.x, el.y);
					if (el.textDecoration == "underline" && !DocumentLayoutManager.isInteracting) {
						ctx.save();
						ctx.beginPath();
						ctx.scale(this.scaleX, 1/this.scaleY);
						ctx.lineWidth = this.scaleY;
						ctx.moveTo(Math.ceil((offset+el.x)/this.scaleX), Math.ceil((el.y + el.textHeightMM/8)*this.scaleY));
						ctx.lineTo(Math.ceil((offset+el.x+el.textWidth)/this.scaleX), Math.ceil((el.y + el.textHeightMM/8)*this.scaleY));
						ctx.stroke();
						ctx.restore();
					}
				}
			}

			// workaround
			ctx.moveTo(0, 0);
			ctx.lineTo(0, 0);
			ctx.stroke();
			
			ctx.translate(0, 0);
			ctx.restore();
			
			var resized = false;
			if (this.getWidth() != maxLineWidth) {
				this.setWidth(maxLineWidth/this.scaleX);
				resized = true;
			}
			if (this.getHeight() != y+lineHeight) {
				this.setHeight((y+lineHeight)/this.scaleY);
				resized = true;
			}
			if (resized) {
				var top = this.getTop();
				var left = this.getLeft();
				this.setCoords();
				this.setPositionByOrigin(new fabric.Point(left, top), "left", "top");
				if (this.widget) {
					this.widget.getContent().setStyleProperty("width", maxLineWidth/this.scaleX);
					this.widget.getContent().setStyleProperty("height", (y+lineHeight-totalOffset)/this.scaleY);
				}
			}
		}
	});
};

// getters

/**
 * fillShape
 * 
 * @param {Function} callback
 */
RichTextWidget.prototype.fillShape = function(callback)
{
	var ctx = this;
	var children = this.getContent().getChildren();
	var textStr = this.getContent().getText();
	var rect = DocumentLayoutManager.getElementBoundingRect(this.getContent());
	var maxWidth = rect.width;
	var height = rect.height;
	// TODO
	if (!maxWidth) {
		maxWidth = 30;
	}
	if (!height) {
		height = 30;
	}
	var richText = new fabric._RichText({
		fill : 'transparent',
		text : textStr,
		format : children,
		maxWidth : maxWidth,
		calculateOffset : function(w, x, y, z)
		{
			return ctx.calculateNextPageOffset(w, x, y, z);
		},
		width : maxWidth,
		height : height,
		widget : ctx
	});
	ctx.shape = richText;
	callback(richText);
};

/**
 * updateShape
 * 
 * @param {Function} callback
 */
RichTextWidget.prototype.updateShape = function(callback)
{
	this.fillShape(callback);
};

/**
 * @class DocumentLayoutManager
 * @constructor
 */
function DocumentLayoutManager() {
	this.document = new DocumentContent();
	this.handlers = new LayoutContentHandlerMap();
	//this.handlers.register(ItemListContent, TableWidget);
	this.handlers.register(TextElementContent, RichTextWidget);
	this.pageSeparatorHeight = 1;
	this.scaleX = 1;
	this.scaleY = 1;
}

/**
 * setDocument
 * 
 * @param {DocumentContent} document
 */
DocumentLayoutManager.prototype.setDocument = function(document) {
	this.document = document;
};

/**
 * getDocument
 * 
 * @returns {DocumentContent}
 */
DocumentLayoutManager.prototype.getDocument = function() {
	return this.document;
};

/**
 * getPageSeparatorHeight
 * 
 * @returns {number}
 */
DocumentLayoutManager.prototype.getPageSeparatorHeight = function() {
	return this.pageSeparatorHeight;
};

/**
 * getPageCount
 * 
 * @returns {number}
 */
DocumentLayoutManager.prototype.getPageCount = function() {
	// get document and elements
	var document = this.getDocument();
	var bodyElements = document.getBody().getChildren();
	var coverRect = this.getElementBoundingCoverRect(document.getBody());
	var bodyRect = this.getElementBoundingRect(document.getBody());
	var bodyCoverHeight = coverRect.height;
	var bodyHeight = bodyRect.height;
	// calculate total height (max[element top + element height])
	var maxBottom = 0; var i; for (i = 0; i < bodyElements.length; i++) { var elementRect = this.getElementBoundingRect(bodyElements[i]); maxBottom = Math.max(elementRect.top + elementRect.height, maxBottom); }
	// calculate number of pages (total height / page body height)
	var pageCount = Math.floor((maxBottom - bodyCoverHeight) / bodyHeight) + 2;
	return pageCount;
};

/**
 * getCanvas
 * 
 * @returns {fabric.Canvas}
 */
DocumentLayoutManager.prototype.getCanvas = function() {
	return letter_pages_canvas;
};

/**
 * getRatioX
 * 
 * @returns {number}
 */
DocumentLayoutManager.prototype.getRatioX = function() {
	var documentRect = this.getElementBoundingRect(this.getDocument());
	return this.getCanvas().getWidth() / documentRect.width;
};

/**
 * getRatioY
 * 
 * @returns {number}
 */
DocumentLayoutManager.prototype.getRatioY = function() {
	var documentRect = this.getElementBoundingRect(this.getDocument());
	return (this.getCanvas().getHeight() / documentRect.height) / this.getPageCount();
};

/**
 * clearCanvas
 * 
 */
DocumentLayoutManager.prototype.clearCanvas = function() {
	this.getCanvas().clear();
	this.getCanvas().backgroundColor = "white";
	this.getCanvas().renderOnAddition = false;
};

/**
 * flushCanvas
 * 
 */
DocumentLayoutManager.prototype.flushCanvas = function() {
	this.getCanvas().renderAll();
};

/**
 * resizeCanvas
 * 
 */
DocumentLayoutManager.prototype.resizeCanvas = function() {
	// calculate height based on letter page aspect and page count
	var calculatedHeight = (Math.round(this.getCanvas().getWidth() / letter_page_aspect)+this.getPageSeparatorHeight())*this.getPageCount();
	this.getCanvas().setHeight(calculatedHeight);
};

/**
 * drawPageSeparators
 * 
 */
DocumentLayoutManager.prototype.drawPageSeparators = function() {
	var pageCount = this.getPageCount();
	var documentRect = this.getElementBoundingRect(this.getDocument());
	var i;
	for (i = 1; i < pageCount; i++) {
		var line = new fabric.Rect({
			// unscaled position
			originX : "left",
			originY : "top",
			left : 0,
			top : ((this.getCanvas().getHeight()/pageCount)*i),
			// scaled size
			width : documentRect.width*this.getRatioX(),
			height : this.getPageSeparatorHeight(),
			// disable interactivity
			lockRotation : true,
			selectable : false
		});
		this.getCanvas().add(line);
	}
};

/**
 * drawPageNumbers
 * 
 */
DocumentLayoutManager.prototype.drawPageNumbers = function() {
	var pageCount = this.getPageCount();
	if (pageCount == 1) return;
	var i;
	var ctx = this;
	for (i = 1; i <= pageCount; i++) {
		// TODO
		RichTextWidget.prototype.createFabricClass.call(ctx);
		var pageNumberText = new fabric._RichText({
			text : "Seite "+i+" von "+pageCount,
			fill : 'transparent',
			format : null,
			calculateOffset : function() { return 0; },
			// unscaled position
			originX : "left",
			originY : "top",
			left : 160*ctx.getScaleX()*ctx.getRatioX(),
			top : ((ctx.getCanvas().getHeight()/pageCount)*i)-40*ctx.getScaleY()*ctx.getRatioY(),
			// scaled size
			maxWidth : 60*ctx.getScaleX()*ctx.getRatioX(),
			width : 60*ctx.getScaleX()*ctx.getRatioX(),
			height : 5*ctx.getScaleY()*ctx.getRatioY(),
			// disable interactivity
			lockRotation : true,
			selectable : false
		});
		pageNumberText.setScaleX(ctx.getRatioX());
		pageNumberText.setScaleY(ctx.getRatioY());
		this.getCanvas().add(pageNumberText);
	}
};

/**
 * getScaleX
 * @returns {number}
 */
DocumentLayoutManager.prototype.getScaleX = function() {
	return this.scaleX;
};

/**
 * getScaleY
 * @returns {number}
 */
DocumentLayoutManager.prototype.getScaleY = function() {
	return this.scaleY;
};

/**
 * getElementBoundingRect
 * 
 * @param {DocumentElementContent} element
 * @returns {object}
 */
DocumentLayoutManager.prototype.getElementBoundingRect = function(element) {
	return {
		top : element.getStyleProperty("top")*this.getScaleY(),
		left : element.getStyleProperty("left")*this.getScaleX(),
		width : element.getStyleProperty("width")*this.getScaleX(),
		height : element.getStyleProperty("height")*this.getScaleY()
	};
};

/**
 * scaleFontSize
 *
 * @param {string} fontSizeStyle
 * @returns {string}
 */
DocumentLayoutManager.prototype.scaleFontSize = function(fontSizeStyle) {
	if (typeof fontSizeStyle == "string") {
		var value = parseInt(fontSizeStyle, 10);
		var unit = fontSizeStyle.substring(fontSizeStyle.length-2);
		var scale = 0;
		switch (unit) {
			case "cm": scale = 0.1/0.353; break;
			case "in": scale = 0.0393700787/0.353; break;
			case "mm": scale = 1/0.353; break;
			case "pc": scale = (1/12)*0.353; break;
			case "px": scale = 1/0.353; break;
		}
		var fontSize = Math.ceil((value*scale)/DocumentLayoutManager.getRatioY()) + "pt";
		return fontSize;
	}
	return null;
};

/**
 * getElementBoundingCoverRect
 * 
 * @param {DocumentElementContent} element
 * @returns {object}
 */
DocumentLayoutManager.prototype.getElementBoundingCoverRect = function(element) {
	return {
		top : element.getStyleProperty("cover.top")*this.getScaleY(),
		left : element.getStyleProperty("cover.left")*this.getScaleX(),
		width : element.getStyleProperty("cover.width")*this.getScaleX(),
		height : element.getStyleProperty("cover.height")*this.getScaleY()
	};
};

/**
 * checkElementBoundingRects
 * 
 * @param {object} containerRect
 * @param {object} elementRect
 * @returns {boolean}
 */
DocumentLayoutManager.prototype.checkElementBoundingRects = function(containerRect, elementRect) {
	if ((elementRect.top>=0) && (elementRect.left>=0) && ((elementRect.top+elementRect.height)<containerRect.height) && ((elementRect.left+elementRect.width)<containerRect.width)) return true;
	return false;
};

/**
 * drawHeader
 * 
 */
DocumentLayoutManager.prototype.drawHeader = function() {
	var ctx = this;
	// iterate all children of header list
	var headerList = this.getDocument().getHeader().getChildren();
	var i, j, k;
	for (i = 0; i < headerList.length; i++) {
		var header = headerList[i];
		// check if element is a header element
		if (header instanceof LetterHeaderContent) {
			// get page number
			var page = header.getPageNumber();
			var pages = [];
			if (page === null || page === -1) {
				var start = 1;
				if (page === -1) start = 2;
				for (j = start; j <= this.getPageCount(); j++) {
					pages.push(j);
				}
			}
			else {
				pages.push(page);
			}
			// iterate all elements of header element
			var elements = header.getChildren();
			for (j = 0; j < elements.length; j++) {
				var element = elements[j];
				// check if element is within valid bounds
				if (this.checkElementBoundingRects(ctx.getElementBoundingRect(header), ctx.getElementBoundingRect(element))) {
					for (k = 0; k < pages.length; k++) {
						var pageNumber = pages[k];
						// calculate position
						var pos = {
							left : (ctx.getElementBoundingRect(header).left+ctx.getElementBoundingRect(element).left) * ctx.getRatioX(),
							top : (((pageNumber-1)*(ctx.getElementBoundingRect(ctx.getDocument()).height+ctx.getPageSeparatorHeight()/ctx.getRatioY()))+ctx.getElementBoundingRect(header).top+ctx.getElementBoundingRect(element).top) * ctx.getRatioY()
						};
						// get __ new __ widget instance
						var widget = this.handlers.getHandler(element);
						widget.setContainer(header);
						widget.fillShape(function(shape) {
							// unscaled position
							shape.set("originX", "left");
							shape.set("originY", "top");
							//shape.setPositionByOrigin(new fabric.Point(pos.left, pos.top), "let", "top");
							shape.setLeft(pos.left);
							shape.setTop(pos.top);
							shape.setScaleX(ctx.getRatioX());
							shape.setScaleY(ctx.getRatioY());
							//shape.setWidth(shape.calculatedWidth);
							//shape.setHeight(shape.calculatedHeight);
							//shape.setPositionByOrigin(new fabric.Point(pos.left, pos.top), 'left', 'top');
							ctx.getCanvas().add(shape);
						});
					}
				}
				else {
					console.log("Error: Element out of bounding rect.", "Element:", element, "Document:", this.getDocument());
				}
			}
		}
	}
};

/**
 * drawFooter
 * 
 */
DocumentLayoutManager.prototype.drawFooter = function() {
	var ctx = this;
	// iterate all children of footer list
	var footerList = this.getDocument().getFooter().getChildren();
	var i, j, k;
	for (i = 0; i < footerList.length; i++) {
		var footer = footerList[i];
		// check if element is a footer element
		if (footer instanceof LetterFooterContent) {
			// get page number
			var page = footer.getPageNumber();
			var pages = [];
			if (page === null || page === -1) {
				var start = 1;
				if (page === -1) start = 2;
				for (j = start; j <= this.getPageCount(); j++) {
					pages.push(j);
				}
			}
			else {
				pages.push(page);
			}
			// iterate all elements of footer element
			var elements = footer.getChildren();
			for (j = 0; j < elements.length; j++) {
				var element = elements[j];
				// check if element is within valid bounds
				if (this.checkElementBoundingRects(ctx.getElementBoundingRect(footer), ctx.getElementBoundingRect(element))) {
					for (k = 0; k < pages.length; k++) {
						var pageNumber = pages[k];
						// calculate position
						var pos = {
							left : (ctx.getElementBoundingRect(footer).left+ctx.getElementBoundingRect(element).left) * ctx.getRatioX(),
							top : (((pageNumber-1)*(ctx.getElementBoundingRect(ctx.getDocument()).height+ctx.getPageSeparatorHeight()/ctx.getRatioY()))+ctx.getElementBoundingRect(footer).top+ctx.getElementBoundingRect(element).top) * ctx.getRatioY()
						};
						// get __ new __ widget instance
						var widget = this.handlers.getHandler(element);
						widget.setContainer(footer);
						widget.fillShape(function(shape) {
							// unscaled position
							shape.set("originX", "left");
							shape.set("originY", "top");
							//shape.setPositionByOrigin(new fabric.Point(pos.left, pos.top), "let", "top");
							shape.setLeft(pos.left);
							shape.setTop(pos.top);
							shape.setScaleX(ctx.getRatioX());
							shape.setScaleY(ctx.getRatioY());
							//shape.setWidth(shape.calculatedWidth);
							//shape.setHeight(shape.calculatedHeight);
							//shape.setPositionByOrigin(new fabric.Point(pos.left, pos.top), 'left', 'top');
							ctx.getCanvas().add(shape);
						});
					}
				}
				else {
					console.log("Error: Element out of bounding rect.", "Element:", element, "Document:", this.getDocument());
				}
			}
		}
	}
};

/**
 * drawBody
 * 
 */
DocumentLayoutManager.prototype.drawBody = function() {
	var ctx = this;
	var body = this.getDocument().getBody();
	var pageCount = this.getPageCount();
	// iterate all children of body
	var bodyChildren = body.getChildren();
	var i, j, k;
	for (i = 0; i < bodyChildren.length; i++) {
		var element = bodyChildren[i];
		var documentRect = ctx.getElementBoundingRect(ctx.getDocument());
		var elementRect = ctx.getElementBoundingRect(element);
		var bodyRect = ctx.getElementBoundingRect(body);
		var bodyCoverRect = ctx.getElementBoundingCoverRect(body);
		// determine number of pages before start page of element
		var prePages = Math.floor((elementRect.top-bodyCoverRect.height)/bodyRect.height)+1;
		// determine if start page of element is cover page
		var isCover = false; if (elementRect.top<bodyCoverRect.height) isCover = true;
		// calculate relative top if start page is not cover page
		var elementRelTop = elementRect.top; if (!isCover) elementRelTop = (elementRect.top-bodyCoverRect.height-(prePages-1)*bodyRect.height);
		// construct element relative rect
		var elementRelRect = {
			top: elementRelTop,
			left: elementRect.left,
			height: elementRect.height,
			width: elementRect.width
		};
		// calculate body total height
		var bodyTotalHeight = bodyCoverRect.height+(pageCount-1)*bodyRect.height;
		// construct body total rect (consent boundaries)
		var bodyTotalRect = {
			top: bodyCoverRect.top,
			left: Math.max(bodyCoverRect.left, bodyRect.left),
			width: Math.min(bodyCoverRect.width, bodyRect.width),
			height: bodyTotalHeight
		};
		// check if element is within valid bounds
		if (this.checkElementBoundingRects(bodyTotalRect, elementRect)) {
			// calculate position
			var pos = {
				left : (bodyRect.left+elementRect.left) * ctx.getRatioX(),
				top : ((isCover?bodyCoverRect.top:bodyRect.top)+prePages*documentRect.height+elementRelRect.top) * ctx.getRatioY()
			};
			// get __ new __ widget instance
			var widget = this.handlers.getHandler(element);
			widget.setContainer(body);
			widget.fillShape(function(shape) {
				// unscaled position
				shape.set("originX", "left");
				shape.set("originY", "top");
				shape.setLeft(pos.left);
				shape.setTop(pos.top);
				shape.setScaleX(ctx.getRatioX());
				shape.setScaleY(ctx.getRatioY());
				ctx.getCanvas().add(shape);
			});
		}
		else {
			console.log("Error: Element out of bounding rect.", "Element:", element, "Document:", this.getDocument());
		}
	}
	//this.cache = this.getCanvas().toDataURL();
};

/**
 * updateElement
 * 
 * @param {Object} element
 */
DocumentLayoutManager.prototype.updateElement = function(element) {
	var ctx = this;
	this.queue.block();
	ctx.getCanvas().renderAll();
	ctx.queue.unblock();
	/*
	element.widget.updateShape(function(shape) {
		ctx.getCanvas().renderAll();
		ctx.queue.unblock();
	});
	*/
};

/**
 * setupEvents
 * 
 */
DocumentLayoutManager.prototype.setupEvents = function() {
	var ctx = this;
	// TODO: update
	var pageCount = ctx.getPageCount();
	this.getCanvas().on('mouse:up', function(e) {
		ctx.isInteracting = false;
		var tgt = e.target;
		if (!tgt) {
			if (window.editor) {
				if (window.editor.canClose===false) {
					return false;
				}
				var target = window.editor.target;
				var content = target.widget.parseHtml(window.editor.options.el);
				target.widget.getContent().setText(content.text);
				target.widget.getContent().children = content.format;
				target.widget.shape.text = content.text;
				target.widget.shape.format = content.format;
				window.editor = null;
				delete window["editor"];
			}
			var editorField = $("div#editorField")[0];
			editorField.setAttribute("style", "visibility: hidden");
			while (editorField.hasChildNodes()) {
				editorField.removeChild(editorField.lastChild);
			}
			var objList = ctx.getCanvas().getObjects();
			var i; for (i = 0; i < objList.length; i++) {
				if (objList[i].isGreyRect) {
					ctx.getCanvas().remove(objList[i]);
					break;
				}
			}
			var newPageCount = ctx.getPageCount();
			if (newPageCount != pageCount) {
				pageCount = newPageCount;
				page_letter_redraw();
			}
			else {
				ctx.getCanvas().renderAll();
			}
		}		
		var isDoubleClick = false;
		var currentTime = (new Date()).getTime();
		if (tgt&&tgt.lastClick&&(currentTime-tgt.lastClick)<1500) {
			isDoubleClick = true;
		}
		if (tgt) tgt.lastClick = currentTime;
		if (isDoubleClick) {
			var greyRect = new fabric.Rect({
				originX: 'left',
				originY: 'top',
				top: 0,
				left: 0,
				width: ctx.getCanvas().getWidth(),
				height: ctx.getCanvas().getHeight(),
				opacity: 0.75,
				fill: '#959595',
				selectable: false
			});
			greyRect.isGreyRect = true;
			ctx.getCanvas().add(greyRect);
			ctx.getCanvas().renderAll();
			window.setTimeout(function() {
				var editorField = $("div#editorField")[0];
				editorField.setAttribute("style", "top: "+tgt.getTop()+"px; left: "+tgt.getLeft()+"px; width: "+tgt.getWidth()+"px; height: "+tgt.getHeight()+"px; visibility: visible; background-color: white; position: absolute; margin: 1em;");
				var contentHTML = tgt.widget.shape.toXml("html");
				var contentText = tgt.widget.getContent().getText();
				editorField.innerHTML = contentHTML;
				var model = Backbone.Model.extend({	defaults: { title: '', body: '' } });
				var view = Backbone.View.extend({
					initialize: function() { this.setElement($("div#letter_pages_wrapper")); },
					events: {
						'mousedown .editable': 'editableClick',
						'keyup .editable': 'checkHeight',
						'keypress .editable': 'checkEditable'
					},
					editableClick: etch.editableInit,
					checkEditable: function(e) {
						if (window.editor.canClose == false) {
							return false;
						}
					},
					checkHeight: function(e) {
						$("div#editorField").height(0);
						$("div#editorField").height(editorField.scrollHeight);
						
						var top = tgt.getTop();
						var heightPerPage = (ctx.getCanvas().getHeight()/pageCount);
						var pageOffset = Math.floor(top/heightPerPage)*heightPerPage;
						var element = tgt.widget.getContent();
						var container = tgt.widget.getContainer();
						var elementRect = ctx.getElementBoundingRect(element);
						var containerRect = ctx.getElementBoundingRect(container);
						var isCover = null;
						if (container instanceof LetterBodyContent) {
							return;
						}
						var elementRectTop = (top-pageOffset)/ctx.getRatioY()-(isCover?bodyCoverRect.top:containerRect.top);
						if (elementRectTop>(containerRect.height-editorField.scrollHeight/DocumentLayoutManager.getRatioY())) {
							$("div#editorField").css("backgroundColor", "#dd9999");
							if (window.editor.preservedZIndex === undefined) {
								window.editor.preservedZIndex = window.getComputedStyle($(".upper-canvas")[0], null).zIndex
							}
							$(".upper-canvas")[0].style.zIndex = -1;
							window.editor.canClose = false;
						}
						else {
							$("div#editorField").css("backgroundColor", "#ffffff");
							if (window.editor.preservedZIndex !== undefined) {
								$(".upper-canvas")[0].style.zIndex = window.editor.preservedZIndex;
								delete[window.editor.preservedZIndex];
							}
							window.editor.canClose = true;
						}

					}
				});
				var inst = new view({model: new model(), el: editorField, tagName: editorField.tagName});
				window.editor = inst;
				window.editor.target = tgt;
			}, 0);
		}
	});
	this.getCanvas().on('object:modified', function(e) {
		ctx.isInteracting = false;
		//ctx.getCanvas().setBackgroundImage(ctx.cache);
	});
	this.getCanvas().on('object:selected', function(e) {
		//ctx.isInteracting = true;
		//ctx.getCanvas().setBackgroundImage(ctx.cache);
	});
	this.getCanvas().on('selection:cleared', function(e) {
		//ctx.isInteracting = false;
		//DocumentLayoutManager.getCanvas().setBackgroundImage();
	});
	this.getCanvas().on('object:moving', function(e) {
		ctx.isInteracting = true;
		var tgt = e.target;
		var top = tgt.getTop();
		var left = tgt.getLeft();
		// check for negative coordinates
		var isOutOfCanvas = false;
		if (top<0) {
			top = 0;
			isOutOfCanvas = true;
		}
		if (left<0) {
			left = 0;
			isOutOfCanvas = true;
		}
		// calculate height per page
		var heightPerPage = (ctx.getCanvas().getHeight()/pageCount);
		// calculate height of all pages before start of element
		var pageOffset = Math.floor(top/heightPerPage)*heightPerPage;
		// determine bounding rects
		var element = tgt.widget.getContent();
		var container = tgt.widget.getContainer();
		var elementRect = ctx.getElementBoundingRect(element);
		var containerRect = ctx.getElementBoundingRect(container);
		var isCover = null;
		if (container instanceof LetterBodyContent) {
			isCover = (pageOffset/heightPerPage === 0);
			// determine body total rect
			var bodyCoverRect = ctx.getElementBoundingCoverRect(container);
			containerRect = {
				top: bodyCoverRect.top,
				left: Math.max(bodyCoverRect.left, containerRect.left),
				width: Math.min(bodyCoverRect.width, containerRect.width),
				height: bodyCoverRect.height+(pageCount-1)*containerRect.height
			};
		}
		// determine target rect
		var elementTgtRect = {
			top: (top-pageOffset)/ctx.getRatioY()-(isCover?bodyCoverRect.top:containerRect.top),
			left: left/ctx.getRatioX()-containerRect.left,
			width: elementRect.width,
			height: elementRect.height
		};
		if (tgt.lastValidY) {
			var pageOffsetValid = Math.floor(tgt.lastValidY/heightPerPage)*heightPerPage;
			if (pageOffset < pageOffsetValid) {
				elementTgtRect.top = 0;
				pageOffset = pageOffsetValid;
				isOutOfCanvas = true;
			}
			if (pageOffset > pageOffsetValid) {
				elementTgtRect.top = (containerRect.height-elementTgtRect.height)+1;
				pageOffset = pageOffsetValid;
				isOutOfCanvas = true;
			}
		}
		if (isOutOfCanvas||!ctx.checkElementBoundingRects(containerRect, elementTgtRect)) {
			if (elementTgtRect.left<=0) {
				tgt.setLeft(containerRect.left*ctx.getRatioX());
			}
			if (elementTgtRect.left>(containerRect.width-elementTgtRect.width)) {
				tgt.setLeft((containerRect.left+containerRect.width-elementTgtRect.width)*ctx.getRatioX());
			}
			if (elementTgtRect.top<=0) {
				tgt.setTop(pageOffset+containerRect.top*ctx.getRatioY());
			}
			if (elementTgtRect.top>(containerRect.height-elementTgtRect.height)) {
				tgt.setTop(pageOffset+(containerRect.top+containerRect.height-elementTgtRect.height)*ctx.getRatioY());
			}
		}
		else {
			element.setStyleProperty("top", elementTgtRect.top);
			element.setStyleProperty("left", elementTgtRect.left);
			tgt.set('lastValidY', top);
		}
	});
};

/**
 * doLayout
 * 
 */
DocumentLayoutManager.prototype.doLayout = function() {
	this.isInteracting = false;
	window.bench = new Benchmark();
	this.getCanvas().renderAll = function(p) {
		bench.start();
		fabric.StaticCanvas.prototype.renderAll.call(this, p);
		bench.stop();
	};
	this.clearCanvas();
	this.resizeCanvas();
	this.drawPageSeparators();
	this.drawPageNumbers();
	this.drawHeader();
	this.drawBody();
	this.drawFooter();
	this.setupEvents();
	this.flushCanvas();
};