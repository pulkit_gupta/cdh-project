
// load a new page
// main_section_id should be present in main


function Loadutils() {
    this.currentpage="";
}

// to prevent code injection, test if a variable just contains word chars, dots
// and underscores
Loadutils.prototype.isGoodVar = function(variable){
    return /^[\w\._]+$/g.test(variable);
};

Loadutils.prototype.overlayShow = function(){
    // check if overlay is not alrdy shown
    if (! $("#wrapper").hasClass("modenormal") ) return false;
    
    // switch mode of the body from normal mode to display the 
    // overlay 
    $("#wrapper")
            .removeClass("modenormal")
            .addClass("modeoverlay");
    return this;
};

Loadutils.prototype.overlayHide = function(){
    if (! $("#wrapper").hasClass("modeoverlay") ) return false;
    // switch mode of the body from displaying the overlay to normal mode
    $("#wrapper")
            .delay(5000)
            .removeClass("modeoverlay")
            .addClass("modenormal");  
    return this;  
};

Loadutils.prototype.overlaySetProgress = function(val){
    // check if val is in range
    if ( val > 100 || val < 0) return false;
    
    var $progress = $("#overlay progress:first");
    $progress.prop("value", val); // http://api.jquery.com/prop/#prop2
    return this;
};

Loadutils.prototype.loadpage = function(pname, args){

    if (this.currentpage!==pname) {
        this.currentpage=pname;
    } else {
        return;
    }

	var callback = args.callback;
	var initargs = args.initargs;
    if (! this.isGoodVar(pname) ) return false;
    // http://api.jquery.com/load/
    $(config.main_section_id)
            .load("pages/" + pname + ".html .page>*",function(){
                // -- better ideas?
                var pinit=window["page_"+pname+"_init"]; 
                if (pinit) {
                    pinit();
                    if(typeof(translate) !== "undefined"){
                    	translate(config.uilanguage);
                    }
                } else {
                    console.log("WARNING: page_"+pname+"_init is not defined.");
                }
                // --
                callback(initargs);
            });
    return this;
}; 

Loadutils.prototype.loadwidget = function(wname, $target, args){
	var callback = args.callback;
	var initargs = args.initargs;
    if (! this.isGoodVar(wname) ) return false;
    
    
    //TODO remove after develop
	    var tmpurl = "widgets/";
	    var req = new XMLHttpRequest();
	    req.open('GET', tmpurl+wname+".html" , false);
	    req.send();
	    if (req.status !== 200){
	    	tmpurl = "../widgets/";
	    }
    //TODO remove after develop 
    
    $target.load(tmpurl+wname+".html .widget",function(){
      // -- better ideas?
      var winit=window["widget_"+wname+"_init"]; 
      if (winit) {
        winit();
        if(typeof(translate) !== "undefined"){
        	translate(config.uilanguage);
        }
      } else {
          console.log("WARNING: widget_"+wname+"_init is not defined.");
      }
      // --
      if (callback) callback(initargs);
    });
    return this;
};

var loadutils = new Loadutils();
