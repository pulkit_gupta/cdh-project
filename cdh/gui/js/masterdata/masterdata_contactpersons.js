/*
 * widget to provide inputs for the contact person data
 *
 * editmode:
 * 	- method to control the editmode of the loaded widgets
 * 		-> standard is inactive
 *
 * list_elementSelectFunction:
 *  - function called if a list entry in the contact person list is selected
 *
 * list_elementUnselectFunction:
 *  - function called if a list entry in the contact person list is unselected
 *
 * createPersonList:
 *  - creates a list with all contact persons
 *
 * insertPersons:
 *  - insert the data from the personArray-object into the overviewList
 * 
 * updatePersonseditor:
 *  - read the selected entry from the overviewlist
 *    and fill in the corresponding data into the textfields
 *
 * init_stage2:
 *  - the secound init stage
 *  - handle the editmode and add click listeners to buttons
 *
 * init:
 *  - the init function which should be called by the loader
 *  - load the widgets
 *
 * new:
 *  - function called if the new button is clicked
 *
 * change:
 *  - function called if the change button is clicked
 *
 * cancel:
 *  - function called if the cancel button is clicked
 *
 * save:
 *  - function called if the save button is clicked
 */
var widget_masterdata_contactpersons_list = null;

/*
 * several content objects with demo-data
 */
var widget_masterdata_contactpersons_data_listhead = [
	{
		key : "salutation",
		text: "Anrede"
	},
	{
		key : "lastname",
		text: "Name"
	},
	{
		key : "position",
		text: "Position"
	},
	{
		key : "telephone",
		text: "Telefon"
	},
	{
		key : "mobilphone",
		text: "Mobiltelefon"
	},
	{
		key : "email",
		text: "Email"
	}																					
];

var widget_masterdata_contactpersons_data_listcontent = [
	{	
		identifier:1,
		salutation:"Herr",
		salutationkey:"mr",
		title:"no",
		surname:"Max",
		lastname:"Mustermann",
		position:"Chef",
		telephone:"0371/214567",
		fax:"",
		mobilphone:"",
		email:"info@mustermann.de",
		birthday:"01.01.1900",
		bithdaymailing:true,
		notes:"Hier stehen Notzien zu:\n - Max Mustermann"
	},
	{	
		identifier:2,
		salutationkey:"mr",
		salutation:"Herr",
		title:"no",
		surname:"Max",
		lastname:"Mustermann",
		position:"Chef",
		telephone:"2371/214567",
		fax:"",
		mobilphone:"",
		email:"info@mustermann.de",
		birthday:"01.01.1900",
		bithdaymailing:true,
		notes:"Hier stehen Notzien zu:\n - Max Mustermann"
	}												
] 	 

/*
 * editmode:
 *  - method which enables or disables input/selection fields
 *
 * enabled {boolean} selector if the editmode shall be enabled or not
 */
var widget_masterdata_contactpersons_editmode = function(enabled)
{
	enabled = !enabled;
	$("#widget_masterdata_contactpersons_button_new").prop('disabled', enabled);
	$("#widget_masterdata_contactpersons_button_change").prop('disabled', enabled);
	$("#widget_masterdata_contactpersons_button_save").prop('disabled', enabled);
	$("#widget_masterdata_contactpersons_button_cancel").prop('disabled', enabled);
}

/*
 * list_elementSelectFunction:
 *  - function called if a list entry in the adresslist is selected
 *  - identifier {int} the identifier of the dataset
 *  - $elemClicked {object} jQuery object which was clicked
 */
var widget_masterdata_contactpersons_list_elementSelectFunction = function(identifier, $elemClicked)
{
	//enable change button
	$("#widget_masterdata_contactpersons_button_change").prop('disabled', false);
	//fillin personeditor with the selected value in personslist
	widget_masterdata_contactpersons_updatePersonseditor();
}

/*
 * list_elementUnselectFunction:
 *  - function called if a list entry in the adresslist is unselected
 *  - identifier {int} the identifier of the dataset
 *  - $elemClicked {object} jQuery object which was clicked
 */
var widget_masterdata_contactpersons_list_elementUnselectFunction = function(identifier, $elemClicked)
{
	//clear values shown in personeditor and disbale editmode of the personeditor
	widget_masterdata_personeditor_clearValues();
	widget_masterdata_personeditor_editmode(false);
	//disable change button	
	$("#widget_masterdata_contactpersons_button_change").prop('disabled', true);
}

/*
 * createPersonList:
 *  - creates a personlist with all persons
 */
var widget_masterdata_contactpersons_createPersonList = function()
{
	//create overviewlist 
	widget_masterdata_contactpersons_list = new overviewlist(
	{
		target:"#widget_masterdata_contactpersons_listdiv > div.overviewlist_wrapper",
		resizable:true,
		selectFirst:true,
		autoresize:true,
		sortable:true,
		listElemSelectFunction:widget_masterdata_contactpersons_list_elementSelectFunction,
		listElemSelectFunctionArgs:null,
		listElemUnselectFunction:widget_masterdata_contactpersons_list_elementUnselectFunction,
		listElemUnselectFunctionArgs:null
	});
	//create headline of overviewlist	
	widget_masterdata_contactpersons_list.insertHeadLine(widget_masterdata_contactpersons_data_listhead, {callback:null});
	//create list content
	widget_masterdata_contactpersons_list.insertDataArray(widget_masterdata_contactpersons_data_listcontent, {callback:null});	
}

/*
 * insertPersons:
 *  - insert the data from the personArray-object into the overviewList
 *  - personArray {object} of the following format:
 *    {
 * 			identifier:{int},
 *			salutation:"",
 *			salutationkey:"",
 *			title:"",
 *			surname:"",
 *			lastname:"",
 *			position:"",
 *			telephone:"",
 *			fax:"",
 *			mobilphone:"",
 *			email:"",
 *			birthday:"",
 *			bithdaymailing:{bool},
 *			notes:""
 *    }
 */
var widget_masterdata_contactpersons_insertPersons = function(personArray)
{
	widget_masterdata_contactpersons_list.insertDataArray(personArray, {callback:widget_masterdata_contactpersons_updatePersonseditor});	
}

/*
 * updatePersonseditor:
 *  - read the selected entry from the overviewlist
 *    and fill in the corresponding data into the textfields
 */
var widget_masterdata_contactpersons_updatePersonseditor = function()
{
	// Problem - no entry is selected
	//console.debug("Personeditor - selected dataset: " + widget_masterdata_contactpersons_list.getSelectedData());
	
	widget_masterdata_personeditor_fillInValues(widget_masterdata_contactpersons_list.getSelectedData());
}

/*
 * init_stage2:
 *  - the second init stage
 *  - handle the editmode and add click listeners to buttons
 */
var widget_masterdata_contactpersons_init_stage2 = function()
{
	//disable editing of person editor
	widget_masterdata_personeditor_editmode(false);
	
	//fillin personeditor with the current selected value in personslist
	widget_masterdata_contactpersons_updatePersonseditor();
		
	//add clickfunctions to the buttons
	$("#widget_masterdata_contactpersons_button_new").click(function()
	{
		widget_masterdata_contactpersons_new();
	});
	
	$("#widget_masterdata_contactpersons_button_change").click(function()
	{
		widget_masterdata_contactpersons_change();
	});
	
	$("#widget_masterdata_contactpersons_button_cancel").click(function()
	{
		widget_masterdata_contactpersons_cancel();
	});
	
	$("#widget_masterdata_contactpersons_button_save").click(function()
	{
		widget_masterdata_contactpersons_save();
	});
}

/*
 * init:
 *  - the init function which should be called by the loader
 *  - load the widgets
 */
var widget_masterdata_contactpersons_init = function()
{
	console.log("widget_masterdata_contactpersons_init");
	//disable editmode
	widget_masterdata_contactpersons_editmode(true);
	//load listwidget to show all contactpersons
	loadutils.loadwidget("overviewlist", $("#widget_masterdata_contactpersons_listdiv"), {callback: widget_masterdata_contactpersons_createPersonList});
	//load personenesitor to show and edit persons
	loadutils.loadwidget("masterdata_personeditor", $("#widget_masterdata_contactpersons_editdiv"), {callback: widget_masterdata_contactpersons_init_stage2, initargs:null});
}

/*
 * new:
 *  - function called if the new button is clicked
 */
var widget_masterdata_contactpersons_new = function()
{
	//console.log("widget_masterdata_contactpersons_new");
	//hide new and change button and show save and cancel buttons
	$("#widget_masterdata_contactpersons_button_new").hide();
	$("#widget_masterdata_contactpersons_button_change").hide();
	$("#widget_masterdata_contactpersons_button_save").show();
	$("#widget_masterdata_contactpersons_button_cancel").show();
	//clear values shown in personeditor and enable editmode of the personeditor
	widget_masterdata_personeditor_clearValues();
	widget_masterdata_personeditor_editmode(true);
}

/*
 * change:
 *  - function called if the change button is clicked
 */
var widget_masterdata_contactpersons_change = function()
{
	//console.log("widget_masterdata_contactpersons_change");
	//hide new and change button and show save and cancel buttons
	$("#widget_masterdata_contactpersons_button_new").hide();
	$("#widget_masterdata_contactpersons_button_change").hide();
	$("#widget_masterdata_contactpersons_button_save").show();
	$("#widget_masterdata_contactpersons_button_cancel").show();
	//enable editmode of the personeditor
	widget_masterdata_personeditor_editmode(true);
}

/*
 * cancel:
 *  - function called if the cancel button is clicked
 */
var widget_masterdata_contactpersons_cancel = function()
{
	//console.log("widget_masterdata_contactpersons_cancel");
	//show new and change button and hide save and cancel buttons
	$("#widget_masterdata_contactpersons_button_save").hide();
	$("#widget_masterdata_contactpersons_button_cancel").hide();
	$("#widget_masterdata_contactpersons_button_new").show();
	$("#widget_masterdata_contactpersons_button_change").show();
	//disable editmode of the personeditor
	widget_masterdata_personeditor_editmode(false);
	//fillin personeditor with the old values stored in widget_masterdata_contactpersons_data_listcontent
	for (var i=0; i < widget_masterdata_contactpersons_data_listcontent.length; i++) 
	{
	  if (widget_masterdata_contactpersons_data_listcontent[i].identifier == widget_masterdata_contactpersons_list.getselectedEntryIdentifier())
	  {	  	
			widget_masterdata_personeditor_fillInValues(widget_masterdata_contactpersons_data_listcontent[i]);
	  }
	};
}

/*
 * save:
 *  - function called if the save button is clicked
 */
var widget_masterdata_contactpersons_save = function()
{
	//console.log("widget_masterdata_contactpersons_save");
	//show new and change button and hide save and cancel buttons
	$("#widget_masterdata_contactpersons_button_save").hide();
	$("#widget_masterdata_contactpersons_button_cancel").hide();
	$("#widget_masterdata_contactpersons_button_new").show();
	$("#widget_masterdata_contactpersons_button_change").show();
	//disable editmode of the personeditor
	widget_masterdata_personeditor_editmode(false);
	
	//TODO add function to save new data to database 
	//temporary save changes only in local var : widget_masterdata_contactpersons_data_listcontent
	var found = false;
	var tmpdata = widget_masterdata_personeditor_getValues();
	for (var i=0; i < widget_masterdata_contactpersons_data_listcontent.length; i++) 
	{
	  if (widget_masterdata_contactpersons_data_listcontent[i].identifier == tmpdata.identifier)
	  {	  	
			widget_masterdata_contactpersons_data_listcontent[i] = tmpdata;
			widget_masterdata_contactpersons_list.changeEntry(tmpdata, tmpdata.identifier);
			found = true;
	  }
	};
	
	if (!found)
	{
		tmpdata.identifier = "tmp_"+Math.floor((Math.random()*100)+1); 
		widget_masterdata_contactpersons_list.clearSelected();
		widget_masterdata_contactpersons_list.insertData(tmpdata, true, false);
		widget_masterdata_contactpersons_data_listcontent.push(tmpdata);
		//disable change button	
		$("#widget_masterdata_contactpersons_button_change").prop('disabled', true);
	}
	console.log(tmpdata.identifier)
	
	widget_masterdata_personeditor_clearValues();		
}