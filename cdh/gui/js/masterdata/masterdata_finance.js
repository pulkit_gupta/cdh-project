/*
 * widget to provide inputs for the finance information to masterdata-datasets
 *
 * editmode:
 * 	- method to control the editmode of the loaded widgets
 * 		-> standard is inactive
 *
 * clearValues:
 *	- method to delete all input
 * 
 * fillInValues:
 *	- method to fill in values of a provided data object
 *
 * getValues:
 *	- read all the values from the inputs and return them as a data object
 *
 * init:
 *  - the init function which should be called by the loader
 */

/*
 * a content object with demo-data
 */
var demoData_masterdata_finance = 
{	
	paymentcondition:"14 Tage 3%Skonto, 30 Tage netto",
	deliverycondition:"frei Haus",
	shippingtyp:"DHL Paket",
	shippingnote:"zu Hd. Herr Mustermann",
	debitorennumber:"12345",
	taxnumber:"277 227 227",
	accountholder:"Firma Mustermann",
	accountnumber:"12.123.12345",
	blz:"1234567",
	bank:"Musterbank",
	delegate:"Frau Musterfrau",
	catalogues:"keine",
	discount:"0%",
	discountnote:"dem Kunde wird kein Rabatt gewährt"
}

/*
 * editmode:
 *  - method which enables or disables input/selection fields
 *
 * enabled {boolean} selector if the editmode shall be enabled or not
 */
var widget_masterdata_finance_editmode = function(enabled)
{
	enabled = !enabled;
	//row left
	$("#widget_masterdata_finance_paymentcondition_input").prop('disabled', enabled);
	$("#widget_masterdata_finance_deliverycondition_input").prop('disabled', enabled);
	$("#widget_masterdata_finance_shippingtyp_input").prop('disabled', enabled);
	$("#widget_masterdata_finance_shippingnote_textarea").prop('disabled', enabled);
	
	//row center
	$("#widget_masterdata_finance_debitorennumber_input").prop('disabled', enabled);
	$("#widget_masterdata_finance_taxnumber_input").prop('disabled', enabled);
	$("#widget_masterdata_finance_accountholder_input").prop('disabled', enabled);
	$("#widget_masterdata_finance_accountnumber_input").prop('disabled', enabled);
	$("#widget_masterdata_finance_blz_input").prop('disabled', enabled);
	$("#widget_masterdata_finance_bank_input").prop('disabled', enabled);
	
	//row right
	$("#widget_masterdata_finance_delegate_input").prop('disabled', enabled);
	$("#widget_masterdata_finance_catalogues_input").prop('disabled', enabled);
	$("#widget_masterdata_finance_discount_input").prop('disabled', enabled);
	$("#widget_masterdata_finance_discountnote_textarea").prop('disabled', enabled);
}

/*
 * clearValues:
 *  - clear all inputs
 */
var widget_masterdata_finance_clearValues = function()
{
	//row left
	$("#widget_masterdata_finance_paymentcondition_input").val("");
	$("#widget_masterdata_finance_deliverycondition_input").val("");
	$("#widget_masterdata_finance_shippingtyp_input").val("");
	$("#widget_masterdata_finance_shippingnote_textarea").text("");
	
	//row center
	$("#widget_masterdata_finance_debitorennumber_input").val("");
	$("#widget_masterdata_finance_taxnumber_input").val("");
	$("#widget_masterdata_finance_accountholder_input").val("");
	$("#widget_masterdata_finance_accountnumber_input").val("");
	$("#widget_masterdata_finance_blz_input").val("");
	$("#widget_masterdata_finance_bank_input").val("");
	
	//row right
	$("#widget_masterdata_finance_delegate_input").val("");
	$("#widget_masterdata_finance_catalogues_input").val("");
	$("#widget_masterdata_finance_discount_input").val("");
	$("#widget_masterdata_finance_discountnote_textarea").text("");	
}

/*
 * fillInValues:
 *  - fills the input with values
 *  - data {object} of this format:
 *	{
 *		paymentcondition:{String},
 *		deliverycondition:{String},
 *		shippingtyp:{String},
 *		shippingnote:{String},
 *		debitorennumber:{String},
 *		taxnumber:{String},
 *		accountholder:{String},
 *		accountnumber:{String},
 *		blz:{String},
 *		bank:{String},
 *		delegate:{String},
 *		catalogues:{String},
 *		discount:{String},
 *		discountnote:{String}
 *	}
 */
var widget_masterdata_finance_fillInValues = function(data)
{
	//row left
	$("#widget_masterdata_finance_paymentcondition_input").val(data.paymentcondition);
	$("#widget_masterdata_finance_deliverycondition_input").val(data.deliverycondition);
	$("#widget_masterdata_finance_shippingtyp_input").val(data.shippingtyp);
	$("#widget_masterdata_finance_shippingnote_textarea").text(data.shippingnote);
	
	//row center
	$("#widget_masterdata_finance_debitorennumber_input").val(data.debitorennumber);
	$("#widget_masterdata_finance_taxnumber_input").val(data.taxnumber);
	$("#widget_masterdata_finance_accountholder_input").val(data.accountholder);
	$("#widget_masterdata_finance_accountnumber_input").val(data.accountnumber);
	$("#widget_masterdata_finance_blz_input").val(data.blz);
	$("#widget_masterdata_finance_bank_input").val(data.bank);
	
	//row right
	$("#widget_masterdata_finance_delegate_input").val(data.delegate);
	$("#widget_masterdata_finance_catalogues_input").val(data.catalogues);
	$("#widget_masterdata_finance_discount_input").val(data.discount);
	$("#widget_masterdata_finance_discountnote_textarea").text(data.discountnote);
}

/*
 * getValues:
 *  - reads the values of all inputs
 *	- returns {object} of the following format:
 *	{
 *		paymentcondition:{String},
 *		deliverycondition:{String},
 *		shippingtyp:{String},
 *		shippingnote:{String},
 *		debitorennumber:{String},
 *		taxnumber:{String},
 *		accountholder:{String},
 *		accountnumber:{String},
 *		blz:{String},
 *		bank:{String},
 *		delegate:{String},
 *		catalogues:{String},
 *		discount:{String},
 *		discountnote:{String}
 *	}
 */
var widget_masterdata_finance_getValues = function()
{
	var data = new Object;
	//row left
	data.paymentcondition = $("#widget_masterdata_finance_paymentcondition_input").val();
	data.deliverycondition = $("#widget_masterdata_finance_deliverycondition_input").val();
	data.shippingtyp = $("#widget_masterdata_finance_shippingtyp_input").val();
	data.shippingnote = $("#widget_masterdata_finance_shippingnote_textarea").text();
	
	//row center
	data.debitorennumber = $("#widget_masterdata_finance_debitorennumber_input").val();
	data.taxnumber = $("#widget_masterdata_finance_taxnumber_input").val();
	data.accountholder = $("#widget_masterdata_finance_accountholder_input").val();
	data.accountnumber = $("#widget_masterdata_finance_accountnumber_input").val();
	data.blz = $("#widget_masterdata_finance_blz_input").val();
	data.bank = $("#widget_masterdata_finance_bank_input").val();
	
	//row right
	data.delegate = $("#widget_masterdata_finance_delegate_input").val();
	data.catalogues = $("#widget_masterdata_finance_catalogues_input").val();
	data.discount = $("#widget_masterdata_finance_discount_input").val();
	data.discountnote = $("#widget_masterdata_finance_discountnote_textarea").text();
	return data;	
}

/*
 * init:
 *  - the init function which should be called by the loader
 */
var widget_masterdata_finance_init = function()
{
	console.log("widget_masterdata_finance_init");
	widget_masterdata_finance_clearValues();
	widget_masterdata_finance_editmode(false);
}