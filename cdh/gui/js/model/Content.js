/*
 * Content.js 
 *
 * contains:
 * ContentTree.*
 * ContentNode.*
 * model_helper_*
 *
 */

/**
 * createTimestamp
 * 
 * @description create Timestamp
 * @returns {Number}
 */
var model_helper_createTimestamp = function()
{
	return (new Date()).getTime();
};

/**
 * createGuid, credits: Robert Kieffer
 * 
 * @description create GUID
 * @returns {String}
 */
var model_helper_createGuid = function()
{
	return '{xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx}'.replace(/[xy]/g, function(c) { var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);return v.toString(16);}).toUpperCase();
};

/**
 * createTransactionID
 * 
 * @description create GUID
 * @returns {String}
 */
var model_helper_createTransactionID = function()
{
	return model_helper_createGuid();
};

/**
 * stringify
 * 
 * @description stringify object as JSON
 */
var model_helper_stringify = function(object)
{
	return JSON.stringify(object, function(key, value) {
		if (key=="parent"&&value!==null) {
			return null;
		}
		if (key=="isMapped") {
			return null;
		}
		return value;
	});
};

/**
 * @class ContentTree
 * @constructor
 */
function ContentTree()
{
	this.className = "ContentTree";
	// GUID
	this.guid = model_helper_createGuid(); // unique ID
	// attributes and default values
	this.children = []; // list of children objects
	this.style = {}; // {KeyValue}
	// set transaction info
	this.update();
}

/**
 * map
 * 
 * @param {Object} rawObject
 * @param {Object} parentObject
 * @returns this
 */
ContentTree.prototype.map = function(rawObject, parentObject)
{
	for (e in rawObject) {
		if (rawObject.hasOwnProperty(e) && rawObject[e]) {
			this[e] = rawObject[e];
		}
	}
	return this;
};

/**
 * update
 */
ContentTree.prototype.update = function()
{
	this.transactionID = model_helper_createTransactionID(); // timestamp of last modification
	this.timeStamp = model_helper_createTimestamp(); // unique transaction ID
	this.userID = "default"; // @TODO
};

/**
 * hasStyleProperty
 * 
 * @param {String} propertyName
 * @returns {boolean}
 */
ContentTree.prototype.hasStyleProperty = function(propertyName)
{
	return this.style.hasOwnProperty(propertyName);
};

// getters

/**
 * getStyleProperty
 * 
 * @param {String} propertyName
 * @returns {Object}
 */
ContentTree.prototype.getStyleProperty = function(propertyName)
{
	return this.style[propertyName];
};

/**
 * getChildren
 * 
 * @returns {Array}
 */
ContentTree.prototype.getChildren = function()
{
	return this.children;
};

/**
 * getGuid
 * 
 * @returns {String}
 */
ContentTree.prototype.getGuid = function()
{
	return this.guid;
};

/**
 * getTimeStamp
 * 
 * @returns {Number}
 */
ContentTree.prototype.getTimeStamp = function()
{
	return this.timeStamp;
};

/**
 * getTransactionID
 * 
 * @returns {String}
 */
ContentTree.prototype.getTransactionID = function()
{
	return this.transactionID;
};

/**
 * getUserID
 * 
 * @returns {String}
 */
ContentTree.prototype.getUserID = function()
{
	return this.userID;
};

/**
 * toList
 * 
 * @description get flat list of (unnamed) children
 * @returns {Array}
 */
ContentTree.prototype.toList = function()
{
	var i;
	var c = this.children;
	var list = new Array();
	if (this instanceof ContentNode) {
		list.push(this);
	}
	for (i = 0; i < c.length; i++) {
		list = list.concat(c[i].toList());
	}
	return list;
};

// setters

/**
 * setStyleProperty
 * 
 * @param {String} propertyName
 * @param {String} propertyValue
 */
ContentTree.prototype.setStyleProperty = function(propertyName, propertyValue)
{
	this.style[propertyName] = propertyValue;
	this.update();
};

/**
 * add Child
 * 
 * @description example: addChild(child, { prev: prevChild })
 * @param {ContentNode} child
 * @param {Object} position
 */
ContentTree.prototype.addChild = function(child, position)
{
	if (position) {
		var i;
		for (i = 0; i < this.children.length; i++) {
			if (position.prev && (this.children[i] == position.prev)) {
				this.children.splice(i + 1, 0, child);
				break;
			}
			if (position.next && (this.children[i] == position.next)) {
				this.children.splice(i, 0, child);
				break;
			}
		}
	} else {
		this.children.push(child);
	}
	child.setParent(this);
	this.update();
};

/**
 * removeChild
 * 
 * @param {ContentNode} child
 */
ContentTree.prototype.removeChild = function(child)
{
	var i;
	var c = this.children;
	for (i = 0; i < c.length; i++) {
		if (c[i] == child) {
			c.splice(i, 1);
			break;
		}
	}
	child.setParent(null);
	this.update();
};

/**
 * @class ContentNode
 * @constructor
 */
function ContentNode()
{
	ContentTree.call(this);
	this.className = "ContentNode";
	this.parent = null;
};

/**
 * __proto__
 */
ContentNode.prototype.__proto__ = ContentTree.prototype;

/**
 * map
 * 
 * @param {Object} rawObject
 * @param {Object} parentObject
 * @returns this
 */
ContentNode.prototype.map = function(rawObject, parentObject)
{
	ContentTree.prototype.map.call(this, rawObject, parentObject);
	for (e in rawObject) {
		if (rawObject.hasOwnProperty(e) && rawObject[e] === null && e == "parent") {
			if (parentObject.hasOwnProperty("root")) {
				this[e] = parentObject.root;
			}
			else {
				this[e] = parentObject;
			}
		}
	}
	return this;
};

/**
 * update
 */
ContentNode.prototype.update = function()
{
	ContentTree.prototype.update.call(this);
	if (this.parent && this.parent.update) {
		this.parent.update();
	}
};

// getters

/**
 * getParent
 * 
 * @returns {ContentTree}
 */
ContentNode.prototype.getParent = function()
{
	return this.parent;
};

// setters

/**
 * setParent
 * 
 * @param {ContentTree} value
 */
ContentNode.prototype.setParent = function(value)
{
	this.parent = value;
	this.update();
};