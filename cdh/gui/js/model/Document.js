/*
 * Document.js
 * 
 * contains:
 * DocumentContent.*
 * LetterContent.*
 * LetterBodyContent.*
 * LetterFooterContent.*
 * LetterHeaderContent.*
 * QuoteContent.*
 * 
 */

/**
 * @class DocumentContent
 * @constructor
 */
function DocumentContent()
{
	ContentTree.call(this);
	this.className = "DocumentContent";
}

/**
 * __proto__
 */
DocumentContent.prototype.__proto__ = ContentTree.prototype;

/**
 * @class LetterContent
 * @constructor
 */
function LetterContent()
{
	DocumentContent.call(this);
	this.className = "LetterContent";
	// attributes and default values
	this.date = null; // {DateTime}
	// named children
	this.body = null; // {LetterBodyContent}
	this.footer = null; // {LetterFooterContent}
	this.header = null; // {LetterHeaderContent}
}

/**
 * __proto__
 */
LetterContent.prototype.__proto__ = DocumentContent.prototype;

// getters

/**
 * getDate
 * 
 * @returns {DateTime}
 */
LetterContent.prototype.getDate = function()
{
	return this.date;
};

/**
 * getBody
 * 
 * @returns {LetterBodyContent}
 */
LetterContent.prototype.getBody = function()
{
	return this.body;
};

/**
 * getFooter
 * 
 * @returns {LetterFooterContent}
 */
LetterContent.prototype.getFooter = function()
{
	return this.footer;
};

/**
 * getHeader
 * 
 * @returns {LetterHeaderContent}
 */
LetterContent.prototype.getHeader = function()
{
	return this.header;
};

// setters

/**
 * setDate
 * 
 * @param {DateTime} value
 */
LetterContent.prototype.setDate = function(value)
{
	this.date = value;
	this.update();
};

/**
 * setBody
 * 
 * @param {LetterBodyContent} value
 */
LetterContent.prototype.setBody = function(value)
{
	this.body = value;
};

/**
 * setFooter
 * 
 * @param {LetterFooterContent} value
 */
LetterContent.prototype.setFooter = function(value)
{
	this.footer = value;
};

/**
 * setHeader
 * 
 * @param {LetterHeaderContent} value
 */
LetterContent.prototype.setHeader = function(value)
{
	this.header = value;
};

/**
 * @class LetterBodyContent
 * @constructor
 */
function LetterBodyContent()
{
	ContentNode.call(this);
	this.className = "LetterBodyContent";
}

/**
 * __proto__
 */
LetterBodyContent.prototype.__proto__ = ContentNode.prototype;

// getters

// setters

/**
 * @class LetterFooterContent
 * @constructor
 */
function LetterFooterContent()
{
	ContentNode.call(this);
	this.className = "LetterFooterContent";
	// attributes and default values
	this.pageNumber = null; // {number}
}

/**
 * __proto__
 */
LetterFooterContent.prototype.__proto__ = ContentNode.prototype;

// getters

/**
 * getPageNumber
 *
 * @returns {number}
 */
LetterFooterContent.prototype.getPageNumber = function() {
	return this.pageNumber;
};

// setters

/**
 * setPageNumber
 *
 * @param {number} value
 */
LetterFooterContent.prototype.setPageNumber = function(value) {
	this.pageNumber = value;
};

/**
 * @class LetterHeaderContent
 * @constructor
 */
function LetterHeaderContent()
{
	ContentNode.call(this);
	this.className = "LetterHeaderContent";
	// attributes and default values
	this.pageNumber = null; // {number}
}

/**
 * __proto__
 */
LetterHeaderContent.prototype.__proto__ = ContentNode.prototype;

// getters

/**
 * getPageNumber
 *
 * @returns {number}
 */
LetterHeaderContent.prototype.getPageNumber = function() {
	return this.pageNumber;
};

// setters

/**
 * setPageNumber
 *
 * @param {number} value
 */
LetterHeaderContent.prototype.setPageNumber = function(value) {
	this.pageNumber = value;
};

/**
 * @class QuoteContent
 * @constructor
 */
function QuoteContent()
{
	LetterContent.call(this);
	this.className = "QuoteContent";
	// named children
	this.customer = null; // {CustomerContent}
	this.itemList = null; // {ItemListContent}
}

/**
 * __proto__
 */
QuoteContent.prototype.__proto__ = LetterContent.prototype;

// getters

/**
 * getCustomer
 * 
 * @returns {CustomerContent}
 */
QuoteContent.prototype.getCustomer = function()
{
	return this.customer;
};

/**
 * getItemList
 * 
 * @returns {ItemListContent}
 */
QuoteContent.prototype.getItemList = function()
{
	return this.itemList;
};

/**
 * @class DocumentElementContent
 * @constructor
 */
function DocumentElementContent()
{
	ContentNode.call(this);
	this.className = "DocumentElementContent";
}

/**
 * __proto__
 */
DocumentElementContent.prototype.__proto__ = ContentNode.prototype;

// getters

// setters

/**
 * @class TextElementContent
 * @constructor
 */
function TextElementContent()
{
	DocumentElementContent.call(this);
	this.className = "TextElementContent";
	this.text = null;
}

/**
 * __proto__
 */
TextElementContent.prototype.__proto__ = DocumentElementContent.prototype;

// getters

/**
 * getText
 * 
 * @returns {String}
 */
TextElementContent.prototype.getText = function()
{
	return this.text;
};

// setters

/**
 * setText
 * 
 * @param {String} value
 */
TextElementContent.prototype.setText = function(value)
{
	this.text = value;
	this.update();
};
