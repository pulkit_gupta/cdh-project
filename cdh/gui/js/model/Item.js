/*
 * Item.js
 * 
 * contains:
 * ItemListContent.*
 * ItemListHeaderContent.*
 * ItemContent.*
 * ProductContent.*
 * VariantContent.*
 * 
 */

/**
 * @class ItemListContent
 * @constructor
 */
function ItemListContent()
{
	ContentNode.call(this);
	this.className = "ItemListContent";
	// named children
	this.header = null; // {ItemListHeaderContent}
}

/**
 * __proto__
 */
ItemListContent.prototype.__proto__ = ContentNode.prototype;

// getters

/**
 * getHeader
 * 
 * @returns {ItemListHeaderContent}
 */
ItemListContent.prototype.getHeader = function()
{
	return this.header;
};

/**
 * @class ItemListHeaderContent
 * @constructor
 */
function ItemListHeaderContent()
{
	ContentNode.call(this);
	this.className = "ItemListHeaderContent";
	this.defaultValue = null; // {String}
	this.text = null; // {String}
	this.type = null; // {String}
}

/**
 * __proto__
 */
ItemListHeaderContent.prototype.__proto__ = ContentNode.prototype;

// getters

/**
 * getDefaultValue
 * 
 * @returns {String}
 */
ItemListHeaderContent.prototype.getDefaultValue = function()
{
	return this.defaultValue;
};

/**
 * getText
 * 
 * @returns {String}
 */
ItemListHeaderContent.prototype.getText = function()
{
	return this.text;
};

/**
 * getType
 * 
 * @returns {String}
 */
ItemListHeaderContent.prototype.getType = function()
{
	return this.type;
};

// setters

/**
 * setDefaultValue
 * 
 * @param {String} value
 */
ItemListHeaderContent.prototype.setDefaultValue = function(value)
{
	this.defaultValue = value;
	this.update();
};

/**
 * setText
 * 
 * @param {String} value
 */
ItemListHeaderContent.prototype.setText = function(value)
{
	this.text = text;
	this.update();
};

/**
 * setType
 * 
 * @param {String} value
 */
ItemListHeaderContent.prototype.setType = function(value)
{
	this.type = value;
	this.update();
};

/**
 * @class ItemContent
 * @constructor
 */
function ItemContent()
{
	ContentNode.call(this);
	this.className = "ItemContent";
	this.discount = null; // {Number}
	this.document = null; // {ContentTree}
	this.product = null; // {ProductContent}
	this.quantity = null; // {Number}
	this.tax = null; // {Number}
}

/**
 * __proto__
 */
ItemContent.prototype.__proto__ = ContentNode.prototype;

// getters

/**
 * getDiscount
 * 
 * @returns {Number}
 */
ItemContent.prototype.getDiscount = function()
{
	return this.discount;
};

/**
 * getDocument
 * 
 * @returns {ContentTree}
 */
ItemContent.prototype.getDocument = function()
{
	return this.document;
};

/**
 * getProduct
 * 
 * @returns {ProductContent}
 */
ItemContent.prototype.getProduct = function()
{
	return this.product;
};

/**
 * getQuantity
 * 
 * @returns {Number}
 */
ItemContent.prototype.getQuantity = function()
{
	return this.quantity;
};

/**
 * getTax
 * 
 * @returns {Number}
 */
ItemContent.prototype.getTax = function()
{
	return this.tax;
};

// setters

/**
 * setDiscount
 * 
 * @param {Number} value
 */
ItemContent.prototype.setDiscount = function(value)
{
	this.discount = value;
	this.update();
};

/**
 * setDocument
 * 
 * @param {ContentTree} value
 */
ItemContent.prototype.setDocument = function(value)
{
	this.document = value;
	this.update();
};

/**
 * setProduct
 * 
 * @param {Product} value
 */
ItemContent.prototype.setProduct = function(value)
{
	this.product = value;
	this.update();
};

/**
 * setQuantity
 * 
 * @param {Number} value
 */
ItemContent.prototype.setQuantity = function(value)
{
	this.quantity = value;
	this.update();
};

/**
 * setTax
 * 
 * @param {Number} value
 */
ItemContent.prototype.setTax = function(value)
{
	this.tax = value;
	this.update();
};

/**
 * @class ProductContent
 * @constructor
 */
function ProductContent()
{
	ContentNode.call(this);
	this.className = "ProductContent";
	this.artId = null; // {String}
	this.currency = null; // {String}
	this.name = null; // {String}
	this.thumbnail = null; // {String}
	this.unitPrice = null; // {Number}
	this.unitType = null; // {String}
}

/**
 * __proto__
 */
ProductContent.prototype.__proto__ = ContentNode.prototype;

// getters

/**
 * getArtId
 * 
 * @returns {String}
 */
ProductContent.prototype.getArtId = function()
{
	return this.artId;
};

/**
 * getCurrency
 * 
 * @returns {String}
 */
ProductContent.prototype.getCurrency = function()
{
	return this.currency;
};

/**
 * getName
 * 
 * @returns {String}
 */
ProductContent.prototype.getName = function()
{
	return this.name;
};

/**
 * getThumbnail
 * 
 * @returns {String}
 */
ProductContent.prototype.getThumbnail = function()
{
	return this.thumbnail;
};

/**
 * getUnitPrice
 * 
 * @returns {Number}
 */
ProductContent.prototype.getUnitPrice = function()
{
	return this.unitPrice;
};

/**
 * getUnitType
 * 
 * @returns {String}
 */
ProductContent.prototype.getUnitType = function()
{
	return this.unitType;
};

// setters

/**
 * setArtId
 * 
 * @param {String} value
 */
ProductContent.prototype.setArtId = function(value)
{
	this.artId = value;
	this.update();
};

/**
 * setCurrency
 * 
 * @param {String} value
 */
ProductContent.prototype.setCurrency = function(value)
{
	this.currency = value;
	this.update();
};

/**
 * setName
 * 
 * @param {String} value
 */
ProductContent.prototype.setName = function(value)
{
	this.name = value;
	this.update();
};

/**
 * setThumbnail
 * 
 * @param {String} value
 */
ProductContent.prototype.setThumbnail = function(value)
{
	this.thumbnail = value;
	this.update();
};

/**
 * setUnitPrice
 * 
 * @param {Number} value
 */
ProductContent.prototype.setUnitPrice = function(value)
{
	this.unitPrice = value;
	this.update();
};

/**
 * setUnitType
 * 
 * @param {String} value
 */
ProductContent.prototype.setUnitType = function(value)
{
	this.unitType = value;
	this.update();
};

/**
 * @class VariantContent
 * @constructor
 */
function VariantContent()
{
	ContentNode.call(this);
	this.className = "VariantContent";
	this.product = null; // {ProductContent}
}

/**
 * __proto__
 */
VariantContent.prototype.__proto__ = ContentNode.prototype;

// getters

/**
 * getProduct
 * 
 * @returns {ProductContent}
 */
VariantContent.prototype.getProduct = function()
{
	return this.product;
};

// setters

/**
 * setProduct
 * 
 * @param {Product} value
 */
VariantContent.prototype.setProduct = function(value)
{
	this.product = value;
	this.update();
};
