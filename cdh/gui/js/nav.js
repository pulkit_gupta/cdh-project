/*
 * element_mouseenter:
 *  - mapped by init to the mouseenter event of each menu entry
 *  - overwrites the tooltip text and shows it
 * 
 * mouseleave:
 *  - mapped by init to the mouseleave event of the whole nav bar
 *  - hides the tooltip
 * 
 * toggle:
 *  - shows or hides a submenu
 *  - parameters: 
 *      - name of the submenu
 *      - show (true) or hide (false) it
 *  -> if just the name is passed, the menu will became hidden
 * 
 * 
 */

var widget_nav_element_mouseenter = function()
{
	$("#nav_tooltip").hide().text($(this).prop("tooltip")).fadeIn(100);
};

var widget_nav_mouseleave = function()
{
	$("#nav_tooltip").fadeOut(300);
};

var widget_nav_toggle = function(submenu, show)
{
	if (!show) show = 0;
	if (!loadutils.isGoodVar(submenu)) return false;

	var $el = $("#nav_sub" + submenu);

	if (show && $el.hasClass("hidden")) $el.removeClass("hidden");

	if (!show && !$el.hasClass("hidden")) $el.addClass("hidden");
};

var widget_nav_defaultaction = function(e)
{
	e.preventDefault();
	var text = $(this).prop("tooltip");
	/*widget_notification_display(text, false, function()
	{
	});*/
    widget_notification_display({id: text+"_id", title: text});

	return false;
};

// experimental db integration. names are not right
// also, this may belong somewhere else

var widget_nav_loadsettings = function()
{
	/* obsolete!
	Database.query("nav_settings", {
		"ident" : "1"
	}, function(e)
	{
		console.log(e);
	});
	*/

};

/*
 * var widget_nav_savesettings = function(){ database_put(cdhDwr_Nav.setConfig,{param:[7,"setting7"], cbSuccess: function(result){ console.log("result:" + result); }}); };
 */


/// binds a function to click-event of an object; last row of callback MUST be >> return false << (without << and >>) so the site won't reload immediatly.
var widget_nav_bind = function(entry, callback)
{   
    // workaround: when div#nav isn't yet initialized try again later
    if ($("#nav").length>0){
        $(".nav_subnav li a#nav_" + entry).unbind("click");
        $(".nav_subnav li a#nav_" + entry).bind("click", callback);
    }else{
        window.setTimeout(function(){
            widget_nav_bind(entry,callback);
        },200);
    }
};

var widget_nav_init = function()
{
    Database.query("settings_receivenav", {}, function (ret) {
        if (ret.children.length<1 || !ret.children[0].nav)
        {
            widget_nav_init2();
            return;
        }
            
        var navfunc=[];
        $nav=$(".nav_subnav li a");
        for (i = 0; i < $nav.length; i++)
        {
            if (typeof(jQuery._data(document.getElementById($nav[i].id), "events"))!="undefined")
            {
                navfunc[$nav[i].id]=jQuery._data(document.getElementById($nav[i].id), "events").click[0].handler;
                //console.log(jQuery._data(document.getElementById($nav[i].id), "events").click[0].handler);
            }
            else
            {
                navfunc[$nav[i].id]=widget_nav_defaultaction;
            }
        }
        elem=ret.children[0].nav;
            
        $main=$("#nav_submain").empty();
        for (i=0; i<elem.main.length; i++){
            $main.append(elem.main[i]);
        }
        $main=$("#nav_suboutput").empty();
        for (i=0; i<elem.output.length; i++){
            $main.append(elem.output[i]);
        }
        $main=$("#nav_subpages").empty();
        for (i=0; i<elem.pages.length; i++){
            $main.append(elem.pages[i]);
        }
        $main=$("#nav_subdocument").empty();
        for (i=0; i<elem.document.length; i++){
            $main.append(elem.document[i]);
        }
        $main=$("#nav_subedit").empty();
        for (i=0; i<elem.edit.length; i++){
            $main.append(elem.edit[i]);
        }
        $main=$("#nav_submasterdata_main").empty();
        for (i=0; i<elem.masterdata_main.length; i++){
            $main.append(elem.masterdata_main[i]);
        }
        $main=$("#nav_submasterdata_context").empty();
        for (i=0; i<elem.masterdata_context.length; i++){
            $main.append(elem.masterdata_context[i]);
        }
        $main=$("#nav_subclientoverview_main").empty();
        for (i=0; i<elem.clientoverview_main.length; i++){
            $main.append(elem.clientoverview_main[i]);
        }
        $main=$("#nav_subclientoverview_context").empty();
        for (i=0; i<elem.clientoverview_context.length; i++){
            $main.append(elem.clientoverview_context[i]);
        }

        $nav=$(".nav_subnav li a");
        for (i = 0; i < $nav.length; i++)
        {
            $("#"+$nav[i].id).click(navfunc[$nav[i].id]);
        }
        
        setTimeout(widget_nav_init2, 1);
    });
};

var widget_nav_translate=function () {
    var gonethrough=true;
    $(".nav_subnav li a").each(function(e)
    {
        var $el = $($(".nav_subnav li a")[e]);
        var answer=translator.ask($(".nav_subnav li a")[e].id, "", false);
        if (answer==false)
            gonethrough=false;
        else
        {
            $el.prop("tooltip", answer);
        }
    });
    
    if (gonethrough==false)
    {
        window.setTimeout(widget_nav_translate, 1000);
    }
}

var widget_nav_init2=function() {
    // map event of each element, store tooltip	
    $(".nav_subnav li a").each(function(e)
    {
        var $el = $($(".nav_subnav li a")[e]);
        $el.prop("tooltip", $el.text());
        $el.mouseenter(widget_nav_element_mouseenter);
    });
    
    widget_nav_translate();

    // clear the text
    $(".nav_subnav li a").text("");

    // set elements not! sortable
    $(".nav_subnav").sortable({
        tolerance : 'pointer',
        revert : true,
        distance : 16
    });	
    $(".nav_subnav").sortable("disable");


    // events
    $("#nav").mouseleave(widget_nav_mouseleave);


    $nav=$(".nav_subnav li a");
    for (i = 0; i<$nav.length; i++)
    {
        if (typeof(jQuery._data(document.getElementById($nav[i].id), "events").click)=="undefined")
        {
            $("#"+$nav[i].id).unbind("click").click(widget_nav_defaultaction);
        }
    }

    widget_nav_bind('masterdata', function()
    {
        // this is possible because the hashchange has been bound to an event
        window.location.hash = "!masterdata";
        return false;
    });
    widget_nav_bind('clientoverview', function()
    {
        // this is possible because the hashchange has been bound to an event
        window.location.hash = "!clientoverview";
        return false;
    });

    widget_nav_bind("shutdown", function(){ // bind logout event to button
        session_id_clear();
        location.reload();
    });
    
    widget_nav_bind("reload", function(){ // bind reload event to button
        location.reload();
    });
}
