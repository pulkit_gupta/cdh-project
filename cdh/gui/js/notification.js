/*
add_new_element(parent, child, type)
It adds the child of a type from html dummy in parent with id.

widget_notification_populate_box(obj, boxid)
-> again a helper function used by display function to populate the html elements in a box with value in db structure

widget_notification_display(obj)
Calls to display popup with obj

*/

var $widget_notification_popup_wrapper;
var widget_notification_show=true;

var structure = {id: "sample_id", title: "Hello",
            columns: 4,
            rows: 2,
            elements: [                            
                {id:"choose_number",class_name: "c1", type: "select", options: {1: "choice_1", 2: "choice_2", 3: "choice_3"}},                
                {id:"show_hints_notification",class_name: "c2", type: "button", name: "button1", value: "clickme", onclick_event: function(){alert("clicked");}},
                {id:"id2",class_name: "c2", type: "label", text_label: "Number" },
                {id:"c3",class_name: "c2", type: "checkbox", ischecked: true},
                {id:"c4",class_name: "c2", type: "checkbox", ischecked: false},
                {id:"c5",class_name: "c2", type: "checkbox", ischecked: true},
                {id: "b4", class_name: "c4", type: "button", value: "button1", event: function(id){ alert("clicked button with id = "+id);}},
                {id: "t2", class_name: "c4", type: "textfield", value: "enter here"}
            ]              
};


//Helper function to add child new element with type in dummy html

var add_new_element = function(parent, child_id, type)
{
    var dummy_div = "#widget_notification_dummy_"+type;
    $dummy_element = $(dummy_div);
    if($dummy_element.length<=0)
    {
        console.log("error!! can't find parent with id"+dummy_div);
    }
    
    var html = $dummy_element.html();   
    $("#"+parent).append(html);         
    $("#widget_notification_new_"+type).eq(0).attr("id", child_id);         
    return child_id;    
}

/*
operations: displays the object in form of popups
*/
var widget_notification_display = function(obj) {     

    //please note that in production later on during finalization,  temp will be removed instead obj.id will be used.
    console.log("display called");    
    $widget_notification_popup_wrapper.show();
    if (widget_notification_show)
    {                             
        var boxID= "widget_notification_box_"+obj.id;    
        if(boxID.substr(0,boxID.indexOf(' '))!="")
        {
            boxID = boxID.substr(0,boxID.indexOf(' '));
        }

        console.log("child id is: "+boxID);
        if($( "#widget_notification_wrapper" ).find( "#"+boxID).length>0)
        {
            console.log("Already there!! Two box with same IDs.please check!");                
        }        
        else
        {
            console.log("creating box");
            //creating a new empty box
            add_new_element("widget_notification_wrapper",boxID, "box" );            
            $("#widget_notification_box_new_close").eq(0).attr("id", boxID+"_close");

            $("#"+boxID+"_close").click(function(){
                console.log("clicked x button of "+boxID);
                widget_notification_close(boxID);                                
            });

            $("#widget_notification_box_new_title").eq(0).attr("id", boxID+"_title").html(obj.title);
            
            widget_notification_populate_box(obj, boxID);
        }
                        
    }  //if boolean
        console.log("display ended");

};


/*
Function name: widget_notification_populate_box
input: JSON structure with element of notification box popup.
return: Null
operation: form the grid layout and fill the details from databse in the particular box
*/
var widget_notification_populate_box = function(obj, box_id)
{   
 //function can be shortened, if we run all populating statements without checking the element in the JSON from database.
    var elements = obj.elements;
    var no_col = obj.columns;
    var no_row = obj.rows;
    for(var i =0; i<no_row;i++)
    {
        add_new_element(box_id, box_id+"_row"+String(i+1), "row");
        for(var j =0; j<no_col;j++)
        {
            var id_element = add_new_element(box_id+"_row"+String(i+1), box_id+"_"+elements[i*no_col+j].id, elements[i*no_col+j].type);                        
                        
            if(elements[i*no_col+j].type === "label")    
            {
                $("#"+id_element).html(elements[i*no_col+j].text_label);
            }        
            
            else if(elements[i*no_col+j].type === "button")    
            {   
                var $button = $("#"+id_element);             
                $button.attr('value', elements[i*no_col+j].value);
                $button.click(elements[i*no_col+j].event);
            }              

            else if(elements[i*no_col+j].type === "checkbox")    
            {                                
                $("#"+id_element).prop('checked',elements[i*no_col+j].ischecked);
                //$("#"+id_element).attr('checked','checked');  //this can be used
            }  
            else if(elements[i*no_col+j].type === "select")    
            {                                
                $.each(elements[i*no_col+j].options, function(val, text) {                                        
                    $("#"+id_element).append(
                        $('<option></option>').val(val).html(text)
                    );
                });                
            }
            else if(elements[i*no_col+j].type === "textfield")    
            {                
                $("#"+id_element).attr('value', elements[i*no_col+j].value);
            }      
            else if(elements[i*no_col+j].type === "textfield")    
            {

            }
        }   
    }    
}

/*
operation: close the notification by removing that box related to that id

Also check the empty notification wrapper and hides it.
*/
var widget_notification_close = function(id){
    
    var $box = $("#"+id);
    $box.remove();   
    //check if all notification boxes are closed
    if($( "#widget_notification_wrapper" ).find( ".widget_notification_box_popup").length===0)
    {
        $widget_notification_popup_wrapper.hide();
    }
};


/*
Initiaize and make popup visible
*/
var widget_notification_init = function(){
    console.log("initializing notification");
    widget_notification_show = true;
     $notify = $("#notification");    
     $notify.show();

     /*uncomment the statements below for final run*/

    $("#notification_box_test").remove();    
    $widget_notification_popup_wrapper = $("#widget_notification_wrapper");
     $widget_notification_popup_wrapper.hide();
    //$notify.hide();    
};
