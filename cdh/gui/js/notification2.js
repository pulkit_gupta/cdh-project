
var $notify;
var notifyws;
show_notifications=true;
var not_active =0;

var widget_notification_display = function(message, isoverlay, onclick) {
    if (show_notifications)
    {
        //$notify.hide();
        //$notify.removeClass("overlay");
        //$notify.removeClass("popup");
        loadutils.loadwidget("notification_box", $("#notification_wrapper"), {
        callback : null
        })
        document.getElementById("notification_box").style.top="100px";
        if (isoverlay) {
            $notify.addClass("overlay");
        } else {
            $notify.addClass("popup");
        }
        
        $("#notification_text").text(message);
        
        // unbind old click events
        $("#notification_box").unbind("click");
        $("#notification_box").removeClass("clickable");
        
        if (arguments.length>2) {
            $("#notification_box").addClass("clickable");
            $("#notification_box").click(function(){
                widget_notification_close();
                onclick();
            });
        }
        
        $notify.show();
    }
};

var widget_notification_close = function(){
    $notify.hide();
};


var widget_notification_init = function(){
    $notify = $("#notification");
	
    $notify.hide();
    
    $("#notification_close").click(function(e){
        e.stopPropagation();
        widget_notification_close();
    });
};
