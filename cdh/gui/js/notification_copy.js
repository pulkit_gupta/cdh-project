
var $notify;
var notifyws;
show_notifications=true;
var restrict_multi_overlay = false;
var not_active =0;

var structure = {id: "select", title: "Title",
            columns: 2,
            rows: 2,
            elements: [
            //{id:"id3",class_name: "c2", type: "select", options: [{display: "computer", value: 'ap'}, {display: "laptop", value: '2'}]},
                {id:"choose_number",class_name: "c1", type: "select", options: [{display: "one", value: '1'}, {display: "two", value: '2'}]},
                //{id:"id2",class_name: "c2", type: "label", text_label: "number" },
                {id:"show_hints_notification",class_name: "c2", type: "input", subtype: "button", name: "button1", value: "clickme", onclick_event: function(){alert("clicked");}},
                {id:"show_hints_notification",class_name: "c2", type: "input", subtype: "checkbox", ischecked: ""},
                {id: "id4", class_name: "c4", type: "label", text_label: "sub", element: {id:"id5",class_name: "c2", type: "input", subtype: "checkbox"}}
            ]              
};

var pos=0;
var open = [0,0,0,0,0];


//still incomplete
var getValue_notification = function(parent_id, field_id)
{
    var field = document.getElementById("notification_box_"+parentid+"_"+field_id);    
    
}


//may give error in some cases. debugging in process
var notification_bind_event = function(field_id, click_event, box_type)    //popup or overlay
{                                         
    $('#'+field_id).addClass("notification_box_clickable");            
    $('#'+field_id).click(function(){
        console.log("clicked"+field_id);
        widget_notification_close(field_id,"popup");
        if(box_type==="overlay")
        {
         restrict_multi_overlay = false;
        }        
        clickevent();
    });
    
}

//Helper function for widget_notification_display
function getElementInsideContainer(containerID, childID) {
    var elm = document.getElementById(childID);
    var parent = elm ? elm.parentNode : {};
    return (parent.id && parent.id === containerID) ? elm : {};
}

var widget_notification_display = function(obj, isoverlay) {
    var box_type = isoverlay?"overlay":"popup";
    console.log(box_type);
    if (show_notifications)
    {                    
        var idName= "notification_box_"+obj.id;        
        if(getElementInsideContainer("notification_wrapper", idName).id===idName)
        {
            console.log("Already there");                
        }        
        else if(!restrict_multi_overlay)
        {
            //creating a new box
            var parent=document.getElementById("notification_wrapper");
            parent.className = "notification_wrapper_"+box_type;
            var divBox=document.createElement("div");
            divBox.id = idName;        
            //add to parent        
            parent.appendChild(divBox);        

            //change position                        
            for(var i=0;i<open.length;i++){                
                pos = i;
                if(open[pos]==0){
                    break;
                }
            }
            document.getElementById(idName).style.top=pos*100+'px';
            //add class
            var box = document.getElementById(idName)
            box.className = 'notification_box_'+box_type;                              
            
            //adding clode button
            var notify_close=document.createElement("button");            
            notify_close.id= idName+"_close";
            notify_close.innerHTML = "x";
            box.appendChild(notify_close);
            var closeButton = '#'+idName+'_close';
            document.getElementById(idName+"_close").className = 'notification_close';
            $(closeButton).click(function(){
                console.log("clicked x button of "+idName);
                widget_notification_close(idName, box_type);                                
            });

            // creating default text message notification box
            var notify_text=document.createElement("span");            
            notify_text.id= idName+"_text";
            notify_text.innerHTML = obj.title;            
            box.appendChild(notify_text);            
            document.getElementById(idName+"_text").className = 'notification_text';                

            ///////////////////////////////////            
            if (box_type==="overlay") {
                console.log("found overlay");                
                restrict_multi_overlay = true;
                $notify.addClass("widget_wrapper_overlay");                
                console.log("adding wrapper overlay");
            } else {
                open[pos]=1;
                pos+=1;            
                $notify.addClass("widget_wrapper_popup");
            }
            
            //from database grid layout

            //adding <ul>
            for(var i=0; i<obj.columns;i++)       
            {                
                var rows=document.createElement("ul");            
                rows.id= idName+"_row_"+String(i+1);    
                //adding <li>       
                for(var j=0; j<obj.rows;j++)       
                {                
                    rows.innerHTML += '<li id = "'+idName+'_list'+((i*obj.rows)+(j+1))+'" class="notification_item dialog_item"></li>';
                    box.appendChild(rows);     
                    //accessing                        
                }   
                var rows = document.getElementById(idName+"_row_"+String(i+1))
                rows.className = 'notification_item_group dialog_item_group';            
            }
            
            populate_notification_box(idName, obj.elements);
            //////////////////////////////////              
        }
            
                
    }  //if boolean
};

var populate_notification_box = function(parentid, elements)
{   

    for(var i =0; i<elements.length;i++)
    {        
        var parent = document.getElementById(parentid+"_list"+String(1+i));        
        var elem =document.createElement(elements[i].type);            
        elem.id= parentid+"_"+elements[i].id;                  
        elem.className = elements[i].class_name;
        if (elements[i].type === "input"){
                elem.type = elements[i].subtype;   //only in case of input            
        }
        parent.appendChild(elem);        

        parent = document.getElementById(parentid+"_"+elements[i].id);

        if(elements[i].type === "select")    
        {
            for(var j=0;j<elements[i].options.length;j++){            
                elem = document.createElement("option");
                elem.value = elements[i].options[j].value;
                elem.innerHTML = elements[i].options[j].display;
                parent.appendChild(elem);
            }
        }        
        else if(elements[i].type === "label")
        {            
            parent.innerHTML = elements[i].text_label;
            //if foolowed by an input widget
            console.log(elements[i].element);
            if(elements[i].element)
            {
                //Here code will replaced by recursive call of this function
                console.log("sub element found"+elements[i].element.type);
                
                var parent = document.getElementById(parentid+"_list"+String(1+i));                            
                var elem =document.createElement(elements[i].element.type);            
                elem.id= parentid+"_"+elements[i].element.id;          
                elem.className = elements[i].element.class_name;
                if (elements[i].element.type == "input"){
                    elem.type = elements[i].element.subtype;   //only in case of input            
                }
                parent.appendChild(elem);                                                    
            }
        }
        else if(elements[i].subtype==="button")            
        {                      
               elem.setAttribute("value", elements[i].value);
               elem.setAttribute("name", elements[i].name);
               elem.setAttribute("onclick", elements[i].onclick_event);
        }
        
    }

}



var widget_notification_close = function(id, box_type){
    //$('#'+id).hide();    
    if(box_type==="overlay")
    {
     restrict_multi_overlay = false;
    }   
    var val = document.getElementById(id).style.top;
    val.split("p");
    console.log(val[0]);
    //empty position
    open[val[0]]=0;

    var element = document.getElementById(id);
    element.parentNode.removeChild(element);
    
    document.getElementById("notification_wrapper").className = "notification_wrapper_popup";
    
    //$notify.hide();
};


var widget_notification_init = function(){
    $notify = $("#notification");
    
    $notify.show();
    //$notify.hide();    
};
