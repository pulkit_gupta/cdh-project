var widget_settings_speed=400;

var widget_settings_load = function (callback) {
    
    Database.query("lookup_settings", {}, function (ret)
    {
        if (ret.children.length!=1)
        {
            callback(config);    
            return;
        }
        
        $("#settings_group_display").empty();
        $("#settings_group_misc").empty();
        
        widget_settings_addToDialog(ret.children[0].settings.language);
        widget_settings_addToDialog(ret.children[0].settings.notifications);
        widget_settings_addToDialog(ret.children[0].settings.defaultpage);
        widget_settings_addToDialog(ret.children[0].settings.navdrag);
        
        $("#settings_navdrag").unbind("change").change(function () {
            widget_settings_nav(document.settings_form.settings_navdrag.checked);
        });

        Database.query("settings_receive", {}, function (ret)
        {
            if (ret.children.length!=1)
            {
                callback(config);    
                return;
            }  
              
            // config is used in main.js (in init())   
            callback(ret.children[0]);
        });
    });
}

var widget_settings_addToDialog = function (obj) {
    
    if (obj.type=="select")
    {
        widget_settings_add(obj.group, "<li class=\"settings_item dialog_item\">\
            <label id=\"settings_"+obj.name+"\">"+translator.ask("settings_"+obj.name)+"</label>\
            <select name=\"settings_"+obj.name+"\" id=\"settings_"+obj.name+"_select\"></select>\
            </li>");
        for (i=0; i<obj.value.length; i++)
        {
            $("#settings_"+obj.name+"_select").append("<option value=\""+obj.value[i]+"\" id=\"settings_"+obj.value[i]+"\">"+translator.ask("settings_"+obj.value[i])+"</option>");
        }
    }
    else if (obj.type=="bool")
    {
        widget_settings_add(obj.group, "<li class=\"settings_item dialog_item\">\
            <label id=\"settings_"+obj.name+"\">"+translator.ask("settings_"+obj.name)+"</label>\
            <input type=\"checkbox\" name=\"settings_"+obj.name+"\" id=\"settings_"+obj.name+"\"></input>\
            </li>");
    }
    else
    {
        widget_settings_add(obj.group, "<li class=\"settings_item dialog_item\">\
            <label id=\"settings_"+obj.name+"\">"+translator.ask("settings_"+obj.name)+"</label>\
            <input type=\""+obj.type+"\" name=\"settings_"+obj.name+"\" id=\"settings_"+obj.name+"\"></input>\
            </li>");
    }
}

var widget_settings_add = function (id, html){
    $("#settings_group_"+id).append(html);
}

var widget_settings_fill_dialog = function (params) {
    // !! THIS MUST BE IN THE SETTINGS-DIALOG!
    document.settings_form.settings_language.value=params.uilanguage;
    document.settings_form.settings_defaultpage.value=params.defaultpage;
    document.settings_form.settings_notifications.checked=params.notifications;
    document.settings_form.settings_navdrag.checked=lastsetting;
}

var widget_settings_save = function(){
    params={"uilanguage":document.settings_form.settings_language.value,
            "defaultpage":document.settings_form.settings_defaultpage.value,
            "notifications":document.settings_form.settings_notifications.checked,
            "nav":{
                "main":[],
                "output":[],
                "pages":[],
                "document":[],
                "edit":[],
                "masterdata_main":[],
                "masterdata_context":[],
                "clientoverview_main":[],
                "clientoverview_context":[]
            }};

    $main=$("#nav_submain");
    for (i=0; i<$main.children().length; i++){
        $main.children()[i].children[0].innerHTML=($($("#"+$main.children()[i].children[0].id)[0]).prop("tooltip"));
        params.nav.main[i]=$main.children()[i].outerHTML;
        $main.children()[i].children[0].innerHTML="";
    }
    $main=$("#nav_suboutput");
    for (i=0; i<$main.children().length; i++){
        $main.children()[i].children[0].innerHTML=($($("#"+$main.children()[i].children[0].id)[0]).prop("tooltip"));
        params.nav.output[i]=$main.children()[i].outerHTML;
        $main.children()[i].children[0].innerHTML="";
    }
    $main=$("#nav_subpages");
    for (i=0; i<$main.children().length; i++){
        $main.children()[i].children[0].innerHTML=($($("#"+$main.children()[i].children[0].id)[0]).prop("tooltip"));
        params.nav.pages[i]=$main.children()[i].outerHTML;
        $main.children()[i].children[0].innerHTML="";
    }
    $main=$("#nav_subdocument");
    for (i=0; i<$main.children().length; i++){
        $main.children()[i].children[0].innerHTML=($($("#"+$main.children()[i].children[0].id)[0]).prop("tooltip"));
        params.nav.document[i]=$main.children()[i].outerHTML;
        $main.children()[i].children[0].innerHTML="";
    }
    $main=$("#nav_subedit");
    for (i=0; i<$main.children().length; i++){
        $main.children()[i].children[0].innerHTML=($($("#"+$main.children()[i].children[0].id)[0]).prop("tooltip"));
        params.nav.edit[i]=$main.children()[i].outerHTML;
        $main.children()[i].children[0].innerHTML="";
    }
    $main=$("#nav_submasterdata_main");
    for (i=0; i<$main.children().length; i++){
        $main.children()[i].children[0].innerHTML=($($("#"+$main.children()[i].children[0].id)[0]).prop("tooltip"));
        params.nav.masterdata_main[i]=$main.children()[i].outerHTML;
        $main.children()[i].children[0].innerHTML="";
    }
    $main=$("#nav_submasterdata_context");
    for (i=0; i<$main.children().length; i++){
        $main.children()[i].children[0].innerHTML=($($("#"+$main.children()[i].children[0].id)[0]).prop("tooltip"));
        params.nav.masterdata_context[i]=$main.children()[i].outerHTML;
        $main.children()[i].children[0].innerHTML="";
    }
    $main=$("#nav_subclientoverview_main");
    for (i=0; i<$main.children().length; i++){
        $main.children()[i].children[0].innerHTML=($($("#"+$main.children()[i].children[0].id)[0]).prop("tooltip"));
        params.nav.clientoverview_main[i]=$main.children()[i].outerHTML;
        $main.children()[i].children[0].innerHTML="";
    }
    $main=$("#nav_subclientoverview_context");
    for (i=0; i<$main.children().length; i++){
        $main.children()[i].children[0].innerHTML=($($("#"+$main.children()[i].children[0].id)[0]).prop("tooltip"));
        params.nav.clientoverview_context[i]=$main.children()[i].outerHTML;
        $main.children()[i].children[0].innerHTML="";
    } 
    
    //console.log(JSON.stringify(params));
    
    translator.init(params); 
    show_notifications=params.notifications; 
    
    Database.query("settings_set", params, function () {
       /* widget_notification_display("Einstellungen gespeichert.", false, function()
            {  });
        */ 
        //new one
        widget_notification_display({id: "settingsave", title: "settings saved"});
    });
};

var widget_settings_toggle = function(){
    widget_dialog_toggle("settings");
    return false;
};

var widget_settings_setupnav =function(){
    widget_nav_bind("options",widget_settings_toggle);
};

var navfunctionality=[];
var lastsetting=false;
var widget_settings_nav = function (setting) {
    if (setting)
    {
	    $v=$(".nav_subnav");
	    for (i=0; i<$v.length; i++)
	    {
	        for (x=0; x<$v[i].children.length; x++)
	        {
	            // this is a hack, probably won't work with jQuery 1.9!
	            id=$v[i].children[x].children[0].id;
	            navfunctionality[id]=jQuery._data(document.getElementById(id), "events").click[0].handler;
	            
	            $dest=$("#"+id);
	            $dest.unbind("click").click(function() { return false; });
	        }
	    }
	    
	    lastsetting=true;
	    
	    $(".nav_subnav").sortable("enable");
    }
    else
    {
        $(".nav_subnav").sortable("disable");
        
        $v=$(".nav_subnav");
        if (lastsetting)
        {
	        for (i=0; i<$v.length; i++)
	        {
	            for (x=0; x<$v[i].children.length; x++)
	            {
	                // this is a hack, probably won't work with jQuery 1.9!
	                id=$v[i].children[x].children[0].id;
	                $dest=$("#"+id);
	                $dest.unbind("click").click(navfunctionality[id]);
	            }
	        }
	        lastsetting=false;
	    };
    }
}

var widget_settings_translate = function () {
    translator.ask("settings_btnclose", "settings_btnclose");
    translator.ask("settings_footerback", "settings_footerback");
    
    window.setTimeout(widget_settings_redraw_footer, 1000);
}

var widget_settings_redraw_footer=function () {
    var cgui = new canvasgui();
    cgui.pseudocss();
    cgui.drawfooter("settings_footerback");
}

var widget_settings_init = function(){
    widget_settings_translate();
    widget_dialog("settings");

    //$("#settings_groups input").blur(widget_settings_save).change(widget_settings_save);
    widget_settings_setupnav();
    
    $("#settings_btnclose").unbind("click").click(function () {
        widget_settings_save(); 
        widget_settings_toggle();  
        document.settings_form.settings_navdrag.checked=false;      
        widget_settings_nav(false);
    });
    
    $("#settings_navdrag").unbind("change").change(function () {
        widget_settings_nav(document.settings_form.settings_navdrag.checked);
    });
};
