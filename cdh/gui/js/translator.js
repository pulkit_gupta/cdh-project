/*
 * translator.js 
 */

/**
 * @class Translator
 * @description a class which handles the i18next module for the internationalization
 * @constructor
 */
function Translator()
{
	this.data=[];
	this.lang="";
}

/*
 * method which fills the translation object with all translations stored in the database
 */
Translator.prototype.init = function(config)
{
	var ctx = this;
    this.lang="de_DE";
	if (typeof(config)!="undefined" && typeof(config.uilanguage)!="undefined" && config.uilanguage.length>1)
	     this.lang=config.uilanguage;
	     
	Database.query("translation", {"uilanguage" : this.lang}, function(e) {
		ctx.data=e.children[0].translation;
	});
};

/*
 * returns whether the text is available or not (if not I recommend to add it to db)
 */
Translator.prototype.has = function (id)
{
    if (typeof(this.data[id])!="undefined" /*&& this.data[id].length>0*/)
    {
        return true;
    }
    return false;
}

/*
 * returns text in the selected language or tries again later on 
 */
Translator.prototype.ask = function (text_id, element_id, trylater)
{
    if (this.has(text_id))
    {
        if (typeof(element_id)=="undefined" || element_id.length<1)
            return this.data[text_id];
        else
        {
            if ($("#"+element_id).length>0)
                $("#"+element_id)[0].innerHTML=this.data[text_id];
            else if (typeof(trylater)=="undefined" || trylater==true)
                translator_do_later(element_id, text_id, 0);
            else
                return false;
        }
    }
    else
    {
        if (typeof(element_id)=="undefined" || element_id.length<1)
            element_id=text_id;
        if (typeof(trylater)=="undefined" || trylater==true)
            translator_do_later(element_id, text_id, 0);
        else
            return false;
    }
};


/*
 * Maybe the translator hasn't got any data when the translator is asked by GUI for texts
 * So this function will retry in .5 seconds maximum 3 times.
 */
var translator_do_later=function(id, text_id, cnt)
{
    if (typeof(cnt)=="undefined" || cnt<=0) { cnt=1; }
    if (cnt>3)
    {
        console.log("Translator got no result on #"+id+" and TEXTID:"+text_id);
        if ($("#"+id).length>1)
            $("#"+id)[0].innerHTML="#"+text_id;
        return;
    }
    if (translator.has(text_id) && $("#"+id).length>0)
    {
        if ($("#"+id).length>1)
            $("#"+id)[0].innerHTML=translator.data[text_id];
    }
    else
    {
        window.setTimeout("translator_do_later(\""+id+"\", \""+text_id+"\", "+ (++cnt) +")", 500);
    }
}

/*
 * returns whether the text is available or not (if not I recommend to add it to db)
 */
Translator.prototype.has = function (id)
{
    if (typeof(this.data[id])!="undefined" /*&& this.data[id].length>0*/)
    {
        return true;
    }
    return false;
}





/*
 * same as ask only for classes. Important if you want to translate more than one thing at the same time
 */
Translator.prototype.ask_class = function (text_id, class_id, trylater)
{
    if (this.has(text_id))
    {
        if (typeof(class_id)=="undefined" || class_id.length<1)
            return this.data[text_id];
        else
        {
            if ($("."+class_id).length>0)
            {
                for (i=0; i<$("."+class_id).length; i++)
                    $("."+class_id)[i].innerHTML=this.data[text_id];
            }
            else if (typeof(trylater)=="undefined" || trylater==true)
                translator_do_later_class(class_id, text_id, 0);
            else
                return false;
        }
    }
    else
    {
        if (typeof(class_id)=="undefined" || class_id.length<1)
            class_id=text_id;
        if (typeof(trylater)=="undefined" || trylater==true)
            translator_do_later_class(class_id, text_id, 0);
        else
            return false;
    }
};


/*
 * Same as translator_do_later, but for classes.
 */
var translator_do_later_class=function(id, text_id, cnt)
{
    if (typeof(cnt)=="undefined" || cnt<=0) { cnt=1; }
    if (cnt>3)
    {
        console.log("Translator got no result on #"+id+" and TEXTID:"+text_id);
        for (i=0; i<$("."+id).length; i++)
            $("."+id)[i].innerHTML="."+text_id;
        return;
    }
    if (translator.has(text_id) && $("."+id).length>0)
    {
        for (i=0; i<$("."+id).length; i++)
            $("."+id)[i].innerHTML=translator.data[text_id];
    }
    else
    {
        window.setTimeout("translator_do_later_class(\""+id+"\", \""+text_id+"\", "+ (++cnt) +")", 500);
    }
}


