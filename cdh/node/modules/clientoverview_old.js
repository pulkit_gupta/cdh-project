

///////////////////////////////////////////////////////////////////////////////
// we need an event stack for juggling requests, so they not cause lock 
// situations. 
//

var config = require('../conf.json');

var db;

var debug = false;

try
{
	db = require('jugglingdb').Schema;
}
catch(e)
{
	db = require('C:/Program Files/nodejs/node_modules/jugglingdb-mysql/node_modules/mysql');
}

/*
var mysql = new db('mysql', {
  host:config.sql.host,
  username : config.sql.user,
  password : config.sql.password,
  database : config.sql.db,
  debug: false
});        "host" : "cdh.n3o.at",
        "user" : "TUC_CDH",
        "password" : "TUC_CDH",
        "db" : "TUC_CDH"
*/
var mysql = db.createConnection({
	  host:config.sql.host,
	  user : config.sql.user,
	  password : config.sql.password,
	  database : config.sql.db,
	  debug: false
	  });

var  processRow = function(row, callback)
{
	
	console.log(row);
	console.log("----------------------row ended-----------------------------")
	
	callback();
}

var cdh_customers = new Object();

//store keyTables
var cdh_delivery_condition = new Object();
var cdh_payment_condition = new Object();
var cdh_shipping_typ = new Object();
var cdh_customer_title = new Object();
var cdh_customer_category = new Object();


/**
 * getPersonObject
 * 	- returns the PersonObject
 */
var getPersonObject = function(data)
{
	if(data.personID == null)
	{
		return null;
	}
	else
	{
		var persons_data = 
		{	
			personID:data.personID,
			salutation:{name:data.salutation, key:data.salutationkey},
			title:{key:cdh_customer_title[data.titleID].titleKey, name:cdh_customer_title[data.titleID].titleName},
			surname:data.surname,
			lastname:data.lastname,
			position:data.position,
			telephone:data.telephone,
			fax:data.fax,
			mobilphone:data.mobilphone,
			email:data.email,
			birthday:data.birthday,
			bithdaymailing:data.bithdaymailing,
			notes:data.notes
		}
		return persons_data;
	}

};

var getPersonObjectArango = function(data)
{
	if(data.personID == null)
	{
		return null;
	}
	else
	{
		var mailing;
		if(data.bithdaymailing == 0)
			{
			mailing = false;
			}
		else
			{
			mailing = true;
			}
		
		
		var persons_data = 
		{	
			contactid:data.personID,
			salutation:{name:data.salutation, key:data.salutationkey},
			title:{key:cdh_customer_title[data.titleID].titleKey, name:cdh_customer_title[data.titleID].titleName},
			surname:data.surname,
			lastname:data.lastname,
			position:data.position,
			telephone:data.telephone,
			fax:data.fax,
			mobilphone:data.mobilphone,
			email:data.email,
			birthday:data.birthday,
			bithdaymailing:mailing,
			notes:data.notes
		}
		return persons_data;
	}

};

/**
 * getAdressObject
 * 	- returns the AdressObject
 */
var getAdressObject = function(data)
{
	var ismain = true;
	if(data.ismainadress == 0)
		{
		ismain = false
		}
	var adresses_data = {
			
				adressid:data.customerAdressID,
				companyname:data.companyname,
				nameaffix:data.nameaffix,
				adress:data.adress,
				zipcode:data.zipcode,
				city:data.city,
				country:data.country,
				deliverynote:data.deliverynote,
	};
	return adresses_data;
};


/**
 * getMainObject
 * 	- returns the MainObject
 */
var getMainObject = function(data_customer, data_customer_main)
{
	var main_data = 
	{
			customerID:data_customer.customerID,
			clientnumber:data_customer.clientnumber,
			mainadressID:data_customer_main.mainAdressID,
			telephone:data_customer_main.telephone,
			fax:data_customer_main.fax,
			mobilephone:data_customer_main.mobilephone,
			email:data_customer_main.email,
			website:data_customer_main.website,
			comment:data_customer_main.comment,
			category:cdh_customer_category[data_customer_main.categoryID],
			categoryID:data_customer_main.categoryID,
			clienttyp:data_customer_main.clienttyp,
			maincontactID:data_customer_main.maincontactID,
			logopath:data_customer_main.logopath
	};
	return main_data;
};
var getMainObjectArango = function(data_customer, data_customer_main)
{
	var main_data = 
	{
			customerID:data_customer.customerID,
			clientnumber:data_customer.clientnumber,
			mainadressID:data_customer_main.mainAdressID,
			telephone:data_customer_main.telephone,
			fax:data_customer_main.fax,
			mobilephone:data_customer_main.mobilephone,
			email:data_customer_main.email,
			website:data_customer_main.website,
			comment:data_customer_main.comment,
			category:cdh_customer_category[data_customer_main.categoryID],
			categoryID:data_customer_main.categoryID,
			clienttyp:data_customer_main.clienttyp,
			maincontactID:data_customer_main.maincontactID,
			logopath:data_customer_main.logopath
	};
	return main_data;
};

/**
 * getFinanceObject
 * 	- returns the FinanceObject
 */
var getFinanceObject = function(data)
{
	var finance_data =
	{	
		financeID:data.financeID,
		paymentcondition:cdh_payment_condition[data.paymentconditionID],
		paymentconditionID:data.paymentconditionID,
		deliverycondition:cdh_delivery_condition[data.deliveryconditionID],
		deliveryconditionID:data.deliveryconditionID,
		shippingtyp:cdh_shipping_typ[data.shippingtypID],
		shippingtypID:data.shippingtypID,
		shippingnote:data.shippingnote,
		debitorennumber:data.debitorennumber,
		taxnumber:data.taxnumber,
		accountholder:data.accountholder,
		accountnumber:data.accountnumber,
		blz:data.blz,
		bank:data.bank,
		delegate:data.delegate,
		catalogues:data.catalogues,
		discount:data.discount,
		discountnote:data.discountnote
	};
	return finance_data;
};
var getFinanceObjectArango = function(data)
{
		
	var finance_data =
	{	
		paymentcondition:{name:cdh_payment_condition[data.paymentconditionID]},
		deliverycondition:{name:cdh_delivery_condition[data.deliveryconditionID]},
		shippingtypID:{name:cdh_shipping_typ[data.shippingtypID]},
		shippingnote:data.shippingnote,
		debitorennumber:data.debitorennumber,
		taxnumber:data.taxnumber,
		accountholder:data.accountholder,
		accountnumber:data.accountnumber,
		blz:data.blz,
		bank:data.bank,
		delegate:data.delegate,
		catalogues:data.catalogues,
		discount:data.discount,
		discountnote:data.discountnote
	};
	return finance_data;
};

/**
 * getAdditionalObject
 * 	- returns the AdditionalObject
 */
var getAdditionalObject = function(data)
{
	var additional_data = 
	{	
		crmversion:data.crmversion,
		syndicatemember:data.syndicatemember,
		exhibitions:data.exhibitions
	};
	return additional_data;
};



var buildCustomerObject = function(row)
{
	console.log(row);
	
	var cdh_payment_condition = new Object();
	
	
}

var starttime;
var endtime;
var getCustomer = function()
{
	starttime = new Date();
	var indexMap = new Object();
	var customerArray = new Array();
	var sqlQuery = "SELECT * FROM cdh_customer LEFT JOIN cdh_customer_main ON cdh_customer.customerID = cdh_customer_main.customerID "+  
	"LEFT JOIN cdh_customer_category AS category ON cdh_customer_main.categoryID  = category.categoryID "+ 
	"LEFT JOIN cdh_customer_adress ON cdh_customer.customerID = cdh_customer_adress.customerID "+ 
	"LEFT JOIN cdh_customer_person ON cdh_customer.customerID = cdh_customer_person.customerID "+ 
	"LEFT JOIN cdh_customer_additional ON cdh_customer.customerID = cdh_customer_additional.customerID "+ 
	"LEFT JOIN cdh_customer_finance ON cdh_customer.customerID = cdh_customer_finance.customerID "+ 
	//"";
	"WHERE cdh_customer.customerID = 1";
	mysql.query({sql:sqlQuery, nestTables:true}, function(err, rows) {
		
		console.log("err:"+err);	
			
		for ( var i = 0; i < rows.length; i++) 
		{
			
			if(cdh_customers[rows[i].cdh_customer.customerID] != undefined)
			{
				var found = false;
				for ( var j = 0; j < cdh_customers[rows[i].cdh_customer.customerID].contactpersons.length; j++) 
				{
					if(cdh_customers[rows[i].cdh_customer.customerID].contactpersons[j].personID == rows[i].cdh_customer_person.personID)
					{
						found = true;
					}
				}
				if(!found)
				{
					cdh_customers[rows[i].cdh_customer.customerID].contactpersons.push(getPersonObject(rows[i].cdh_customer_person));
				}
				
				
				found = false;
				for ( var j = 0; j < cdh_customers[rows[i].cdh_customer.customerID].adresses.length; j++) 
				{
					if(cdh_customers[rows[i].cdh_customer.customerID].adresses[j].customeradressID == rows[i].cdh_customer_adress.customeradressID)
					{
						found = true;
					}
				}
				if(!found)
				{
					cdh_customers[rows[i].cdh_customer.customerID].adresses.push(getAdressObject(rows[i].cdh_customer_adress));
				}
			}
			else
			{
				
				cdh_customers[rows[i].cdh_customer.customerID] = 
				{
					main: getMainObject(rows[i].cdh_customer, rows[i].cdh_customer_main),
					adresses: [getAdressObject(rows[i].cdh_customer_adress)],
					finance: getFinanceObject(rows[i].cdh_customer_finance),
					contactpersons: [getPersonObject(rows[i].cdh_customer_person)],
					additional: getAdditionalObject(rows[i].cdh_customer_additional)
				};
				/*
				getMainObject(rows[i].cdh_customer, rows[i].cdh_customer_main);
				getAdressObject(rows[i].cdh_customer_adress);
				getFinanceObject(rows[i].cdh_customer_finance);
				getAdditionalObject(rows[i].cdh_customer_additional);
				getPersonObject(rows[i].cdh_customer_person)
				*/

			}
			
		}
		endtime = new Date();
		console.log("starttime:"+starttime);
		console.log("endtime:"+endtime);
		console.log("difference:"+(endtime-starttime));
		console.log(JSON.stringify(cdh_customers));
		//console.log(fields);
	});
	


}
var getCustomerForArango = function()
{
	starttime = new Date();
	var indexMap = new Object();
	var customerArray = new Array();
	var sqlQuery = "SELECT * FROM cdh_customer LEFT JOIN cdh_customer_main ON cdh_customer.customerID = cdh_customer_main.customerID "+  
	"LEFT JOIN cdh_customer_category AS category ON cdh_customer_main.categoryID  = category.categoryID "+ 
	"LEFT JOIN cdh_customer_adress ON cdh_customer.customerID = cdh_customer_adress.customerID "+ 
	"LEFT JOIN cdh_customer_person ON cdh_customer.customerID = cdh_customer_person.customerID "+ 
	"LEFT JOIN cdh_customer_additional ON cdh_customer.customerID = cdh_customer_additional.customerID "+ 
	"LEFT JOIN cdh_customer_finance ON cdh_customer.customerID = cdh_customer_finance.customerID "+ 
	"";
	//"WHERE cdh_customer.customerID = 67";
	mysql.query({sql:sqlQuery, nestTables:true}, function(err, rows) {
		
		for ( var i = 0; i < rows.length; i++) 
		{
				var personObj = getPersonObjectArango(rows[i].cdh_customer_person);
				
				if(personObj == null)
				{
					cdh_customers[rows[i].cdh_customer.customerID] = 
					{
							clientnumber:rows[i].cdh_customer.clientnumber,
							mainadress:getAdressObject(rows[i].cdh_customer_adress),
							telephone:rows[i].cdh_customer_main.telephone,
							fax:rows[i].cdh_customer_main.fax,
							mobilephone:rows[i].cdh_customer_main.mobilephone,
							email:rows[i].cdh_customer_main.email,
							website:rows[i].cdh_customer_main.website,
							comment:rows[i].cdh_customer_main.comment,
							category:{name:cdh_customer_category[rows[i].cdh_customer_main.categoryID]},
							clienttyp:rows[i].cdh_customer_main.clienttyp,
							maincontact:null,
							logopath:rows[i].cdh_customer_main.logopath,
							adresses: null,
							finance: getFinanceObjectArango(rows[i].cdh_customer_finance),
							contactpersons: null,
							additional: getAdditionalObject(rows[i].cdh_customer_additional)
					};
				}
				else
				{
					cdh_customers[rows[i].cdh_customer.customerID] = 
					{
							clientnumber:rows[i].cdh_customer.clientnumber,
							mainadress:getAdressObject(rows[i].cdh_customer_adress),
							telephone:rows[i].cdh_customer_main.telephone,
							fax:rows[i].cdh_customer_main.fax,
							mobilephone:rows[i].cdh_customer_main.mobilephone,
							email:rows[i].cdh_customer_main.email,
							website:rows[i].cdh_customer_main.website,
							comment:rows[i].cdh_customer_main.comment,
							category:{name:cdh_customer_category[rows[i].cdh_customer_main.categoryID]},
							clienttyp:rows[i].cdh_customer_main.clienttyp,
							maincontact:personObj,
							logopath:rows[i].cdh_customer_main.logopath,
							adresses: null,
							finance: getFinanceObjectArango(rows[i].cdh_customer_finance),
							contactpersons: null,
							additional: getAdditionalObject(rows[i].cdh_customer_additional)
					};
				}

				/*
				getMainObject(rows[i].cdh_customer, rows[i].cdh_customer_main);
				getAdressObject(rows[i].cdh_customer_adress);
				getFinanceObject(rows[i].cdh_customer_finance);
				getAdditionalObject(rows[i].cdh_customer_additional);
				getPersonObject(rows[i].cdh_customer_person)
				*/

			}
			
			
		endtime = new Date();
		//console.log("starttime mysql:"+starttime);
		//console.log("endtime mysql:"+endtime);
		//console.log("difference mysql:"+(endtime-starttime));
		console.log(JSON.stringify(cdh_customers));
		//console.log(cdh_customers);
	});
	


}






var getKeytables = function()
{
	
	var sqltQuery = "SELECT * FROM cdh_delivery_condition, cdh_payment_condition, cdh_shipping_typ, cdh_customer_title, cdh_customer_category";
	mysql.query({sql:sqltQuery, nestTables:false}, function(err, rows) {
		for ( var i = 0; i < rows.length; i++) 
		{
			if(cdh_delivery_condition[rows[i].deliveryconditionID] == undefined)
			{
				cdh_delivery_condition[rows[i].deliveryconditionID] = rows[i].deliverycondition;
			}
			if(cdh_payment_condition[rows[i].paymentconditionID] == undefined)
			{
				cdh_payment_condition[rows[i].paymentconditionID] = rows[i].paymentcondition;
			}
			if(cdh_shipping_typ[rows[i].shippingtypID] == undefined)
			{
				cdh_shipping_typ[rows[i].shippingtypID] = rows[i].shippingtyp;
			}
			if(cdh_customer_title[rows[i].titleID] == undefined)
			{
				cdh_customer_title[rows[i].titleID] = {titleKey:rows[i].titleKey, titleName:rows[i].titleName};
			}
			if(cdh_customer_category[rows[i].categoryID] == undefined)
			{
				cdh_customer_category[rows[i].categoryID] = rows[i].categoryName;
			}
		}
		if (debug == true)
		{
			console.log("-------------------------------cdh_delivery_condition--------------------------");
			console.log(cdh_delivery_condition);
			console.log("-------------------------------------------------------------------------------");
			console.log("-------------------------------cdh_payment_condition--------------------------");
			console.log(cdh_payment_condition);
			console.log("------------------------------------------------------------------------------");
			console.log("-------------------------------cdh_shipping_typ--------------------------");
			console.log(cdh_shipping_typ);
			console.log("-------------------------------------------------------------------------");
			console.log("-------------------------------cdh_customer_title--------------------------");
			console.log(cdh_customer_title);
			console.log("---------------------------------------------------------------------------");
			console.log("-------------------------------cdh_customer_category--------------------------");
			console.log(cdh_customer_category);
			console.log("---------------------------------------------------------------------------");
		}
		//getCustomer();
		getCustomerForArango();
	});
	
};

getKeytables();

var test = function()
{
	var query = "SELECT * FROM cdh_customer LEFT JOIN cdh_customer_main ON cdh_customer.customerID = cdh_customer_main.customerID "+  
	"LEFT JOIN cdh_customer_category AS category ON cdh_customer_main.categoryID  = category.categoryID "+ 
	"LEFT JOIN cdh_customer_adress ON cdh_customer.customerID = cdh_customer_adress.customerID "+ 
	"LEFT JOIN cdh_customer_person ON cdh_customer.customerID = cdh_customer_person.customerID "+ 
	"LEFT JOIN cdh_customer_title ON cdh_customer_person.titleID = cdh_customer_title.titleID "+ 
	"LEFT JOIN cdh_customer_additional ON cdh_customer.customerID = cdh_customer_additional.customerID "+ 
	"LEFT JOIN cdh_customer_finance ON cdh_customer.customerID = cdh_customer_finance.customerID "+ 
	"LEFT JOIN cdh_payment_condition ON cdh_customer_finance.paymentconditionID = cdh_payment_condition.paymentconditionID "+ 
	"LEFT JOIN cdh_delivery_condition ON cdh_customer_finance.deliveryconditionID = cdh_delivery_condition.deliveryconditionID "+ 
	"LEFT JOIN cdh_shipping_typ ON cdh_customer_finance.shippingtypID = cdh_shipping_typ.shippingtypID "+
	"WHERE cdh_customer.customerID = 67";
	//"LIMIT 0, 10";
	/*
	mysql.query({sql:query, nestTables:true}, function(err, rows, fields) {
		
		console.log(rows);
		console.log(err);	
		//console.log(fields);
	});*/
	var curQuery = mysql.query({sql:query, nestTables:true});
	curQuery
	  .on('error', function(err) {
	    // Handle error, an 'end' event will be emitted after this as well
	  })
	  .on('fields', function(fields) {
	    // the field packets for the rows to follow
	  })
	  .on('result', function(row) {
	    // Pausing the connnection is useful if your processing involves I/O
		  mysql.pause();


		    processRow(row, function() {
		    	mysql.resume();
		      });

	  })
	  .on('end', function() {
	    // all rows have been received
		  console.log("----------------------end-----------------------------")
	  });
};


exports.simpleQuery = function(what, transactionId, params, callback)
{
	mysql.query('SELECT * FROM cdh_customer LEFT JOIN cdh_customer_adress ON cdh_customer.customerID = cdh_customer_adress.customerID WHERE cdh_customer.customerID = 1', function(err, rows) {
	
		console.log(rows);
		console.log(err);
	});
	
};



