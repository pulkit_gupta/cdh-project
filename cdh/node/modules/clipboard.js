/**
  * For getting elements for the clipboard
*/

DaoRegistry["clipboard"] = function(params, callback)
{
    if (sw==true)
    {
	    console.log("params:"+JSON.stringify(params));
	    console.log("callback:"+callback);
	}
	
	if(params.session && (m = /SID=([^ ,;]*)/.exec(params.session)))
    {
	    // at first check if the user is logged in and get group and user.
	   /* FOR u in User
        LET rights = (
            FOR r in Roles
            FILTER r._key==u.role
            RETURN r)
        FILTER u.name=="root"
        RETURN {"u": u, "r":rights}
        
        
        FILTER s.sessID==\""+m[1]+"\"*/
	    
	    query="FOR u in User \
            LET rights = ( FOR r in Roles FILTER r._key==u.role RETURN r)\
            LET sess = ( FOR s in Sessions FILTER s.userkey==u._key FILTER s.sessID==\""+m[1]+"\" RETURN s) \
            FILTER sess!=[]\
            RETURN {'group':u.group, 'key':u._key, 'rights': rights, 'session':sess}";
	    arangodb.query.exec(query, function (err, ret)
	    {
            if (ret.result.length!=1 && ret.result[0].rights.length!=1)
                return;
                
            // user hasn't got any reading-permissions
            if (ret.result[0].rights[0].clipboard & 2)
            {
	            // group is set; owner_id is unimportant and will be ignored
	            if (typeof(params.group)!="undefined")
	                query="FOR u in Clipboard FILTER u.group_id=="+ ret.result[0].group +" FILTER u.owner_id==0 FILTER u.done==0 SORT u.prio ASC  RETURN u";
	            // owner is set
	            else if (typeof(params.owner)!="undefined")
	                query="FOR u in Clipboard FILTER u.owner_id==" + ret.result[0].key +" FILTER u.done==0 SORT u.prio ASC RETURN u";
	            // nothin is set; will get all tasks
	            else 
	                 query="FOR u in Clipboard RETURN u";
	            
	            arangodb.query.exec(query,function(err,ret)
	            {
		            for (i=0; i<ret.result.length; i++) {
			            ret.result[i].className = "ClipboardContent";
		            }
		            callback({ "className": "ContentTree", "children": ret.result });
	            });
	        }
        });
    }
};

/// creates a task
DaoRegistry["clipboard_create"] = function(params, callback)
{
    if (sw==true)
    {
	    console.log("params:"+JSON.stringify(params));
	    console.log("callback:"+callback);
	}
	
    if(params.session && (m = /SID=([^ ,;]*)/.exec(params.session)))
    {
	    // at first check if the user is logged in and get group and userid.
        query="FOR u in User \
            LET rights = ( FOR r in Roles FILTER r._key==u.role RETURN r)\
            LET sess = ( FOR s in Sessions FILTER s.userkey==u._key FILTER s.sessID==\""+m[1]+"\" RETURN s) \
            FILTER sess!=[]\
            RETURN {'group':u.group, 'key':u._key, 'rights': rights, 'session':sess}";
	    arangodb.query.exec(query, function (err, ret)
	    {
            if (ret.result.length!=1 && ret.result[0].rights.length!=1)
                return;
                
            // user hasn't got any reading-permissions
            if (ret.result[0].rights[0].clipboard & 4)
            {
                params.author=parseInt(ret.result[0].key);
                delete params.session;
                
                params.group_id=ret.result[0].group;
	            if (params.owner_id==1)
	            {
	                params.owner_id=parseInt(ret.result[0].key);
	            }
	            else
	            {
	                params.owner_id=0;
	            }
	            
                arangodb.use("Clipboard").document.create(params, function () {
	                var query="FOR u in Clipboard FILTER u.author=="+params.author+" FILTER u.date=="+params.date+" FILTER u.deadline=="+params.deadline+" FILTER u.title=='"+params.title+"' RETURN u";
	                console.log(query);
	                arangodb.query.exec(query,function(err,ret){
		                for (i=0; i<ret.result.length; i++) {
			                ret.result[i].className = "ClipboardContent";
		                }
		                callback({ "className": "ContentTree", "children": ret.result });
	                });
	            });
	        }
	    });
	}
};

DaoRegistry["clipboard_save"] = function(params, callback)
{
    if (sw==true)
    {
	    console.log("params:"+JSON.stringify(params));
	    console.log("callback:"+callback);
	}
	
	//var query="FOR u in annotation FILTER u.noteid=="+params.noteid+" RETURN u";
	
	if(params.session && (m = /SID=([^ ,;]*)/.exec(params.session)))
    {
	    // at first check if the user is logged in and get group and userid.
        query="FOR u in User \
            LET rights = ( FOR r in Roles FILTER r._key==u.role RETURN r)\
            LET sess = ( FOR s in Sessions FILTER s.userkey==u._key FILTER s.sessID==\""+m[1]+"\" RETURN s) \
            FILTER sess!=[]\
            RETURN {'group':u.group, 'key':u._key, 'rights': rights, 'session':sess}";
	    arangodb.query.exec(query, function (err, ret)
	    {
            if (ret.result.length!=1 && ret.result[0].rights.length!=1)
                return;
                
            // user hasn't got any reading-permissions
            if (ret.result[0].rights[0].clipboard & 4)
            {
	            var id=params.id;
	            delete params.id;
	            delete params.session;
	            params.group_id=ret.result[0].group;
	            if (params.owner_id==1)
	            {
	                params.owner_id=parseInt(ret.result[0].key);
	            }
	            else
	            {
	                params.owner_id=0;
	            }
	            arangodb.use("Clipboard").document.put(id, params, function () {
	            });
	        }
	    });
	}    
};

DaoRegistry["clipboard_done"] = function(params, callback)
{
    if (sw==true)
    {
	    console.log("params:"+JSON.stringify(params));
	    console.log("callback:"+callback);
	}
	
	//var query="FOR u in annotation FILTER u.noteid=="+params.noteid+" RETURN u";
	
	if(params.session && (m = /SID=([^ ,;]*)/.exec(params.session)))
    {
	    // at first check if the user is logged in and get group and userid.
        query="FOR u in User \
            LET rights = ( FOR r in Roles FILTER r._key==u.role RETURN r)\
            LET sess = ( FOR s in Sessions FILTER s.userkey==u._key FILTER s.sessID==\""+m[1]+"\" RETURN s) \
            FILTER sess!=[]\
            RETURN {'group':u.group, 'key':u._key, 'rights': rights, 'session':sess}";
	    arangodb.query.exec(query, function (err, ret)
	    {
            if (ret.result.length!=1 && ret.result[0].rights.length!=1)
                return;
                
            // user hasn't got any reading-permissions
            if (ret.result[0].rights[0].clipboard & 4)
            {
	            var id=params.id;
	            delete params.id;
	            delete params.session;
	            params.group_id=ret.result[0].group;
	            if (params.owner_id==1)
	            {
	                params.owner_id=parseInt(ret.result[0].key);
	            }
	            else
	            {
	                params.owner_id=0;
	            }
	            arangodb.use("Clipboard").document.put(id, params, function () {
	                callback({ "className": "ContentTree", "children": {} });            
	            });
	        }
	    });
	}    
};

