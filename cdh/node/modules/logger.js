/**
 * Logger
 * - wrapper for console functions, arguments list is passed through
 * - automatically adds timestamp (hours, minutes, seconds and ms)
 * - only logs when log level matches (does not log if attribute Logger.level is set to Logger.SILENT)
 */
var Logger = function() {
    this.DEBUG = 0;
    this.INFO = 1;
    this.WARN = 2;
    this.ERROR = 3;
    this.SILENT = 4;

    this.level = this.DEBUG;
    
    if (typeof config === "undefined"){
        console.error(
            "config needs to be loaded first, before logger could be loaded."
        );
        process.exit();
    } else{
        // set logger level based on environment setting (production = silent, debug = all)
        switch (config.environment) {
            case "production": 
                Logger.level = Logger.SILENT; 
                break;
            case "debug": 
                Logger.level = Logger.DEBUG; 
                console.log("debug mode");
                break;
        }
    }
};

Logger.prototype.getTimestamp = function() {
    var date = new Date();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    var ms = date.getMilliseconds();
    return ((h < 10) ? ("0" + h) : h) + ":" + ((m < 10) ? ("0" + m) : m) + ":" + ((s < 10) ? ("0" + s) : s) + "." + ((ms < 100) ? ((ms < 10) ? ("00" + ms) : ("0" + ms)) : ms);
};

Logger.prototype.debug = function() {
    if (this.level <= this.DEBUG) {
        tmp=[this.getTimestamp() + " [DEBUG] "].concat(Array.prototype.slice.call(arguments));
        console.log.apply(this, tmp);
    }
};

Logger.prototype.error = function() {
    if (this.level <= this.ERROR) {
        tmp=[this.getTimestamp() + " [ERROR] "].concat(Array.prototype.slice.call(arguments));
        console.error.apply(this, tmp);
    }
};

Logger.prototype.info = function() {
    if (this.level <= this.INFO) {
        tmp=[this.getTimestamp() + " [INFO] "].concat(Array.prototype.slice.call(arguments));
        console.info.apply(this, tmp);
    }
};

Logger.prototype.warn = function() {
    if (this.level <= this.WARN) {
        tmp=[this.getTimestamp() + " [WARNING] "].concat(Array.prototype.slice.call(arguments));
        console.warn.apply(this, tmp);
    }
};

module.exports = new Logger();

// switches server to a more silent mode. 
// if you want the server to talk more, set it to true. 
// if not, leave it as is. 
sw=false;
