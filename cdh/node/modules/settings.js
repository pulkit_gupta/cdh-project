
DaoRegistry["settings_receive"] = function(params, callback)
{
    if (sw==true)
    {
	    console.log("params:"+JSON.stringify(params));
	    console.log("callback:"+callback);
	}
	
	if(params.session && (m = /SID=([^ ,;]*)/.exec(params.session)))
    {	    
	    query="FOR u in User \
            LET sess = ( FOR s in Sessions FILTER s.userkey==u._key FILTER s.sessID==\""+m[1]+"\" RETURN s) \
            FILTER sess!=[]\
            RETURN {'key':u._key}";
	    arangodb.query.exec(query, function (err, ret)
	    {
            if (ret.result.length!=1 || ret.result[0].key=="")
                return;
                                  
            query="FOR s in Settings FILTER s.userkey==\""+ret.result[0].key+"\" \
                RETURN {'uilanguage':s.uilanguage, 'defaultpage':s.defaultpage, 'notifications':s.notifications}";
            arangodb.query.exec(query,function(err,ret)
            {
	            for (i=0; i<ret.result.length; i++) {
		            ret.result[i].className = "SettingsContent";
	            }
	            callback({ "className": "ContentTree", "children": ret.result });
            });
        });
    }
}

DaoRegistry["settings_receivenav"] = function(params, callback)
{
    if (sw==true)
    {
	    console.log("params:"+JSON.stringify(params));
	    console.log("callback:"+callback);
	}
	
	if(params.session && (m = /SID=([^ ,;]*)/.exec(params.session)))
    {	    
	    query="FOR u in User \
            LET sess = ( FOR s in Sessions FILTER s.userkey==u._key FILTER s.sessID==\""+m[1]+"\" RETURN s) \
            FILTER sess!=[]\
            RETURN {'key':u._key}";
	    arangodb.query.exec(query, function (err, ret)
	    {
            if (ret.result.length!=1 || ret.result[0].key=="")
                return;
                                  
            query="FOR s in Settings FILTER s.userkey==\""+ret.result[0].key+"\" \
                RETURN {'nav':s.nav}";
            arangodb.query.exec(query,function(err,ret)
            {
	            for (i=0; i<ret.result.length; i++) {
		            ret.result[i].className = "SettingsContent";
	            }
	            callback({ "className": "ContentTree", "children": ret.result });
            });
        });
    }
}

DaoRegistry["settings_set"] = function(params, callback)
{
    if (sw==true)
    {
	    console.log("params:"+JSON.stringify(params));
	    console.log("callback:"+callback);
	}
	
	if(params.session && (m = /SID=([^ ,;]*)/.exec(params.session)))
    {	    
	    query="FOR u in User \
            LET sess = ( FOR s in Sessions FILTER s.userkey==u._key FILTER s.sessID==\""+m[1]+"\" RETURN s) \
            FILTER sess!=[]\
            RETURN {'key':u._key}";
	    arangodb.query.exec(query, function (err, ret)
	    {
            if (ret.result.length!=1 || ret.result[0].key=="")
                return;
             
            params.userkey=ret.result[0].key;                      
            query="FOR s in Settings FILTER s.userkey==\""+ret.result[0].key+"\" \
                RETURN {'id':s._id}";
            arangodb.query.exec(query,function(err,ret)
            {
                delete params.session; 
                if (ret.result.length>0 && ret.result[0].id!="")
                { 
                    arangodb.use("Settings").document.put(ret.result[0].id, params, function ()
                    {
	                    callback({ "className": "ContentTree", "children": {} });
	                });
	            }
	            else
	            {
	                arangodb.use("Settings").document.create(params, function ()
                    {
	                    callback({ "className": "ContentTree", "children": {} });
	                });
	            }
            });
        });
    }
}


