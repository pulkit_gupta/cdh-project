DaoRegistry["user_namebyid"] = function(params, callback)
{
    if (sw)
    {
	    console.log("params:"+JSON.stringify(params));
	    console.log("callback:"+callback);
	}
	
	if(params.session && (m = /SID=([^ ,;]*)/.exec(params.session)))
    {
	    // at first check if the user is logged in and get group and user.
	    query="FOR u in User \
	    LET sess = ( FOR s in Sessions FILTER s.userkey==u._key FILTER s.sessID==\""+m[1]+"\" RETURN s) \
        FILTER sess!=[] \
        RETURN {'key':u._key}";
	    arangodb.query.exec(query, function (err, ret)
	    {
            if (ret.result.length!=1)
                return;
                
	        query="FOR u in User FILTER u._key=='"+params.id+"' RETURN {'name':u.name}";
	        arangodb.query.exec(query,function(err,ret)
	        {
		        //if (ret.result.length!=1)
		        //    callback({ "className": "ContentTree", "children": [] });
		        callback({ "className": "ContentTree", "children": ret.result });
	        });
	     });
	 };
};

