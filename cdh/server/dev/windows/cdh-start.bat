@echo off

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Path to working copy of CDH repository
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

set CDH_PATH=%CDH_PATH%

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Update links to repository
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if exist %~dp0bin\nginx\html\cdh rmdir %~dp0bin\nginx\html\cdh
mklink /J %~dp0bin\nginx\html\cdh %CDH_PATH%\gui
if exist %~dp0bin\nginx\html\data rmdir %~dp0bin\nginx\html\data
mklink /J %~dp0bin\nginx\html\data %CDH_PATH%\node\data

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Start server
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

call %~dp0nginx
call %~dp0node %CDH_PATH%\node\server.js -d
call %~dp0nginx -s stop
