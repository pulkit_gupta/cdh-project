/*
 * Canvas stuff for gui elements
 */


function canvasgui(){
	var __inst__;
	
	canvasgui = function canvasgui(){	
		this.TextColor;
		this.Font;
		this.Gradient=[];
		this.BGColor;
		this.init=false;
		
		return __inst__;
	};
	
	canvasgui.prototype = this;
	__inst__ = new canvasgui();
	__inst__.constructor=canvasgui();
	
	return __inst__;
};

// warning: ugly
canvasgui.prototype.pseudocss = function(){
    var $pseudo=$("<div id='canvasgui_gradient'></div>").addClass("__pseudo__").hide();
    var bg;         // matches for example (123, 123, 123) 100%
    var patternfull=/\([0-9]+\s*,\s*[0-9]+\s*,\s*[0-9]+\s*\)\s*[0-9]+%/gi;
    var matchesfull;
    
    // append to body so css has an effect 
    $('body').append($pseudo);

    // read the properties needed
    bg=$pseudo.css("background-image");
    this.TextColor = $pseudo.css("color");
    this.Font=
            $pseudo.css("font-style") + " " + 
            $pseudo.css("font-size") + " " + 
            $pseudo.css("font-family");
    this.BGColor=$pseudo.css("background-color");
    
    // immediately remove it just because it was just a workaround
    $pseudo.remove();
    
    if (bg.indexOf("linear-gradient") === -1) return false;
    
    matchesfull = bg.match(patternfull);
    for (i=0; i<matchesfull.length; i++){
        // we could be sure that the syntax is ok, so we just match the 4 nums
        var values = matchesfull[i].match(/[0-9]+/g);
        this.Gradient.push([
            values[3]/100, 
            "rgb(" + values[0]  + "," + values[1]  + "," + values[2]  + ")"
        ]);
    }
    
};

canvasgui.prototype.drawcurve = function(ctx,width,height){
    var grd = ctx.createLinearGradient(0,0,0,height);
    var h = height-1;
    var w = width;
    
    // gradient
    for (i=0; i<this.Gradient.length; i++){
        var vals=this.Gradient[i];
        grd.addColorStop(vals[0], vals[1]);
    }
    
    if (!this.Gradient.length) {
        ctx.fillStyle=this.BGColor;
        ctx.strokeStyle=this.BGColor;
    }else{
        ctx.fillStyle=grd;
        ctx.strokeStyle=this.Gradient[0][1];
    }
    ctx.beginPath();
        ctx.moveTo(0,0);
		
        ctx.lineTo(h/5,0);
        ctx.quadraticCurveTo(h/5 + h/2, 0 ,h/5 + h/2,h/2);
        ctx.quadraticCurveTo(h/5 + h/2, h ,h+h/5 ,h );
        
        ctx.lineTo(w - h - h/5,h);
        
        ctx.quadraticCurveTo(w - h/5 - h/2, h, w - h/5 - h/2,h/2);
        ctx.quadraticCurveTo(w - h/5 - h/2, 0, w - h/5, 0);
        ctx.lineTo(w,0);
        
        ctx.lineTo(0,0);
    ctx.fill();
    ctx.stroke();
};

canvasgui.prototype.drawheader = function(id){
    var $cv = $("#"+id);
    var ctx = $cv[0].getContext('2d'); 
    
    // use the css settings to prevent streching
    ctx.canvas.width=$cv.width();
    ctx.canvas.height=$cv.height();
    this.drawcurve(ctx, $cv.width(), $cv.height());
};

canvasgui.prototype.drawfooter = function(id){
    var $cv = $("#"+id);
    var ctx = $cv[0].getContext('2d'); 
    var text =$cv.text();
    ctx.canvas.width=$cv.width();
    ctx.canvas.height=$cv.height();
    
    ctx.save();
    // flip vertical
    ctx.translate(0, $cv.height());
    ctx.scale(1, -1);
    this.drawcurve(ctx, $cv.width(), $cv.height());
    ctx.restore();
    
    ctx.font = this.Font;
    ctx.textAlign = 'center';
    ctx.fillStyle = this.TextColor;
    ctx.fillText(text, $cv.width()/2, 3*$cv.height()/4);
};

