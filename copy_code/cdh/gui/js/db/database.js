/*
 * database.js 
 *
 * contains:
 * Database.*
 * Cache.*
 * Mapper.*
 * Query.*
 * Queue.*
 *
 */

/**
 * @class Database
 * @description: Database connectivity. Must be instantiated and overwritten as static object before use.
 * @constructor
 */
function Database()
{
	this.status = false;
	this.cache = new Cache();
	this.transactions = new HashMap();
}

/**
 * getStatus
 * 
 * @description Indicates health of database connection. True = Active, False = Inactive.
 * @returns {Boolean}
 */
Database.prototype.getStatus = function()
{
	return this.status;
};

/**
 * setOnline
 * 
 * @description Set database connection status to active and enable cache cleaning (on-line mode).
 */
Database.prototype.setOnline = function()
{
	this.status = true;
	this.cache.setWorker(true);
};

/**
 * setOffline
 * 
 * @description Set database connection status to inactive and disable cache cleaning (off-line mode).
 */
Database.prototype.setOffline = function()
{
	this.status = false;
	this.cache.setWorker(false);
};

/**
 * onResponse
 * 
 * @description Event handler for request responses
 * @param {Object} data
 */
Database.prototype.onResponse = function(data)
{
	if (this.transactions.has(data.transactionId)) {
		this.transactions.get(data.transactionId).successHandler(data.result);
	}
};

/**
 * lookupCache
 * 
 * @description Fetch entry from query cache by hash (checks if hash is valid)
 * @param {String} hash
 * @return {null|Object}
 */
Database.prototype.lookupCache = function(hash)
{
	return this.cache.read(hash);
};

/**
 * query
 * 
 * @deprecated
 * @description Execute simple query defined by query string, parameters and callback.
 * @param {String} queryStr
 * @param {Object} params (optional)
 * @param {Function} callback (optional)
 */
Database.prototype.query = function(queryStr, params, callback)
{
	// check arguments
	console.log(queryStr, params);
	var queryObj;
	if (arguments.length > 0) {
		queryObj = new Query();
		queryObj.setQueryString(queryStr);
		params.session=session_id_get();
		if (arguments.length > 1) queryObj.setParameters(params);
		if (arguments.length > 2) queryObj.setSuccessHandler(callback);
		this.executeQuery(queryObj);
	} else {
		return;
	}
};

/**
 * executeQuery
 * 
 * @description Execute query defined by query object.
 * @param {Query} queryObj
 */
Database.prototype.executeQuery = function(queryObj)
{
	var ctx = this;
	// prepare request
	var request = function()
	{
		// look up query cache
		var combinedHash = queryObj.getHash();
		if (queryObj.type == queryObj.GET || ctx.getStatus() == false) {
			var cachedResult = ctx.lookupCache(combinedHash);
			if (cachedResult) {
				// debug
				console.log("from cache ->", cachedResult);
				// call success handler and exit function
				if (queryObj.onSuccess) {
					queryObj.onSuccess(cachedResult);
				}
				return;
			}
		}
		// temporarily disabled, handled by websocket interface
		// if (ctx.getStatus() == false) {
		// // debug
		// console.log("error -> Database offline");
		// // call failure handler
		// if (queryObj.onFailure) {
		// queryObj.onFailure();
		// }
		// return;
		// }
		// block
		if (queryObj.queue) {
			queryObj.queue.block();
		}
		// create callback object
		var ret = {
			successHandler : function(r)
			{
				// map classes
				if (queryObj.mapper) {
					//r.isMapped = true;
					var m = { root: r };
					queryObj.mapper.mapClasses(m);
					queryObj.mapper.data = null; // @TODO
					r = m.root;
				}
				// store in query cache
				if ((queryObj.type == queryObj.GET) && queryObj.doCache) {
					ctx.cache.write(combinedHash, r);
				}
				// debug
				console.log("server response ->", r);
				// call success handler
				if (queryObj.onSuccess) {
					queryObj.onSuccess(r);
				}
				// unblock
				if (queryObj.queue) {
					queryObj.queue.unblock();
				}
			},
			failureHandler : function(error)
			{
				// debug
				console.log("error ->", error);
				// call failure handler
				if (queryObj.onFailure) {
					queryObj.onFailure();
				}
				// unblock
				if (queryObj.queue) {
					queryObj.queue.unblock();
				}
			}
		};
		// prepare transfer object and send
		var transactionId = model_helper_createGuid();
		var DTO = {
			action : "query",
			transactionId : transactionId,
			query : queryObj.queryString,
			parameters : queryObj.parameters,
			
		};
		ctx.transactions.put(transactionId, ret);
		websocket_send(DTO);
	};
	// process
	if (queryObj.queue) {
		queryObj.queue.enqueue(request);
	} else {
		request();
	}
};

/**
 * @class Cache
 * @description Provides cache service with cleaner worker.
 * @constructor
 */
function Cache()
{
	this.data = {};
	this.worker = null;
}

/**
 * setWorker
 * 
 * @description Enable or disable automatic cache cleaner. True = On-line mode, False = Off-line mode.
 * @param {boolean} mode
 */
Cache.prototype.setWorker = function(mode)
{
	var ctx = this;
	if (this.worker) {
		window.clearInterval(this.worker);
	}
	if (mode) {
		this.worker = window.setInterval(function()
		{
			ctx.clean({
				lastAccess : config.database.cache_invalidateAfterLastAccess,
				creation : config.database.cache_invalidateAfterCreation
			});
		}, config.database.cache_invalidateCheckInterval);
	}
};

/**
 * clean
 * 
 * @description Remove expired entries (will remove all entries immediately if TTL is not specified)
 * @param {Object} ttl (optional)
 */
Cache.prototype.clean = function(ttl)
{
	// iterate through all cache items
	var hash = null;
	for (hash in this.data) {
		if (this.data.hasOwnProperty(hash)) {
			var cacheEntry = this.data[hash];
			var currentTime = model_helper_createTimestamp();
			// check if entry is expired
			if ((ttl == null) || (cacheEntry.lastAccess < (currentTime - ttl.lastAccess)) || (cacheEntry.timestamp < (currentTime - ttl.creation))) {
				if (cacheEntry.expires) {
					// remove expired entry from cache
					this.data[hash] = {};
					delete this.data[hash]; // TODO: GC friendly?
				}
			}
		}
	}
};

/**
 * read
 * 
 * @description Fetch entry from cache by hash (does not check validity of hash)
 * @param {String} hash
 * @return {null|Object}
 */
Cache.prototype.read = function(hash)
{
	if (this.data.hasOwnProperty(hash)) {
		// entry found
		var cacheEntry = this.data[hash];
		// set last access to current time stamp
		cacheEntry.lastAccess = model_helper_createTimestamp();
		return cacheEntry.content;
	}
	return null;
};

/**
 * write
 * 
 * @description Insert new or update existing cache item
 * @param {String} hash
 * @param {Object} content
 * @param {boolean} expires (optional) = true on default
 */
Cache.prototype.write = function(hash, content, expires)
{
	if (content) {
		var curTime = model_helper_createTimestamp();
		if (typeof expires != "boolean") expires = true;
		this.data[hash] = {
			// object to store
			content : $.extend(true, {}, content), // TODO: best solution?
			// creation time (current time stamp)
			timestamp : curTime,
			// last access (current time stamp)
			lastAccess : curTime,
			// expire flag
			expires : expires
		};
	}
};

/**
 * @class Mapper
 * @description Iterative class mapper for JSON objects. Uses className attribute to match class.
 * @constructor
 */
function Mapper()
{
	HashMap.call(this);
	this.objectRegistry = new HashMap();
}

/**
 * __proto__
 */
Mapper.prototype.__proto__ = HashMap.prototype;

/**
 * register
 * 
 * @description Alias for HashMap.put.
 * @param {String} hash
 * @param {Function} value
 */
Mapper.prototype.register = function(hash, value)
{
	this.put(hash, value);
};

/**
 * mapClasses
 * 
 * @description iterative deep class mapping
 * @param {Object} obj
 * @param {Object} parent
 */
Mapper.prototype.mapClasses = function(obj, parent)
{
	if (!(obj instanceof Array)) var parent = obj;
	// iterate through all child elements
	var e; for (e in obj) {
		// check if child element is not null
		if (obj[e] != null) {
			// instantiate new object from class constructor
			var mObj = null;
			if (typeof this.data[obj[e].className] == "function") {
				mObj = this.data[obj[e].className](obj, parent);
			} else {
				if (window[obj[e].className]) {
					if ((window[obj[e].className].prototype instanceof ContentTree) || (window[obj[e].className].prototype == ContentTree.prototype)) {
						// instantiate class for child object and assign parent (current object) to it
						if (this.objectRegistry.has(obj[e].guid)) {
							mObj = this.objectRegistry.get(obj[e].guid);
						}
						else {
							mObj = (new window[obj[e].className]()).map(obj[e], parent);
							this.objectRegistry.put(obj[e].guid, mObj);
						}
					}
				}
			}
			// replace child element with new object
			if (mObj != null) {
				obj[e] = mObj;
			}
			// if child element is not already mapped, map recursively
			if (obj[e].isMapped !== true) {
				obj[e].isMapped = true;
				if (typeof obj[e] == "object") {
					this.mapClasses(obj[e], parent);
				}
			}
		}
	}
};

/**
 * @class Queue
 * @description this class implements a ring buffer
 * @constructor
 * @param {Number} size (optional, default = unrestricted)
 * @param {Number} delay (optional, default = 100ms)
 * @param {Function} callback (optional, default = execute queued entry as function)
 */
function Queue(size, delay, callback)
{
	this.buffer = new Array();
	this.worker = null;
	this.isBlocked = false;
	this.size = size > 0 ? size : null;
	this.delay = delay > 0 ? delay : 100;
	this.callback = (typeof callback == "function") ? callback : function(r)
	{
		r();
	};
}

/**
 * setCallback
 * 
 * @description set new callback
 * @param {Function} callback
 */
Queue.prototype.setCallback = function(callback)
{
	if (typeof callback == "function") this.callback = callback;
};

/**
 * setDelay
 * 
 * @description set new delay, active worker will be restarted
 * @param {Number} delay
 */
Queue.prototype.setDelay = function(delay)
{
	var isWorking = this.worker ? true : false;
	this.stop();
	if (delay) this.delay = delay;
	if (isWorking) this.start();
};

/**
 * block
 * 
 * @description enable queue processing
 */
Queue.prototype.block = function()
{
	this.isBlocked = true;
};

/**
 * unblock
 * 
 * @description disable queue processing
 */
Queue.prototype.unblock = function()
{
	this.isBlocked = false;
};

/**
 * enqueue
 * 
 * @description add new entry to queue
 * @param entry
 */
Queue.prototype.enqueue = function(entry)
{
	if (this.size && ((this.buffer.length + 1) > this.size)) {
		this.buffer.splice(0, 1);
	}
	this.buffer.push(entry);
};

/**
 * dequeue
 * 
 * @description remove next entry from queue stack and return it
 * @returns
 */
Queue.prototype.dequeue = function()
{
	var entry = this.buffer[0];
	this.buffer.splice(0, 1);
	return entry;
};

/**
 * start
 * 
 * @description activate processing
 */
Queue.prototype.start = function()
{
	// deactivate processing
	this.stop();
	var ctx = this;
	// setup worker
	this.worker = window.setInterval(function()
	{
		// do not process, if queue is blocked and only one element left
		if ((ctx.buffer.length > 1) || (!ctx.isBlocked)) {
			if (ctx.buffer.length > 0) {
				// fetch next element from queue
				var r = ctx.dequeue();
				if (!ctx.isBlocked) {
					// call element handler, if queue is not blocked
					if (ctx.callback && (typeof ctx.callback == "function")) {
						ctx.callback(r);
					}
				}
			}
		}
	}, this.delay);
};

/**
 * stop
 * 
 * @description deactivate processing
 */
Queue.prototype.stop = function()
{
	if (this.worker) {
		window.clearInterval(this.worker);
	}
};

/**
 * @class Query
 * @description Constructs query object with parameters and options.
 * @constructor
 * @param {String} queryStr (optional)
 * @param {Object} options (optional)
 */
function Query(queryStr, options)
{
	this.PUT = "put";
	this.GET = "get";
	this.type = this.GET;
	this.queryString = "";
	if (queryStr) this.queryString = queryStr;
	this.parameters = {};
	this.doCache = true;
	this.onSuccess = null;
	this.onFailure = null;
	this.queue = null;
	this.mapper = null;
	if (Mapper) this.mapper = new Mapper();
	if (options) this.setOptions(options);
}

/**
 * getHash
 * 
 * @description return hash string based on query string and parameters
 * @returns {String}
 */
Query.prototype.getHash = function()
{
	var paramHash = "";
	if (JSON && Hashes && Hashes.MD5) {
		var md5 = new Hashes.MD5;
		var objAsString = "";
		try {
			objAsString = model_helper_stringify(this.parameters);
		} catch (error) {
			console.log("Error stringify object", error);
		}
		paramHash = md5.hex(objAsString);
		return this.queryString + "_" + paramHash;
	}
	return null;
};

/**
 * setOptions
 * 
 * @description apply new options
 * @param {Object} options
 */
Query.prototype.setOptions = function(options)
{
	if (options.type) this.setType(options.type);
	if (options.queryString) this.setQueryString(options.queryString);
	if (options.onSuccess) this.setSuccessHandler(options.onSuccess);
	if (options.onFailure) this.setFailureHandler(options.onFailure);
	if (options.doCache) this.setCache(options.doCache);
	if (options.parameters) this.setParameters(options.parameters);
	if (options.queue) this.setQueue(options.queue);
	if (options.mapper) this.setMapper(options.mapper);
};

/**
 * setType
 * 
 * @description change query type (should be Query.GET or Query.PUT)
 * @param {String} value
 */
Query.prototype.setType = function(value)
{
	if (value == this.PUT) {
		this.setCache(false);
	}
	this.type = value;
};

/**
 * setQueryString
 * 
 * @description change query string
 * @param {String} value
 */
Query.prototype.setQueryString = function(value)
{
	this.queryString = value;
};

/**
 * setParameter
 * 
 * @description set new parameters object
 * @param {Object} value
 */
Query.prototype.setParameters = function(value)
{
	if (typeof value == "object") this.parameters = value;
};

/**
 * setCache
 * 
 * @description set cache mode (true = enabled or false = disabled)
 * @param {boolean} value
 */
Query.prototype.setCache = function(value)
{
	this.doCache = value;
};

/**
 * setSuccessHandler
 * 
 * @description set new success handler function, gets called when server returns successful response
 * @param {Function} value
 */
Query.prototype.setSuccessHandler = function(value)
{
	if (typeof value == "function") this.onSuccess = value;
};

/**
 * setFailureHandler
 * 
 * @description set new failure handler function, gets called when server does not return successful response
 * @param {Function} value
 */
Query.prototype.setFailureHandler = function(value)
{
	if (typeof value == "function") this.onFailure = value;
};

/**
 * setQueue
 * 
 * @description set new Queue, so the request will be queued into the specified queue
 * @param {Queue} value
 */
Query.prototype.setQueue = function(value)
{
	if (value instanceof Queue) this.queue = value;
};

/**
 * setMapper
 * 
 * @description set new Mapper, only needs to be called when custom mapper shall be applied
 * @param {Mapper} value
 */
Query.prototype.setMapper = function(value)
{
	if (value instanceof Mapper) this.mapper = value;
};
