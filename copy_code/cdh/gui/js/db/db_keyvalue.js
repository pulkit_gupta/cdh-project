// classic variant
Database.prototype.getTranslations = function(language, callback)
{
	this.query("translation", {
		"language" : language
	}, callback);
};

// alternative variant
Database.prototype.getTranslationsAlternative = function(language, callback)
{
	Database.executeQuery(new Query("translation", {
		params : language,
		onSuccess : callback
	}));
};

// data access object variant
Database.prototype.getTranslationDao = function()
{
	function TranslationDao()
	{
		this.getTranslations = function(language, callback)
		{
			Database.executeQuery(new Query("translation", {
				params : language,
				onSuccess : callback
			}));
		};
	}
	return new TranslationDao();
};
