/*
 * the editmode widget, provides a grey overlay to mark currently uneditable areas
 * and manages the editmode = copy editable content and start editor etc.
 */

function widget_editmode()
{
	// an empty constructor
}

// the initializer of the editmode
var widget_editmode_init = function()
{
		// empty
};

/*
 * entereditmode:
 *  - a function to handle the editmode - add grey canvas layer, copy original content into the editdiv and start editor
 *  
 *  originalelement {html-element} the canvas-element which shall be edited
 *  informationobject {object} the json-object which contains the content to edit and to display
 */
widget_editmode.prototype.entereditmode = function(originalelement, informationobject)
{
	var ctx = this;
	var greycanvas = document.getElementById("editmode_greycanvas").getContext("2d");
	
	// make the grey overlay visible
	if(greycanvas.hasClass("invisible"))
	{
		greycanvas.removeClass("invisible");
		greycanvas.addClass("visible");
	}
	
	// make the editdiv visible and fill it with the content to be edited
	var editdiv = document.getElementById('editmode_editdiv'); //$NON-NLS-1$
	
	// set the position of the editdiv
	editdiv.setAttribute("style", "width:" + originalelement.width + "px; height:" + originalelement.height + "px;"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ 
	editdiv.innerHTML = informationobject[originalelement.id];
	
	if(editdiv.hasClass("invisible"))
	{
		editdiv.removeClass("invisible");
		editdiv.addClass("visible");
	}
	
	greycanvas.click(ctx.exiteditmode(originalelement, informationobject, editdiv));
	
	//TODO: editor starten im editDiv
};

/*
 * exiteditmode:
 *  - a function to handle the editmode - exit editor, copy edited content and remove the grey layer
 *  
 *  originalelement {html-element} the canvas-element which shall be edited
 *  informationobject {object} the json-object which contains the content to edit and to display
 *  editdiv {html-element} the div with the embedded editor
 */
widget_editmode.prototype.exiteditmode = function(originalelement, informationobject, editdiv)
{
	// only if the parentElement is not a textbox
	if( !(
                originalelement.tagName && 
                originalelement.tagName.toLowerCase() === "input" && 
                originalelement.type.toLowerCase() === "text" 
            ))
	{
		// copy edited content into the informationobject
		if(editdiv.innerHTML !== informationobject[originalelement.id])
		{
			informationobject[originalelement.id][content] = editdiv.innerHTML;
		}
		
		// write edited content into the originalelement
		var originalcanvas = originalelement.getContext('2d');
		originalcanvas.font = informationobject[originalelement.id][font];
		originalcanvas.textBaseline = "top";
		originalcanvas.fillText(editdiv.innerHTML, 0, 0);
	}
	
	// delete content of the editdiv
	editdiv.innerHTML = "";
	
	// make the editdiv invisible
	if(editdiv.hasClass("visible"))
	{
		editdiv.removeClass("visible");
		editdiv.addClass("invisible");
	}
	
	// make the grey overlay invisible
	if(greyCanvas.hasClass("visible"))
	{
		greyCanvas.removeClass("visible");
		greyCanvas.addClass("invisible");
	}
};