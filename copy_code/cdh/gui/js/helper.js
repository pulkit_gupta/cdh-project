/*
 * helper.js
 * 
 * central place for all shared custom helper functions
 * 
 */

/**
 * @class Benchmark
 * @constructor
 */
Benchmark = function()
{
	this.startTime = null;
	this.endTime = null;
	this.history = [];
};

/**
 * getTimestamp
 * 
 * @returns
 */
Benchmark.getTimestamp = function()
{
	return (new Date()).getTime();
};

/**
 * start
 */
Benchmark.prototype.start = function()
{
	this.reset();
	this.startTime = Benchmark.getTimestamp();
};

/**
 * stop
 */
Benchmark.prototype.stop = function()
{
	this.endTime = Benchmark.getTimestamp();
	this.history.push((this.endTime - this.startTime));
};

/**
 * reset
 */
Benchmark.prototype.reset = function()
{
	this.startTime = 0;
	this.endTime = 0;
};

/**
 * getElapsedTime
 * 
 * @returns
 */
Benchmark.prototype.getElapsedTime = function()
{
	return (this.endTime - this.startTime);
};

/**
 * getRuns
 * 
 * @returns
 */
Benchmark.prototype.getRuns = function()
{
	return this.history.length;
};

/**
 * getAvgTime
 * 
 * @returns
 */
Benchmark.prototype.getAvgTime = function()
{
	var i;
	var l = this.history.length;
	var t = 0;
	for (i = 0; i < l; i++) {
		t += this.history[i];
	}
	return (t / ((l > 0) ? l : 1));
};

/**
 * getMinTime
 * 
 * @returns
 */
Benchmark.prototype.getMinTime = function()
{
	var i;
	var l = this.history.length;
	var m = Benchmark.getTimestamp();
	for (i = 0; i < l; i++) {
		m = Math.min(m, this.history[i]);
	}
	return m;
};

/**
 * getMaxTime
 * 
 * @returns
 */
Benchmark.prototype.getMaxTime = function()
{
	var i;
	var l = this.history.length;
	var m = 0;
	for (i = 0; i < l; i++) {
		m = Math.max(m, this.history[i]);
	}
	return m;
};

/**
 * @class HashMap
 * @description Implements hash map data structure as object with administrative methods.
 * @constructor
 */
function HashMap()
{
	this.data = {};
}

/**
 * has
 * 
 * @description Check if hash exists in hash map.
 * @param {String} hash
 * @returns {boolean}
 */
HashMap.prototype.has = function(hash)
{
	if (typeof this.data[hash] != "undefined") { return true; }
	return false;
};

/**
 * get
 * 
 * @description Get value by hash.
 * @param {String} hash
 * @returns {Object}
 */
HashMap.prototype.get = function(hash)
{
	if (typeof this.data[hash] != "undefined") { return this.data[hash]; }
	return null;
};

/**
 * put
 * 
 * @description Add new entry.
 * @param {String} hash
 * @param {Object} value
 */
HashMap.prototype.put = function(hash, value)
{
	if (typeof value != "undefined") {
		this.data[hash] = value;
	}
};

/**
 * remove
 * 
 * @description Remove entry by hash.
 * @param {String} hash
 */
HashMap.prototype.remove = function(hash)
{
	if (typeof this.data[hash] != "undefined") {
		this.data[hash] = {};
		delete this.data[hash]; // best solution?
	}
};
