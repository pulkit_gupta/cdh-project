

var login_formsubmit=function()
{
    session_id_set("username="+document.getElementById("usr").value+";passhash="+document.getElementById("pswd").value);
    websocket_send({
        action : "getsession",
        cookie : "username="+document.getElementById("usr").value+";passhash="+document.getElementById("pswd").value
    });
    return false;
}

var login_canvas_redraw=function()
{
    var cgui=new canvasgui();
    cgui.pseudocss();
    cgui.drawheader("loginform_header");
    cgui.drawfooter("loginform_footer");
}

var login_translate=function()
{
    translator.ask("loginform_usr", "loginform_usr");
    translator.ask("loginform_pass", "loginform_pass");
    translator.ask("loginform_footer", "loginform_footer");
    
}

//*** DOM MANIPULATION ***//
var loginform=function()
{
    $("body").empty().append('\
        <div id="login_wrapper"><div id="login">\
        <canvas id="loginform_header" class="loginform_header"></canvas>\
            <form onsubmit="login_formsubmit(); return false;" method="post" id="loginform_form">\
                <table align="center">\
                    <tr><td><label id="loginform_usr">Nutzerkürzel</label>: </td><td><input type="text" name="usrname" id="usr"></td></tr>\
                    <tr><td><label id="loginform_pass">Passwort</label>: </td><td><input type="password" name="password" id="pswd"></td></tr>\
                <tr><td colspan="2" id="loginbtn_cell">\
                    <input type="submit" value="Anmelden"></td></tr></table>\
            </form>\
        <canvas id="loginform_footer" class="loginform_footer">Einloggen</canvas>\
        </div></div>');
    
    login_translate();
    login_canvas_redraw();
    
    $(window).resize(login_resize);
    login_resize();
    
    //window.setTimeout(login_canvas_redraw, 1000);
}

var login_resize = function () 
{
    // calculates margin-top of login_wrapper;
    // 140px is the height of login-div. See login.css
    var margTop=(($(window).height()-140)/2);
    
    /// 300px is the width of login-div
    var margLeft=(($(window).width()-300)/2);
    
    $("#login_wrapper").css("margin-left",margLeft);
    $("#login_wrapper").css("margin-top",margTop);
}

