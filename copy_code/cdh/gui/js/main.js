"use strict";

var configfile = "conf.json";
var config;
var $overlay;
var translator = new Translator();
var Database = new Database();
var srvroot= "/";

var testBrowser = function() {
    var isOk = true;
    
    isOk = isOk && (typeof (Storage) !== "undefined");
    isOk = isOk && (typeof (WebSocket) !== "undefined");
    
    return isOk && false;
};

// scroll to top of a child element dest=child
$.scrollTo = $.fn.scrollTo = function(dest, duration) {
    $(this).animate({
        scrollTop : $(dest).offset().top
    }, !duration ? 500 : duration);
};

var settings_loadsettings = function(callback){
    $.getJSON(configfile + "?" + Math.random(), function(data) {
        callback($.extend({}, data));
    });
};

var window_init = function(cfg, cb) {
    srvroot = 
        window.location.protocol + "//" + 
        window.location.host + "/" + 
        window.location.pathname + "/";
        
    var page = "";
    
    if (!window.location.hash) {
        page = cfg.defaultpage
    } else {
        page = window.location.hash.substring(
            window.location.hash.indexOf("!") + 1
        );
    }

    // please add all default widgets here
    loadutils.loadwidget("notification", $("#notification_container"), {
        callback : null
    }).loadwidget("settings", $("#settings_container"),{
        callback: function () { widget_settings_load(function (params) { widget_settings_fill_dialog(params); }); }
    }).loadwidget(config.main_nav_widget, $(config.main_nav_id), {
            callback : function() {
                var tmploadfunc = function(){
                    loadutils.loadpage(page, {
                        callback : function() {
                            if (cb) cb(cfg);
                        }
                    });
                    websocket_helper_removeEvent(
                        websocket_global, 
                        "open", 
                        tmploadfunc
                    );
                };
                if (websocket_global.readyState === websocket_global.OPEN) {
                    tmploadfunc();
                }
                websocket_helper_registerEvent(
                    websocket_global, "open", tmploadfunc);
            }
    });
};

// -- init --
var init = function() {
    // load the config file
    settings_loadsettings(function(c){
        config=c;
        $overlay = $(config.$overlay_id);

        // load all translations from the db
        //translator.init(config);
        websocket_init();
        translator.init({"uilanguage" : config.uilanguage});
        
        // load stuff from settings_database
        widget_settings_load(function (cfg) {
            translator.init({"uilanguage" : cfg.uilanguage});   
            window_init(cfg, function (params)
            {
                show_notifications=params.notifications;
                widget_settings_fill_dialog(params);
            });
            $(window).on('hashchange', function(){
                loadutils.overlayShow();
                window_init(cfg, function (params)
                {
                    show_notifications=params.notifications;
                    widget_settings_fill_dialog(params);
                });
            });
        });
        
        
    });
};

// events
$(window).load(init);
