/*
 * the masterdata page, which gathers and displays the several masterdata-widgets
 *
 * storevalues:
 *     - method to read the values from the textfields and store the modification into the database
 *
 * navigationsettings:
 *  - method to configure the navigation bar -> add page specific functionality to buttons
 * 
 * editmode:
 * 	- method to control the editmode of the loaded widgets
 * 		-> standard is inactive
 *
 * loadData:
 * 		method to insert datasets from the database
 *
 * resize:
 *	- method to resize the window
 *
 * init:
 *  - the init function which should be called by the loader
 *
 */

/*
 * several content objects with demo-data
 */
var contactpersonlist_headline = [
	{
		key : "salutation",
		text: "Anrede"
	},
	{
		key : "lastname",
		text: "Name"
	},
	{
		key : "position",
		text: "Position"
	},
	{
		key : "telephone",
		text: "Telefon"
	},
	{
		key : "mobilephone",
		text: "Mobiltelefon"
	},
	{
		key : "email",
		text: "Email"
	}
];

var demoData_masterdata =
{
	main:{
		identifier:1,
		clientnumber:"12",
		companyname:"Musterfirma",
		nameaffix:"",
		adress:"Musterstr. 1",
		zipcode:"01234",
		city:"Musterhausen",
		country:"Musterland",
		telephone:"0123456789",
		fax:"",
		mobilephone:"",
		email:"info@mustermann.de",
		website:"www.mustermann.de",
		comment:"das ist eine tolle firma",
		category:"Werbefirma",
		clienttyp:"corporateclient",
		maincontact_name:"Herr Mustermann",
		maincontact_identifier:1,
		logopath:"images/dummy_logo_300_300.png"
				},
	
	contactpersons:{
		headline:[
			{
				key : "salutation",
				text: "Anrede"
			},
			{
				key : "lastname",
				text: "Name"
			},
			{
				key : "position",
				text: "Position"
			},
			{
				key : "telephone",
				text: "Telefon"
			},
			{
				key : "mobilephone",
				text: "Mobiltelefon"
			},
			{
				key : "email",
				text: "Email"
			}
						 ],
		content:[
			{
				identifier:1,
				salutation:"mr",
				title:"no",
				surname:"Max",
				lastname:"Mustermann",
				position:"Chef",
				telephone:"0371/214567",
				fax:"",
				mobilphone:"",
				email:"info@mustermann.de",
				birthday:"01.01.1900",
				bithdaymailing:true,
				notes:"Hier stehen Notzien zu:\n - Max Mustermann"
			}
						]
								 },
	finance:{
		paymentcondition:"14 Tage 3%Skonto, 30 Tage netto",
		deliverycondition:"frei Haus",
		shippingtyp:"DHL Paket",
		shippingnote:"zu Hd. Herr Mustermann",
		debitorennumber:"12345",
		taxnumber:"277 227 227",
		accountholder:"Firma Mustermann",
		accountnumber:"12.123.12345",
		blz:"1234567",
		bank:"Musterbank",
		delegate:"Frau Musterfrau",
		catalogues:"keine",
		discount:"0%",
		discountnote:"dem Kunde wird kein Rabatt gewährt"
					},
	
	adresses:{
		headline:[
			{
				key : "companyname",
				text: "Firma"
			},
			{
				key : "adress",
				text: "Adresse"
			},
			{
				key : "zipcode",
				text: "PLZ"
			},
			{
				key : "city",
				text: "Ort"
			},
			{
				key : "country",
				text: "Land"
			}
							],
		content:[
			{
				identifier:1,
				companyname:"Musterfirma",
				nameaffix:"z.Hd. Herr Mustermann",
				adress:"Musterstr. 1",
				zipcode:"01234",
				city:"Musterhausen",
				country:"Musterland",
				deliverynote:"Der Lieferung keine Rechnung beilegen!"
			}
						]
					 },
	
	additional:{
		crmversion:"2.1",
		syndicatemember:"Nein",
		exhibitions:"CeBit"
						 }
};

/*
 * storevalues:
 *  - method to read the values from the textfields and store the modification into the database
 *
 */
var page_masterdata_storevalues = function()
{
    // trigger the store-function of the loaded widgets
};

/*
 * navigationsettings:
 *  - method to configure the navigation bar -> add page specific functionality to buttons
 */
var page_masterdata_navigationsettings = function()
{
    // Home-button
    // set action of home-button to open clipboard and reset all masterdata-reconfigurations
    $("#nav_toclipboard").unbind("click", widget_nav_defaultaction);
    $("#nav_toclipboard").bind("click", function(event){
        page_masterdata_storevalues();
        widget_nav_defaultaction;
        window.location.hash="!clipboard";

        // unbind other changed event-handlers
        $("#nav_reload").unbind("click", page_masterdata_storevalues);
        $("#nav_reload").click(widget_nav_defaultaction);

        // unbind the customized action of this button itself
        $(this).unbind(event);
        return false;
    });

    // Reload-button
    // TODO: correct jump-destination
    widget_nav_bind("reload", function()
		{
        page_masterdata_storevalues();
        window.location.reload();
    });

    // Area-buttons
    // Address-button
    widget_nav_bind("masterdata_address", function()
		{
        $("#masterdata_page_content_main_div").scrollIntoView(true);
    });

    // Contactperson-button
    widget_nav_bind("masterdata_contactperson", function()
		{
        $("#masterdata_page_content_contactpersons_div").scrollIntoView(true);
    });

    // Addressarea-button
    widget_nav_bind("masterdata_furtheraddress", function()
		{
        $("#masterdata_page_content_address_div").scrollIntoView(true);
    });

    // Finance-button
    widget_nav_bind("masterdata_finance", function(e)
		{
        $("#masterdata_page_content_finance_div").scrollIntoView(true);
    });

    // AdditionalInformation-button
    widget_nav_bind("masterdata_additional", function(e)
		{
			e.preventDefault();
			$("#masterdata_page_wrapper").scrollTo("#masterdata_page_content_additional_div");
			return false;
    });
};

/*
 * editmode:
 *  - method to control the editmode of the loaded widgets
 * 	-> standard is inactive
 *  - enabled {boolean} control whether to set the widget editable or not
 */
var page_masterdata_editmode = function(enabled)
{
    enabled = !enabled;
    widget_masterdata_additional_editmode(enabled);
    widget_masterdata_adresses_editmode(enabled);
    widget_masterdata_contactpersons_editmode(enabled);
    widget_masterdata_main_editmode(enabled);
    widget_masterdata_finance_editmode(enabled);
};

/*
 * loadData:
 *	 - method to insert datasets from the database
 *	 - customerID {integer} identify the dataset of the current customer
 */
var page_masterdata_loadData = function(customerID)
{
	console.log("page_masterdata_loadData")
	
		console.log(overviewlist_demodata_client_content[customerID].main)
		widget_masterdata_main_fillInValues(demoData_masterdata_main);
		widget_masterdata_finance_fillInValues(demoData_masterdata_finance);
		widget_masterdata_additional_fillInValues(demoData_masterdata_additional);
		//widget_masterdata_adresses_insertAdresses(widget_masterdata_adresses_data_listcontent);
		//widget_masterdata_contactpersons_insertPersons(widget_masterdata_contactpersons_data_listcontent);
		loadutils.overlayHide();
	/*
	Database.query("masterdata", {"customerID" : customerID}, function(data)
	{
		console.log(data)
		widget_masterdata_main_fillInValues(data.main);
		widget_masterdata_finance_fillInValues(data.finance);
		widget_masterdata_additional_fillInValues(data.additional);
		widget_masterdata_adresses_insertAdresses(data.adresses);
		widget_masterdata_contactpersons_insertPersons(data.contactpersons);
		loadutils.overlayHide();
	});
	*/
};

/* todo: move code into right places !*/

/*
 * main_init:
 *  - methods to instanciate and fill the widgets with the demo-data
 */
var page_masterdata_main_init = function()
{
	widget_masterdata_main_editmode(false);
	widget_masterdata_main_fillInValues(demoData_masterdata_main);
}
var page_masterdata_contactpersons_init = function()
{
	widget_masterdata_contactpersons_editmode(false);
	//widget_masterdata_contactpersons_insertPersons(widget_masterdata_contactpersons_data_listcontent);
	widget_masterdata_personeditor_fillInValues(widget_masterdata_contactpersons_data_listcontent);
}
var page_masterdata_adresses_init = function()
{
	widget_masterdata_adresses_editmode(false);
	//widget_masterdata_adresses_insertAdresses(widget_masterdata_adresses_data_listcontent);
	widget_masterdata_adresseditor_fillInValues(widget_masterdata_adresses_data_listcontent);
}
var page_masterdata_finance_init = function()
{
	widget_masterdata_finance_editmode(false);
	widget_masterdata_finance_fillInValues(demoData_masterdata_finance);
}
var page_masterdata_additional_init = function()
{
	widget_masterdata_additional_editmode(false);
	widget_masterdata_additional_fillInValues(demoData_masterdata_additional);
}

/*
 * method to resize the window
 */
var page_masterdata_resize = function()
{
    $("#masterdata_page_wrapper").height(
        $(window).height()- 32 // todo: should be calculated because the nav 
                               // can not be resized atm
    );
};

/*
 * init:
 *  - the init function which should be called by the loader
 */
var page_masterdata_init = function()
{
    $("#masterdata_wrapper").ready(function(){
        
        //draw canvas for every section
        var cgui = new canvasgui();
        cgui.pseudocss();
        cgui.drawheader("masterdata_page_main_canvas");
        cgui.drawheader("masterdata_page_contactpersons_canvas");
        cgui.drawheader("masterdata_page_adresses_canvas");
        cgui.drawheader("masterdata_page_finance_canvas");
        cgui.drawheader("masterdata_page_additional_canvas");

        //load the widgets
        loadutils.loadwidget("masterdata_main", 
        	$("#masterdata_page_main_area"), {
        		callback: page_masterdata_main_init, 
        		initargs:false
        	});

        loadutils.loadwidget("masterdata_contactpersons", 
        	$("#masterdata_page_contactpersons_area"), {
        		callback: page_masterdata_contactpersons_init, 
        		initargs:false
        	});

        loadutils.loadwidget("masterdata_adresses", 
        	$("#masterdata_page_adresses_area"), {
        		callback: page_masterdata_adresses_init, 
        		initargs:false
        	});

        loadutils.loadwidget("masterdata_finance", 
        	$("#masterdata_page_finance_area"), {
        		callback: page_masterdata_finance_init, 
        		initargs:false
        	});

        loadutils.loadwidget("masterdata_additional", 
        	$("#masterdata_page_additional_area"), {
        		callback: page_masterdata_additional_init, 
        		initargs:false
        	});        
        
	    $(window).resize(page_masterdata_resize);

        // show the masterdata menus
        widget_nav_toggle("main",true);
        widget_nav_toggle("masterdata_main",true);
        widget_nav_toggle("masterdata_context",true);

        // change some button actions
        page_masterdata_navigationsettings();
        
        page_masterdata_loadData(1);
        
        page_masterdata_resize();
				//loadutils.overlayHide();
    });
};