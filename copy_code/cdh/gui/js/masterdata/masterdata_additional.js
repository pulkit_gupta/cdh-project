/*
 * widget to provide inputs for additional information to masterdata-datasets
 *
 * editmode:
 * 	- method to control the editmode of the loaded widgets
 * 		-> standard is inactive
 *
 * clearValues:
 *	- method to delete all input
 * 
 * fillInValues:
 *	- method to fill in values of a provided data object
 *
 * getValues:
 *	- read all the values from the inputs and return them as a data object
 *
 * init:
 *  - the init function which should be called by the loader
 */
 
/*
 * a content object with demo-data
 */
var demoData_masterdata_additional = 	{	
	crmversion:"2.1",
	syndicatemember:"Nein",
	exhibitions:"CeBit"
}

/*
 * editmode:
 *  - function which enables or disables input/selection fields
 *
 * enabled {boolean} selector if the editmode shall be enabled or not
 */
var widget_masterdata_additional_editmode = function(enabled)
{
	enabled = !enabled;
	//row left
	$("#widget_masterdata_additional_crmversion_input").prop('disabled', enabled);
	$("#widget_masterdata_additional_syndicatemember_input").prop('disabled', enabled);
	$("#widget_masterdata_additional_exhibitions_input").prop('disabled', enabled);
}

/*
 * clearValues:
 *  - clear all inputs
 */
var widget_masterdata_additional_clearValues = function()
{	
	$("#widget_masterdata_additional_crmversion_input").val("");
	$("#widget_masterdata_additional_syndicatemember_input").val("");
	$("#widget_masterdata_additional_exhibitions_input").val("");	
}

/*
 * fillInValues:
 *  - fills the input with values
 *  - data {object} of this format:
 *	{	
 *		crmversion:{String},
 *		syndicatemember:{String},
 *  	exhibitions:{String}
 *	}
 */
var widget_masterdata_additional_fillInValues = function(data)
{

	$("#widget_masterdata_additional_crmversion_input").val(data.crmversion);
	$("#widget_masterdata_additional_syndicatemember_input").val(data.syndicatemember);
	$("#widget_masterdata_additional_exhibitions_input").val(data.exhibitions);
}

/*
 * getValues:
 *  - reads the values of all inputs and return an object
 *	returns {object} of this format:
 *	{	
 *		crmversion:{String},
 *		syndicatemember:{String},
 *		exhibitions:{String}
 * 	}
 */
var widget_masterdata_additional_getValues = function()
{
	var data = new Object;
	
	data.crmversion = $("#widget_masterdata_additional_crmversion_input").val();
	data.syndicatemember = $("#widget_masterdata_additional_syndicatemember_input").val();
	data.exhibitions = $("#widget_masterdata_additional_exhibitions_input").val();
	
	return data;	
}

/*
 * init:
 *  - the init function which should be called by the loader 
 */
var widget_masterdata_additional_init = function()
{
	console.log("widget_masterdata_additional_init");
	widget_masterdata_additional_clearValues();
	widget_masterdata_additional_editmode(false);
}
