/*
 * widget to provide inputs for the address editor to masterdata-datasets
 *
 * editmode:
 * 	- method to control the editmode of the loaded widgets
 * 		-> standard is inactive
 *
 * clearValues:
 *	- method to delete all input
 * 
 * fillInValues:
 *	- method to fill in values of a provided data object
 *
 * getValues:
 *	- read all the values from the inputs and return them as a data object
 *
 * init:
 *  - the init function which should be called by the loader
 */
 
 /*
 * a content object with demo-data
 */
var demoData_masterdata_adresseditor = 	{
	companyname:"Musterfirma",
	nameaffix:"z.Hd. Herr Mustermann",
	adress:"Musterstr. 1",
	zipcode:"01234",
	city:"Musterhausen",
	country:"Musterland",
	deliverynote:"Der Lieferung keine Rechnung beilegen!"
											
}

//identifier of the current adress										
var widget_masterdata_adresseditor_current_adress_identifier = null;


/*
 * editmode:
 *  - method which enables or disables input/selection fields
 *
 * enabled {boolean} selector if the editmode shall be enabled or not
 */
var widget_masterdata_adresseditor_editmode= function(enabled)
{
	enabled = !enabled;
	$("#widget_masterdata_adresseditor_companyname_input").prop('disabled', enabled);
	$("#widget_masterdata_adresseditor_nameaffix_input").prop('disabled', enabled);
	$("#widget_masterdata_adresseditor_adress_input").prop('disabled', enabled);
	$("#widget_masterdata_adresseditor_zipcode_input").prop('disabled', enabled);
	$("#widget_masterdata_adresseditor_city_input").prop('disabled', enabled);
	$("#widget_masterdata_adresseditor_country_input").prop('disabled', enabled);
	$("#widget_masterdata_adresseditor_deliverynote_textarea").prop('disabled', enabled);	
}

/*
 * clearValues:
 *  - clear all inputs
 */
var widget_masterdata_adresseditor_clearValues = function()
{
		widget_masterdata_adresseditor_current_adress_identifier = null;
		$("#widget_masterdata_adresseditor_companyname_input").val("");
		$("#widget_masterdata_adresseditor_nameaffix_input").val("");
		$("#widget_masterdata_adresseditor_adress_input").val("");
		$("#widget_masterdata_adresseditor_zipcode_input").val("");
		$("#widget_masterdata_adresseditor_city_input").val("");
		$("#widget_masterdata_adresseditor_country_input").val("");
		$("#widget_masterdata_adresseditor_deliverynote_textarea").text("");
}

/*
 * fillInValues:
 *  - fills the input fields with values
 *	- data {object} of the following format:
 *	{			
 * 		identifier:{int},
 *		companyname:{String},
 *		nameaffix:{String},
 *		adress:{String},
 *		zipcode:{String},
 *		city:{String},
 *		country:{String},
 *		deliverynote:{String}									
 *	}
 */
var widget_masterdata_adresseditor_fillInValues = function(data)
{
// console.log("Inhalt von data: " + data); // data is null - should be != null
		widget_masterdata_adresseditor_current_adress_identifier = data.identifier;
		$("#widget_masterdata_adresseditor_companyname_input").val(data.companyname);
		$("#widget_masterdata_adresseditor_nameaffix_input").val(data.nameaffix);
		$("#widget_masterdata_adresseditor_adress_input").val(data.adress);
		$("#widget_masterdata_adresseditor_zipcode_input").val(data.zipcode);
		$("#widget_masterdata_adresseditor_city_input").val(data.city);
		$("#widget_masterdata_adresseditor_country_input").val(data.country);
		$("#widget_masterdata_adresseditor_deliverynote_textarea").text(data.deliverynote);
	
}

/*
 * getValues:
 *  - reads the values of all inputs
 *	- returns {object} of the following format:
 *	{
 *		identifier:{int},
 *		companyname:{String},
 *		nameaffix:{String},
 *		adress:{String},
 *		zipcode:{String},
 *		city:{String},
 *		country:{String},
 *		deliverynote:{String}
 *	}
 */
var widget_masterdata_adresseditor_getValues = function()
{
	var data = new Object;

	data.identifier = widget_masterdata_adresseditor_current_adress_identifier;
	data.companyname = $("#widget_masterdata_adresseditor_companyname_input").val();
	data.nameaffix = $("#widget_masterdata_adresseditor_nameaffix_input").val();
	data.adress = $("#widget_masterdata_adresseditor_adress_input").val();
	data.zipcode = $("#widget_masterdata_adresseditor_zipcode_input").val();
	data.city = $("#widget_masterdata_adresseditor_city_input").val();
	data.country = $("#widget_masterdata_adresseditor_country_input").val();
	data.deliverynote = $("#widget_masterdata_adresseditor_deliverynote_textarea").text();

	return data;	
}

/*
 * init:
 *  - the init function which should be called by the loader
 */
var widget_masterdata_adresseditor_init = function()
{
	console.log("widget_masterdata_adresseditor_init");
	widget_masterdata_adresseditor_clearValues();
	widget_masterdata_adresseditor_editmode(false);
}