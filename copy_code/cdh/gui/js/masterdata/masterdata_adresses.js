/*
 * widget to provide inputs for the address data
 *
 * editmode:
 * 	- method to control the editmode of the loaded widgets
 * 		-> standard is inactive
 *
 * list_elementSelectFunction:
 *  - function called if a list entry in the adresslist is selected
 *
 * list_elementUnselectFunction:
 *  - function called if a list entry in the adresslist is unselected
 *
 * createAdressList:
 *  - creates an adresslist with all addresses
 *
 * insertAdresses:
 *  - insert addresses from the adressArray-object into the overviewList
 * 
 * updateAdresseditor:
 *  - read the selected entry from the overviewlist
 *    and fill in the corresponding data into the textfields
 *
 * init_stage2:
 *  - the secound init stage
 *  - handle the editmode and add click listeners to buttons
 *
 * init:
 *  - the init function which should be called by the loader
 *  - load the widgets
 *
 * new:
 *  - function called if the new button is clicked
 *
 * change:
 *  - function called if the change button is clicked
 *
 * cancel:
 *  - function called if the cancel button is clicked
 *
 * save:
 *  - function called if the save button is clicked
 */
var widget_masterdata_adresses_list = null;

/*
 * several content objects with demo-data
 */
var widget_masterdata_adresses_data_listhead = [
	{
		key : "companyname",
		text: "Firma"
	},
	{
		key : "adress",
		text: "Adresse"
	},
	{
		key : "zipcode",
		text: "PLZ"
	},
	{
		key : "city",
		text: "Ort"
	},

	{
		key : "country",
		text: "Land"
	}																					
];

var widget_masterdata_adresses_data_listcontent = [
	{	
		identifier:1,
		companyname:"Musterfirma",
		nameaffix:"z.Hd. Herr Mustermann",
		adress:"Musterstr. 1",
		zipcode:"01234",
		city:"Musterhausen",
		country:"Musterland",
		deliverynote:"Der Lieferung keine Rechnung beilegen!"
	},
	{	
		identifier:2,
		companyname:"Musterfraufirma",
		nameaffix:"z.Hd. Frau Mustermann",
		adress:"weststr. 1",
		zipcode:"08371",
		city:"Glauchau",
		country:"Deutschland",
		deliverynote:"Der Lieferung keine Rechnung beilegen!"
	}
];

/*
 * editmode:
 *  - function which enables or disbales the editing
 *
 * enabled {boolean} selector if the editmode shall be enabled or not
 */
var widget_masterdata_adresses_editmode = function(enabled)
{
	enabled = !enabled;
	$("#widget_masterdata_adresses_button_new").prop('disabled', enabled);
	$("#widget_masterdata_adresses_button_change").prop('disabled', enabled);
	$("#widget_masterdata_adresses_button_save").prop('disabled', enabled);
	$("#widget_masterdata_adresses_button_cancel").prop('disabled', enabled);
}

/*
 * list_elementSelectFunction:
 *  - function called if a list entry in the adresslist is selected
 *  - identifier {int} the identifier of the dataset
 *  - $elemClicked {object} jQuery object which was clicked
 */
var widget_masterdata_adresses_list_elementSelectFunction = function(identifier, $elemClicked)
{
	//enable change button
	$("#widget_masterdata_adresses_button_change").prop('disabled', false);
	//fillin adresseditor with the selected value in adresslist
	widget_masterdata_adresseditor_fillInValues(widget_masterdata_adresses_list.getSelectedData());
}

/*
 * list_elementUnselectFunction:
 *  - function called if a list entry in the adresslist is unselected
 *  - identifier {int} the identifier of the dataset
 *  - $elemClicked {object} jQuery object which was clicked
 */
var widget_masterdata_adresses_list_elementUnselectFunction = function(identifier, $elemClicked)
{
	//clear values shown in adresseditor and disbale editmode of the adresseditor
	widget_masterdata_adresseditor_clearValues();
	widget_masterdata_adresseditor_editmode(false);
	//disable change button	
	$("#widget_masterdata_adresses_button_change").prop('disabled', true);

}

/*
 * createAdressList:
 *  - creates an adresslist with all addresses
 */
var widget_masterdata_adresses_createAdressList = function()
{
	console.log("widget_masterdata_adresses_createAdressList")
	//create overviewlist 
	widget_masterdata_adresses_list = new overviewlist(
	{
		target:"#widget_masterdata_adresses_listdiv > div.overviewlist_wrapper",
		resizable:true,
		selectFirst:true,
		autoresize:true,
		sortable:true,
		listElemSelectFunction:widget_masterdata_adresses_list_elementSelectFunction,
		listElemSelectFunctionArgs:null,
		listElemUnselectFunction:widget_masterdata_adresses_list_elementUnselectFunction,
		listElemUnselectFunctionArgs:null
	});
	//create headline of overviewlist	
	widget_masterdata_adresses_list.insertHeadLine(widget_masterdata_adresses_data_listhead, {callback:null});
	//create list content
	widget_masterdata_adresses_list.insertDataArray(widget_masterdata_adresses_data_listcontent, {callback:null});
	
}

/*
 * insertAdresses:
 *  - insert the addresses from the adressArray-object into the overviewList
 *  - adressArray {object} of the following format:
 *    {
 * 			identifier:{int},
 * 			companyname:{String},
 * 			nameaffix:{String},
 * 			adress:{String},
 * 			zipcode:{String},
 * 			city:{String},
 * 			country:{String},
 * 			deliverynote:{String}
 *    }
 */
var widget_masterdata_adresses_insertAdresses = function(adressArray)
{
	console.log(adressArray)
	widget_masterdata_adresses_list.insertDataArray(adressArray, {callback:widget_masterdata_adresses_updateAdresseditor});
}

/*
 * updateAdresseditor:
 *  - read the selected entry from the overviewlist
 *    and fill in the corresponding data into the textfields
 */
var widget_masterdata_adresses_updateAdresseditor = function()
{
	widget_masterdata_adresseditor_fillInValues(widget_masterdata_adresses_list.getSelectedData());
}

/*
 * init_stage2:
 *  - the secound init stage
 *  - handle the editmode and add click listeners to buttons
 */
var widget_masterdata_adresses_init_stage2 = function()
{
	//disbale editing of adress editor
	widget_masterdata_adresseditor_editmode(false);
	//fillin adresseditor with the current selected value in adresslist
	widget_masterdata_adresseditor_fillInValues(widget_masterdata_adresses_list.getSelectedData());
		
	//add clickfunctions to the buttons
	$("#widget_masterdata_adresses_button_new").click(function()
	{
		widget_masterdata_adresses_new();
	});
	
	$("#widget_masterdata_adresses_button_change").click(function()
	{
		widget_masterdata_adresses_change();
	});
	
	$("#widget_masterdata_adresses_button_cancel").click(function()
	{
		widget_masterdata_adresses_cancel();
	});
	
	$("#widget_masterdata_adresses_button_save").click(function()
	{
		widget_masterdata_adresses_save();
	});
}

/*
 * init:
 *  - the init function which should be called by the loader
 *  - load the widgets
 */
var widget_masterdata_adresses_init = function()
{
	console.log("widget_masterdata_adresses_init");
		
	//disable editmode
	widget_masterdata_adresses_editmode(true);
	//load listwidget to show all adresses
	loadutils.loadwidget("overviewlist", $("#widget_masterdata_adresses_listdiv"), {callback: widget_masterdata_adresses_createAdressList});
	//load adresseditor to show and edit adress
	loadutils.loadwidget("masterdata_adresseditor", $("#widget_masterdata_adresses_editdiv"), {callback: widget_masterdata_adresses_init_stage2, initargs:null});
}

/*
 * new:
 *  - function called if the new button is clicked
 */
var widget_masterdata_adresses_new = function()
{
	//console.log("widget_masterdata_adresses_new");
	//hide new and change button and show save and cancel buttons
	$("#widget_masterdata_adresses_button_new").hide();
	$("#widget_masterdata_adresses_button_change").hide();
	$("#widget_masterdata_adresses_button_save").show();
	$("#widget_masterdata_adresses_button_cancel").show();
	//clear values shown in adresseditor and enable editmode of the adresseditor
	widget_masterdata_adresseditor_clearValues();
	widget_masterdata_adresseditor_editmode(true);
}

/*
 * change:
 *  - function called if the change button is clicked
 */
var widget_masterdata_adresses_change = function()
{
	//console.log("widget_masterdata_adresses_change");
	//hide new and change button and show save and cancel buttons
	$("#widget_masterdata_adresses_button_new").hide();
	$("#widget_masterdata_adresses_button_change").hide();
	$("#widget_masterdata_adresses_button_save").show();
	$("#widget_masterdata_adresses_button_cancel").show();
	//enable editmode of the adresseditor
	widget_masterdata_adresseditor_editmode(true);
}

/*
 * cancel:
 *  - function called if the cancel button is clicked
 */
var widget_masterdata_adresses_cancel = function()
{
	//console.log("widget_masterdata_adresses_cancel");
	//show new and change button and hide save and cancel buttons
	$("#widget_masterdata_adresses_button_save").hide();
	$("#widget_masterdata_adresses_button_cancel").hide();
	$("#widget_masterdata_adresses_button_new").show();
	$("#widget_masterdata_adresses_button_change").show();
	//disable editmode of the adresseditor
	widget_masterdata_adresseditor_editmode(false);
	//fillin adresseditor with the old values stored in widget_masterdata_adresses_data_listcontent
	widget_masterdata_adresseditor_fillInValues(widget_masterdata_adresses_list.getSelectedData());
}

/*
 * save:
 *  - function called if the save button is clicked
 */
var widget_masterdata_adresses_save = function()
{
	//console.log("widget_masterdata_adresses_save");
	//show new and change button and hide save and cancel buttons
	$("#widget_masterdata_adresses_button_save").hide();
	$("#widget_masterdata_adresses_button_cancel").hide();
	$("#widget_masterdata_adresses_button_new").show();
	$("#widget_masterdata_adresses_button_change").show();
	//disable editmode of the adresseditor
	widget_masterdata_adresseditor_editmode(false);
	
	//TODO add function to save new data to database 
	//temporary save changes only in local var : widget_masterdata_adresses_data_listcontent
	var found = false;
	var tmpdata = widget_masterdata_adresseditor_getValues();
	for (var i = 0; i < widget_masterdata_adresses_data_listcontent.length; i++) 
	{
	  if (widget_masterdata_adresses_data_listcontent[i].identifier == tmpdata.identifier)
	  {	  	
		widget_masterdata_adresses_data_listcontent[i] = tmpdata;
		widget_masterdata_adresses_list.changeEntry(tmpdata, tmpdata.identifier);
		found = true;
	  }
	};
	//TODO use new functions from clientoverview
	if (!found)
	{
		tmpdata.identifier = "tmp_" + Math.floor((Math.random() * 100) + 1); 
		widget_masterdata_adresses_list.clearSelected();
		widget_masterdata_adresses_list.insertData(tmpdata, true, false);
		widget_masterdata_adresses_data_listcontent.push(tmpdata);
		//disable change button	
		$("#widget_masterdata_adresses_button_change").prop('disabled', true);
	}
	console.log(tmpdata.identifier)
	
	widget_masterdata_adresseditor_clearValues();		
}