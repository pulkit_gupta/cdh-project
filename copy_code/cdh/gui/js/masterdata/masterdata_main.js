/*
 * widget to provide inputs for the main data of a masterdata-datasets
 * as name, addresse, logo, categorization etc.
 *
 * editmode:
 * 	- method to control the editmode of the loaded widgets
 * 		-> standard is inactive
 *
 * clearValues:
 *	- method to delete all input
 * 
 * fillInValues:
 *	- method to fill in values of a provided data object
 *
 * getValues:
 *	- read all the values from the inputs and return them as a data object
 *
 * init:
 *  - the init function which should be called by the loader
 */
 
/*
 * a content object with demo-data
 */
var demoData_masterdata_main = 
{
	clientnumber:"12",
	companyname:"Musterfirma",
	nameaffix:"",
	adress:"Musterstr. 1",
	zipcode:"01234",
	city:"Musterhausen",
	country:"Musterland",
	telephone:"0123456789",
	fax:"",
	mobilephone:"",
	email:"info@mustermann.de",
	website:"www.mustermann.de",
	comment:"das ist eine tolle firma",
	category:"Werbefirma",
	clienttyp:"corporateclient",
	maincontact_name:"Herr Mustermann",
	maincontact_identifier:1,
	logopath:"images/dummy_logo_300_300.png"
}

/*
 * editmode:
 *  - method which enables or disables input/selection fields
 */
var widget_masterdata_main_editmode = function(enabled)
{
	enabled = !enabled;
	$("#widget_masterdata_main_clientnumber_input").prop('disabled', enabled);
	$("#widget_masterdata_main_category_input").prop('disabled', enabled);
	$("#widget_masterdata_main_clienttyp_select").prop('disabled', enabled);
	$("#widget_masterdata_main_companyname_input").prop('disabled', enabled);
	$("#widget_masterdata_main_nameaffix_input").prop('disabled', enabled);
	$("#widget_masterdata_main_adress_input").prop('disabled', enabled);
	$("#widget_masterdata_main_zipcode_input").prop('disabled', enabled);
	$("#widget_masterdata_main_city_input").prop('disabled', enabled);
	$("#widget_masterdata_main_country_input").prop('disabled', enabled);
	$("#widget_masterdata_main_maincontact_maincontact_select").prop('disabled', enabled);
	$("#widget_masterdata_main_telephone_input").prop('disabled', enabled);
	$("#widget_masterdata_main_fax_input").prop('disabled', enabled);
	$("#widget_masterdata_main_mobilephone_input").prop('disabled', enabled);
	$("#widget_masterdata_main_email_input").prop('disabled', enabled);
	$("#widget_masterdata_main_website_input").prop('disabled', enabled);
	$("#widget_masterdata_content_row_comment").prop('disabled', enabled);
	$("#widget_masterdata_main_change_companylogo").prop('disabled', enabled);		
}

/*
 * clearValues:
 *  - clear all inputs
 */
var widget_masterdata_main_clearValues = function()
{
	$("#widget_masterdata_main_clientnumber_input").val("");
	$("#widget_masterdata_main_companyname_input").val("");
	$("#widget_masterdata_main_nameaffix_input").val("");
	$("#widget_masterdata_main_adress_input").val("");
	$("#widget_masterdata_main_zipcode_input").val("");
	$("#widget_masterdata_main_city_input").val("");
	$("#widget_masterdata_main_country_input").val("");
	$("#widget_masterdata_main_telephone_input").val("");
	$("#widget_masterdata_main_fax_input").val("");
	$("#widget_masterdata_main_mobilephone_input").val("");
	$("#widget_masterdata_main_email_input").val("");
	$("#widget_masterdata_main_website_input").val("");
	$("#widget_masterdata_content_row_comment").text("");	
	$("#widget_masterdata_main_category_input").val("");
	 $("#widget_masterdata_main_maincontact_maincontact_select > option:selected").text("");
	$("#widget_masterdata_main_maincontact_maincontact_select > option:selected").attr("data-identifier" , "undefined") ;
}

/*
 * fillInValues:
 *  - fills the input fields with values
 *	- data {object} of the following format:
 *	{	
 *		clientnumber:{String},
 *		companyname:{String},
 *		nameaffix:{String},
 *		adress:{String},
 *		zipcode:{String},
 *		city:{String},
 *		country:{String},
 *		telephone:{String},
 *		fax:{String},
 *		mobilephone:{String},
 *		email:{String},
 *		website:{String},
 *		comment:{String},
 *		category:{String},
 *		clienttyp:{String},
 *		maincontact_name:{String},
 *		maincontact_identifier:{int},
 *		logopath:{String}
 *	}
 */
var widget_masterdata_main_fillInValues = function(data)
{
	$("#widget_masterdata_main_clientnumber_input").val(data.clientnumber);
	$("#widget_masterdata_main_companyname_input").val(data.companyname);
	$("#widget_masterdata_main_nameaffix_input").val(data.nameaffix);
	$("#widget_masterdata_main_adress_input").val(data.adress);
	$("#widget_masterdata_main_zipcode_input").val(data.zipcode);
	$("#widget_masterdata_main_city_input").val(data.city);
	$("#widget_masterdata_main_country_input").val(data.country);
	$("#widget_masterdata_main_telephone_input").val(data.telephone);
	$("#widget_masterdata_main_fax_input").val(data.fax);
	$("#widget_masterdata_main_mobilephone_input").val(data.mobilephone);
	$("#widget_masterdata_main_email_input").val(data.email);
	$("#widget_masterdata_main_website_input").val(data.website);
	$("#widget_masterdata_content_row_comment").text(data.comment);	
	$("#widget_masterdata_main_category_input").val(data.category);
	$("#widget_masterdata_main_clienttyp_"+data.clienttyp).attr('selected', 'selected');
	if(data.logopath == "")
	{
		// show a dummy logo if there is no specific defined
		$("#widget_masterdata_main_companylogo").val("src", "images/dummy_logo_300_300.png");
	}
	else
	{
		$("#widget_masterdata_main_companylogo").val("src", data.logopath)	
	}	
	
	//TODO maincontact
	$("#widget_masterdata_main_maincontact_maincontact_select > option:selected").text(data.maincontact_name);
	$("#widget_masterdata_main_maincontact_maincontact_select > option:selected").attr("data-identifier" , data.maincontact_identifier) ;
	//data.maincontact_name = $("#widget_masterdata_main_maincontact_maincontact_select > option:selected").attr("value");	
	//data.maincontact_identifier = $("#widget_masterdata_main_maincontact_maincontact_select > option:selected").attr("data-identifier");
	
	//$("#widget_masterdata_main_maincontact_maincontact_select").val(data);
	
}

/*
 * getValues:
 *  - reads the values of all inputs
 *	- returns {object} of the following format:
 *	{
 *		clientnumber:{String},
 *		companyname:{String},
 *		nameaffix:{String},
 *		adress:{String},
 *		zipcode:{String},
 *		city:{String},
 *		country:{String},
 *		telephone:{String},
 *		fax:{String},
 *		mobilephone:{String},
 *		email:{String},
 *		website:{String},
 *		comment:{String},
 *		category:{String},
 *		clienttyp:{String},
 *		maincontact_name:{String},
 *		maincontact_identifier:{int},
 *		logopath:{String}
 *	}
 */
var masterdata_main_getValues = function()
{
	var data = new Object;
	
	data.clientnumber = $("#widget_masterdata_main_clientnumber_input").val();
	data.companyname = $("#widget_masterdata_main_companyname_input").val();
	data.nameaffix = $("#widget_masterdata_main_nameaffix_input").val();
	data.adress = $("#widget_masterdata_main_adress_input").val();
	data.zipcode = $("#widget_masterdata_main_zipcode_input").val();
	data.city = $("#widget_masterdata_main_city_input").val();
	data.country = $("#widget_masterdata_main_country_input").val();
	data.telephone = $("#widget_masterdata_main_telephone_input").val();
	data.fax = $("#widget_masterdata_main_fax_input").val();
	data.mobilephone = $("#widget_masterdata_main_mobilephone_input").val();
	data.email = $("#widget_masterdata_main_email_input").val();
	data.website = $("#widget_masterdata_main_website_input").val();
	data.comment = $("#widget_masterdata_content_row_comment").text();	
	data.category = $("#widget_masterdata_main_category_input").val();
	data.clienttyp = $("#widget_masterdata_main_clienttyp_select > option:selected").attr("value");
	data.logopath = $("#widget_masterdata_main_companylogo").attr("src");
	data.maincontact_name = $("#widget_masterdata_main_maincontact_maincontact_select > option:selected").text();	
	data.maincontact_identifier = $("#widget_masterdata_main_maincontact_maincontact_select > option:selected").attr("data-identifier");

	return data;	
}

/*
 * init:
 *  - the init function which should be called by the loader
 */
var widget_masterdata_main_init = function()
{
	console.log("widget_masterdata_main_init");
	widget_masterdata_main_clearValues();
	widget_masterdata_main_editmode(false);
	$("#widget_masterdata_main_change_companylogo").click(function()
	{
		console.log("Logo ändern")
		//TODO change Logo
	});
}