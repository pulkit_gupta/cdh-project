/*
 * widget to provide inputs for the person editor to masterdata-datasets
 *
 * editmode:
 * 	- method to control the editmode of the loaded widgets
 * 		-> standard is inactive
 *
 * clearValues:
 *	- method to delete all input
 * 
 * fillInValues:
 *	- method to fill in values of a provided data object
 *
 * getValues:
 *	- read all the values from the inputs and return them as a data object
 *
 * init:
 *  - the init function which should be called by the loader
 */
 
/*
 * a content object with demo-data
 */
var demoDaten_personeditor = 
{	
	identifier:1,
	salutation:"Herr",
	salutationkey:"mr",
	title:"no",
	surname:"Max",
	lastname:"Mustermann",
	position:"Chef",
	telephone:"0371/214567",
	fax:"",
	mobilphone:"",
	email:"info@mustermann.de",
	birthday:"01.01.1900",
	bithdaymailing:true,
	notes:"Hier stehen Notzien zu:\n - Max Mustermann"
}

var widget_masterdata_personeditor_current_person_identifier = null;

/*
 * editmode:
 *  - method which enables or disables input/selection fields
 */
var widget_masterdata_personeditor_editmode = function(enabled)
{
	enabled = !enabled;
	$("#widget_masterdata_personeditor_salutaion_select").prop('disabled', enabled);
	$("#widget_masterdata_personeditor_titel_select").prop('disabled', enabled);
	$("#widget_masterdata_personeditor_surname_input").prop('disabled', enabled);
	$("#widget_masterdata_personeditor_lastname_input").prop('disabled', enabled);
	$("#widget_masterdata_personeditor_position_input").prop('disabled', enabled);
	$("#widget_masterdata_personeditor_telephone_input").prop('disabled', enabled);
	$("#widget_masterdata_personeditor_fax_input").prop('disabled', enabled);
	$("#widget_masterdata_personeditor_mobilphone_input").prop('disabled', enabled);
	$("#widget_masterdata_personeditor_email_input").prop('disabled', enabled);
	$("#widget_masterdata_personeditor_birthday_input").prop('disabled', enabled);
	$("#widget_masterdata_personeditor_birthdayemail_select").prop('disabled', enabled);
	$("#widget_masterdata_personeditor_notes_textarea").prop('disabled', enabled);
}

/*
 * clearValues:
 *  - clear all inputs
 */
var widget_masterdata_personeditor_clearValues = function()
{
	widget_masterdata_personeditor_current_person_identifier = null;
	$("#widget_masterdata_personeditor_surname_input").val("");
	$("#widget_masterdata_personeditor_lastname_input").val("");
	$("#widget_masterdata_personeditor_position_input").val("");
	$("#widget_masterdata_personeditor_telephone_input").val("");
	$("#widget_masterdata_personeditor_fax_input").val("");
	$("#widget_masterdata_personeditor_mobilphone_input").val("");
	$("#widget_masterdata_personeditor_email_input").val("");
	$("#widget_masterdata_personeditor_birthday_input").val("");
	$("#widget_masterdata_personeditor_birthdayemail_no").attr('selected', 'selected');
	$("#widget_masterdata_personeditor_salutaion_no").attr('selected', 'selected');
	$("#widget_masterdata_personeditor_titel_no").attr('selected', 'selected');
	$("#widget_masterdata_personeditor_notes_textarea").text("");	
}

/*
 * fillInValues:
 *  - fills the input fields with values
 *	- data {object} of the following format:
 *	{	
 *		identifier:{int}},
 *		salutation:{String},
 *		salutationkey:{String},
 *		title:{String},
 *		surname:{String},
 *		lastname:{String},
 *		position:{String},
 *		telephone:{String},
 *		fax:{String},
 *		mobilphone:{String},
 *		email:{String},
 *		birthday:{String},
 *		bithdaymailing:{bool},
 *		notes:{String}
 *	}
 */
var widget_masterdata_personeditor_fillInValues = function(data)
{
//console.log("Inhalt von data: " + data); // data is null - should be != null
	widget_masterdata_personeditor_current_person_identifier = data.identifier;
	$("#widget_masterdata_personeditor_surname_input").val(data.surname);
	$("#widget_masterdata_personeditor_lastname_input").val(data.lastname);
	$("#widget_masterdata_personeditor_position_input").val(data.position);
	$("#widget_masterdata_personeditor_telephone_input").val(data.telephone);
	$("#widget_masterdata_personeditor_fax_input").val(data.fax);
	$("#widget_masterdata_personeditor_mobilphone_input").val(data.mobilphone);
	$("#widget_masterdata_personeditor_email_input").val(data.email);
	$("#widget_masterdata_personeditor_birthday_input").val(data.birthday);
	
	if(data.bithdaymailing)
	{
		$("#widget_masterdata_personeditor_birthdayemail_yes").attr('selected', 'selected');
	}
	else
	{
		$("#widget_masterdata_personeditor_birthdayemail_no").attr('selected', 'selected');
	}
	//3 salutations defined: mr, mrs, family
	$("#widget_masterdata_personeditor_salutaion_"+data.salutationkey).attr('selected', 'selected');
	//4 title defined: no, dr, dipl, prof
	$("#widget_masterdata_personeditor_titel_"+data.title).attr('selected', 'selected');

	$("#widget_masterdata_personeditor_notes_textarea").text(data.notes);	
}

/*
 * getValues:
 *  - reads the values of all inputs
 *	- returns {object} of the following format:
 *	{
 *		identifier:{int}},
 *		salutation:{String},
 *		salutationkey:{String},
 *		title:{String},
 *		surname:{String},
 *		lastname:{String},
 *		position:{String},
 *		telephone:{String},
 *		fax:{String},
 *		mobilphone:{String},
 *		email:{String},
 *		birthday:{String},
 *		bithdaymailing:{bool},
 *		notes:{String}
 *	}
 */
var widget_masterdata_personeditor_getValues = function()
{
	var tmp = new Object;

	tmp.identifier = widget_masterdata_personeditor_current_person_identifier;
	tmp.surname = $("#widget_masterdata_personeditor_surname_input").val();
	tmp.lastname = $("#widget_masterdata_personeditor_lastname_input").val();
	tmp.position = $("#widget_masterdata_personeditor_position_input").val();
	tmp.telephone = $("#widget_masterdata_personeditor_telephone_input").val();
	tmp.fax = $("#widget_masterdata_personeditor_fax_input").val();
	tmp.mobilphone = $("#widget_masterdata_personeditor_mobilphone_input").val();
	tmp.email = $("#widget_masterdata_personeditor_email_input").val();
	tmp.birthday = $("#widget_masterdata_personeditor_birthday_input").val();
	if($("#widget_masterdata_personeditor_birthdayemail_select > option:selected").attr("value") == "yes")
	{
		tmp.bithdaymailing = true;
	}
	else
	{
		tmp.bithdaymailing = false;
	}
	
	tmp.salutationkey = $("#widget_masterdata_personeditor_salutaion_select > option:selected").attr("value");
	tmp.salutation = $("#widget_masterdata_personeditor_salutaion_select > option:selected").text();
	tmp.title = $("#widget_masterdata_personeditor_titel_select > option:selected").attr("value");
	tmp.notes = $("#widget_masterdata_personeditor_notes_textarea").text();

	return tmp;	
}

/*
 * init:
 *  - the init function which should be called by the loader
 */
var widget_masterdata_personeditor_init = function()
{	
	console.log("widget_masterdata_personeditor_init");
	widget_masterdata_personeditor_clearValues();
	widget_masterdata_personeditor_editmode(false);
}