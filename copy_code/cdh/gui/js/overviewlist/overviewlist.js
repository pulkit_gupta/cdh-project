/*
 * a class which implements a widget to show miscellaneous overviewlists
 *
 * getselectedEntryIdentifier:
 *  - return the selected entry
 *
 * getSelectedData:
 *  - return the data of the selected entry
 *
 * changeSize:
 *  - change the size of the current overviewlist
 *
 * search:
 *  - search within the data for a certain date
 * 
 * filterList:
 *	- method which provides a filter functionality for the overviewlists
 *
 * sortable:
 *	- method to sort the current overviewlist ascending or descending
 *
 * changeListElementSelectFunction:
 *	- callback method which is called when a list element is selected
 *
 * changeListElementUnselectFunction:
 *	- callback method which is called when a list element is unselected
 *
 * listElementClick:
 *	- method to be processed if a list element is clicked
 *
 * insertHeadLine:
 *	- method to create the headline row
 *
 * clearSelected:
 *	- method to clear the selected entry
 *
 * changeEntry:
 *	- change all data with the value of the parameter entry_identifier
 *
 * addDataToList:
 *	- method to insert new data into the overviewlist
 *
 * insertData:
 *	- method to insert new line into the overviewlist
 * 
 * insertDataArray:
 *	- method to insert a data array into the overviewlist
 *
 * resizable:
 *	- method to enable coloumn resizability using jQuery UI
 *
 * autoresize:
 *	- resize the overviewlist considering the size of the content
 *
 * onClick:
 *  -  binds onClick event handler to entry-elements of the overviewlist
 *
 * init:
 *  - the init function which should be called by the loader
 */
 
 /*
  * class overviewlist:
	*	 - creates variables with the provided arguments and makes some settings
	*  - arguments {JSON} of the following format
  */
function overviewlist(arguments)
{
	this.listContainer = $(arguments.target);
	this.listResizable = arguments.resizable;
	this.listSelectFirst = arguments.selectFirst;
	this.listAutoresize = arguments.autoresize;
	this.listSortable = arguments.sortable;
	this.listDebug = arguments.debug;	//not used
	
	this.headLineKeys = new Array();
	this.selectedEntryIdentifier = null;
	
	this.listElementSelectFunction = arguments.listElemSelectFunction;
	this.listElementSelectFunctionArgs = arguments.listElemSelectFunctionArgs;

	this.listElementUnselectFunction = arguments.listElemUnselectFunction;
	this.listElementUnselectFunctionArgs = arguments.listElemUnselectFunctionArgs;	
		
	this.listData = null;
	this.listCurrentData = null;
	this.filterActive = false;
	this.filter = null;
	this.height = arguments.height;
	this.width = arguments.width;
}

/*
 * getselectedEntryIdentifier:
 *  - return the selected entry
 */
overviewlist.prototype.getselectedEntryIdentifier = function()
{
	return this.selectedEntryIdentifier;
}

/*
 * getSelectedData:
 *  - return the data of the selected entry
 */
overviewlist.prototype.getSelectedData = function()
{	
	if(this.listData != null)
	{
		for (var i=0; i < this.listData.length; i++) 
		{
		  if (this.listData[i]._key == this.selectedEntryIdentifier)
		  {	  	
			return this.listData[i];
		  }
		};		
	}
	return null;	
}

/*
 * changeSize:
 *  - change the size of the current overviewlist
 *  - height {int} the new height
 *  - width {int} the new witdth
 */
overviewlist.prototype.changeSize = function(height, width)
{
	if (height != undefined && width != undefined )
	{
		var headlineHeight = $(this.listContainer).children("ul.overviewlist_list_headline").outerHeight(true);
	  $(this.listContainer).children("ul.overviewlist_list_content").height(height-headlineHeight);
		
		var headlinepadding = $(this.listContainer).children("ul.overviewlist_list_headline").innerWidth() - $(this.listContainer).children("ul.overviewlist_list_headline").width();
		var contentpadding = $(this.listContainer).children("ul.overviewlist_list_content").innerWidth() - $(this.listContainer).children("ul.overviewlist_list_content").width();
		$(this.listContainer).children("ul.overviewlist_list_content").width(width-contentpadding);
		$(this.listContainer).children("ul.overviewlist_list_headline").width(width-headlinepadding);
		this.width = width;
		this.height = height;
	}
}

/*
 * search:
 *  - search within the data for a certain date
 *  - searchvalue {String} the value to search
 *  - searchkey {String} the key (e.g. id/name...) where to search the value
 */
overviewlist.prototype.search = function(searchvalue, searchkey)
{
	var count = 0;
	if(this.filter != null)
	{
		this.filterList(this.filter.value, this.filter.key);
	}
	else
	{
		$(this.listContainer).children("ul.overviewlist_list_content").children("li").removeClass('hidden highlight');
	}
	$(this.listContainer).children("ul.overviewlist_list_content").children("li:not(.hidden)").each(function()
	{
		if($(this).children("ul").children("li.overviewlist_list_entry_"+searchkey).text().toLowerCase().indexOf(searchvalue.toLowerCase()) != -1)
		{
			$(this).removeClass('hidden');
			if(count%2 != 0)
			{
				$(this).addClass('highlight');
			}
			else
			{
				$(this).removeClass('highlight');
			}
			count++;
		}
		else
		{
			//$(this).hide();
			$(this).addClass('hidden');
		}
	});
}

//TODO: documentation
/*
 * filterList:
 *	- deprecated
 *	- what shall happen within this method
 *	- method which provides a filter functionality for the overviewlists
 *	- filter {String}
 *	- filterkey {String} the key (e.g. id/name...)
 */
overviewlist.prototype.filterList = function(filter, filterkey)
{		
	var thiz = this;
	this.selectedEntryIdentifier = null;
	this.filter = {"value":filter, "key":filterkey};
	
	if (filter == "*")
	{
		$(this.listContainer).children("ul.overviewlist_list_content").children("li").removeClass('hidden highlight');
		$(this.listContainer).children("ul.overviewlist_list_content").children("li:odd").addClass("highlight");
		this.filterValue = null;
	}
	else if(filter == "123")
	{
		var count = 0;
		$(this.listContainer).children("ul.overviewlist_list_content").children("li").each(function()
		{			
			if(!isNaN($(this).children("ul").children("li.overviewlist_list_entry_"+filterkey).text().charAt(0)))
			{
				if(count%2 != 0)
				{
					$(this).addClass('highlight');
				}
				else
				{
					$(this).removeClass('highlight');
				}
				count++;
				//$(this).show();
				$(this).removeClass('hidden');
			}
			else
			{
				//$(this).hide();
				$(this).addClass('hidden');
			}
		});
	}
	else
	{		
		var count = 0;
		$(this.listContainer).children("ul.overviewlist_list_content").children("li").each(function()
		{
			if($(this).children("ul").children("li.overviewlist_list_entry_"+filterkey).text().charAt(0).toLowerCase()  == filter.toLowerCase())
			{
				//$(this).show();
				if(count%2 != 0)
				{
					$(this).addClass('highlight');
				}
				else
				{
					$(this).removeClass('highlight');
				}
				count++;
				$(this).removeClass('hidden');
			}
			else
			{
				//$(this).hide();
				$(this).addClass('hidden');
			}
		});
	}
	
	console.log(this.listSelectFirst)
	if(this.listSelectFirst)
	{
		$(this.listContainer).children("ul.overviewlist_list_content").children("li").each(function()
		{
			console.log($(this).css("display"))
			if($(this).css("display") != "none")
			{
				$(this).parent().children("li").children("ul").removeClass("overviewlist_list_entry_active");
				$(this).children("ul").addClass('overviewlist_list_entry_active');	
				thiz.selectedEntryIdentifier = $(this).children("ul").attr("data-identifier");
    			//break;
			}
		});	
	}
}

/*
 * sortable:
 *	- method to sort the current overviewlist ascending or descending
 */
overviewlist.prototype.sortable = function()
{
	var thiz = this;
	$(this.listContainer).children("ul.overviewlist_list_headline").children("li").children("ul.overviewlist_list_entry").children("li:not(.overviewlist_list_widening)").click(function() 
	{
		var tmpClassName = $(this).attr("class").split(' ')[0]; 
		var key = tmpClassName.split("overviewlist_list_entry_")[1];
		var sortTyp = $(this).attr("class").split(' ')[1];
		
		// set the order
		if(sortTyp == "asc")
		{
			$(this).removeClass('asc');
			$(this).addClass('desc');
		}
		else
		{
			$(this).removeClass('desc');
			$(this).addClass('asc');
		}
		
		// do the sorting itself
		thiz.listCurrentData.sort(function(a,b)
		{
			if(sortTyp == "asc")
			{
				return a[key] < b[key];
			}
			else
			{
				return a[key] > b[key];
			}
		});
		thiz.insertDataArray(thiz.listCurrentData, {callback:null});
	});
}

/*
 * changeListElementSelectFunction:
 *	- callback method which is called when a list element is selected
 *	- onclickfunction {method} the method to be processed
 *	- initargs {JSON} object with the initilization parameters
 */
overviewlist.prototype.changeListElementSelectFunction = function(onclickfunction, initargs)
{
	if (onclickfunction)
	{
		this.listElementSelectFunction = onclickfunction;
		this.listElementSelectFunctionArgs = initargs;	
	}
}

/*
 * changeListElementUnselectFunction:
 *	- callback method which is called when a list element is unselected
 *	- onclickfunction {method} the method to be processed
 *	- initargs {JSON} object with the initilization parameters
 */
overviewlist.prototype.changeListElementUnselectFunction = function(onclickfunction, initargs)
{
	if (onclickfunction)
	{
		this.listElementUnselectFunction = onclickfunction;
		this.listElementUnselectFunctionArgs = initargs;	
	}
}

/*
 * listElementClick:
 *	- method to be processed if a list element is clicked
 */
overviewlist.prototype.listElementClick = function()
{
	var thiz = this;

	//TODO ändern in abfrage ob clickevent vorhanden
	$(this.listContainer).children("ul.overviewlist_list_content").children("li").children("ul.overviewlist_list_entry").unbind('click');
	$(this.listContainer).children("ul.overviewlist_list_content").children("li").children("ul.overviewlist_list_entry").click(function() 
	{
		// clicked element is the active one
		if($(this).hasClass('overviewlist_list_entry_active'))
		{
			$(this).removeClass('overviewlist_list_entry_active');	
			thiz.selectedEntryIdentifier = $(this).attr("data-identifier");
			// check if a method is defined for unclicking an element
			if (thiz.listElementUnselectFunction)
			{	
				if (thiz.listElementUnselectFunctionArgs instanceof Array)
				{
					// if init arguments are provided
					var initargs = thiz.listElementUnselectFunctionArgs;
					initargs.unshift(thiz.selectedEntryIdentifier, this);
					// call the defined function for unselected elements
					thiz.listElementUnselectFunction.apply(null, initargs);
				}
				else
				{
					// call the defined function for unselected elements
					thiz.listElementUnselectFunction(thiz.selectedEntryIdentifier, this, thiz.listElementUnselectFunctionArgs);
				}
			}
			thiz.selectedEntryIdentifier = null;
		}
		// clicked element was not the active one
		else
		{
			$(this).parent().parent().children("li").children("ul").removeClass("overviewlist_list_entry_active");
			$(this).addClass('overviewlist_list_entry_active');	
			thiz.selectedEntryIdentifier = $(this).attr("data-identifier");

			if (thiz.listElementSelectFunction)
			{
				// check if a method is defined for unclicking an element
				if (thiz.listElementSelectFunctionArgs instanceof Array)
				{
					// if init arguments are provided
					var initargs = thiz.listElementSelectFunctionArgs;
					initargs.unshift(thiz.selectedEntryIdentifier, this);
					// call the defined function for unselected elements
					thiz.listElementSelectFunction.apply(null, initargs);
				}
				else
				{
					// call the defined function for unselected elements
					thiz.listElementSelectFunction(thiz.selectedEntryIdentifier, this, thiz.listElementSelectFunctionArgs);
				}
			}    		
		}		
	});
}

/*
 * insertHeadLine:
 *	- method to create the headline row
 *	- headlineData {JSON[]} the data of the headline(s)
 *	- args {JSON} object with the initilization parameters
 */
overviewlist.prototype.insertHeadLine = function(headlineData, args)
{
	var callback = args.callback;
	var initargs = args.initargs;

	//create HeadlineRow of Table and append it to the children ul.overviewlist_list of the target
	var txt_headlineRow = '<li><ul class="overviewlist_list_entry">';	
	for (var i=0; i < headlineData.length; i++) 
	{		
		txt_headlineRow += '<li class="overviewlist_list_entry_'+ headlineData[i].key +' overviewlist_list_enlargable" title="'+headlineData[i].text+'">'+ headlineData[i].text  +'</li>';
		if(headlineData.length-1 > i)
		{
			txt_headlineRow += '<li class="overviewlist_list_widening"/>';	
		}
		this.headLineKeys.push(headlineData[i].key);
	};
	txt_headlineRow += '</ul></li>';
	$(this.listContainer).children("ul.overviewlist_list_headline").append(txt_headlineRow);

	if(this.listResizable)
	{
		this.resizable();
	}
	if(this.listAutoresize)
	{
		this.autoresize();
	}
	if(this.listSortable)
	{
		this.sortable();
	}
	
	this.changeSize(this.height, this.width);
	
	if (callback)
	{	
		if (initargs instanceof Array)
		{
			callback.apply(null, initargs);
		}
		else
		{
			callback(initargs);
		}
	}
}

/*
 * clearSelected:
 *	- method to clear the selected entry
 */
overviewlist.prototype.clearSelected = function()
{
	$(this.listContainer).find("ul.overviewlist_list_entry_active").removeClass('overviewlist_list_entry_active');
}

/*
 * changeEntry:
 *	- change all data with the value of the parameter entry_identifier
 *	- newdata {String|int|bool} the data to change
 *	- entry_identifier {int} the identifier of the datasets to change
 */
overviewlist.prototype.changeEntry = function(newdata, entry_identifier)
{
	var thiz = this;
	for (var i=0; i < this.listData.length; i++) 
	{
	  if (this.listData[i].identifier == newdata.identifier)
	  {	  	
			this.listData[i] = newdata;
	  }
	};
	$(this.listContainer).find("ul.overviewlist_list_entry").each(function(index, value)
	{
		if($(value).attr("data-identifier") == entry_identifier)
		{
			for (var j=0; j < thiz.headLineKeys.length; j++) 
			{
				$(value).children("li.overviewlist_list_entry_"+thiz.headLineKeys[j]).text(newdata[thiz.headLineKeys[j]]);
			};
		}		
	});	
}

/*
 * addDataToList:
 *	- method to insert new data into the overviewlist
 *	- data {JSON} object with the data to add
 *	- singleData {String|int|bool} optional parameter with only one date to add
 *	- select {bool} if the data shall be inserted into the selected entry
 */
overviewlist.prototype.addDataToList = function(data, singleData, select)
{		
	//create ContentRow of Table and append it to the children ul.overviewlist_list of the target
	var txt_newRow;
	if(select)
	{
		txt_newRow = '<li><ul class="overviewlist_list_entry overviewlist_list_entry_active" data-identifier="'+data._key+'">';
		this.selectedEntryIdentifier = data.identifier;
	}
	else
	{
	 	txt_newRow = '<li><ul class="overviewlist_list_entry" data-identifier="'+data._key+'">';
	}
	for (var j=0; j < this.headLineKeys.length; j++) 
	{
		//if(this.headLineKeys[j].contains(".")) // causes error in chrome - works only in newer browsers
		if(this.headLineKeys[j].indexOf(".") > -1)
		{
			if(data[this.headLineKeys[j].split(".")[0]] != undefined)
			{
				txt_newRow += '<li class="overviewlist_list_entry_'+ this.headLineKeys[j] +' overviewlist_list_entry_enlarge">'+ data[this.headLineKeys[j].split(".")[0]][this.headLineKeys[j].split(".")[1]]  +'</li>';
			}
			else
			{
				txt_newRow += '<li class="overviewlist_list_entry_'+ this.headLineKeys[j] +' overviewlist_list_entry_enlarge"></li>';
			}
			
		}
		else
		{
			if(data[this.headLineKeys[j]] != undefined)
			{
				
				txt_newRow += '<li class="overviewlist_list_entry_'+ this.headLineKeys[j] +' overviewlist_list_entry_enlarge">'+ data[this.headLineKeys[j]]  +'</li>';	
			}
			else
			{
				txt_newRow += '<li class="overviewlist_list_entry_'+ this.headLineKeys[j] +' overviewlist_list_entry_enlarge"></li>';	
			}
			
		}
		//console.log(this.headLineKeys[j].split("."));
		
		if(this.headLineKeys.length-1 > j)
		{
			txt_newRow += '<li class="overviewlist_list_widening"/>';	
		}
	};
	txt_newRow += '</ul></li>';
	$(this.listContainer).children("ul.overviewlist_list_content").append(txt_newRow);
	
	if(singleData)
	{
		this.listElementClick();
		//TODO
		if(this.listAutoresize)
		{
			this.autoresize();
		}	
	}	
}

/*
 * insertData:
 *	- method to insert new line into the overviewlist
 *	- data {JSON} object with the data to add
 *	- singleData {String|int|bool} optional parameter with only one date to add
 *	- select {bool} if the data shall be inserted into the selected entry
 */
overviewlist.prototype.insertData = function(data, singleData, select)
{
	if(this.listData == null)
	{
		this.listData = new Array();
	}
	this.listData.push(data);
	
	this.addDataToList(data, singleData, select);	
}

/*
 * insertDataArray:
 *	- method to insert a data array into the overviewlist
 *	- dataArray {JSON} object with the data to add
 *	- arg {JSON} object with several parameters
 */
overviewlist.prototype.insertDataArray = function(dataArray, args)
{
	var callback = args.callback;
	var initargs = args.initargs;	
	this.listData = dataArray;
	
	//removes content
	 $(this.listContainer).children("ul.overviewlist_list_content").children().remove();
	
	//create ContentRow of Table and append it to the children ul.overviewlist_list of the target
	for (var i=0; i < this.listData.length; i++) 
	{
		if(this.listSelectFirst)
		{
			this.addDataToList(this.listData[i], false, (i==0));
		}
		else
		{
			this.addDataToList(this.listData[i], false, false);
		}
		
	};
	$(this.listContainer).children("ul.overviewlist_list_content").children("li:odd").addClass("highlight");	
	if(this.listAutoresize)
	{
		//TODO
		this.autoresize();
	}
	
	this.listElementClick();
	
	if (callback)
	{	
		if (initargs instanceof Array)
		{
			callback.apply(null, initargs);
		}
		else
		{
			callback(initargs);
		}
	}
}

/*
 * resizable:
 *	- method to enable coloumn resizability using jQuery UI
 */
overviewlist.prototype.resizable = function()
{
	var oldWidth = 0;
	var leftBorder = 0;
	var rightBorder = 0;
	
	 $(this.listContainer).children("ul.overviewlist_list_headline").children("li").children("ul").children("li.overviewlist_list_widening").draggable({
		axis: "x",
		containment: "parent", 
	 	start: function()
		{	 		
			var minWidthOfPrev = 0;
			var widthOfAllPrev = 0;
			var widthOfAllNext= 0;
			var entireWidth = $(this).parents("ul.overviewlist_list_headline").width();
			
			oldWidth = $(this).prev().width();				
			var $pseudo=$("<span id='calculate_textwidth'>"+$(this).prev().text()+"</span>").hide()
			$('body').append($pseudo);
			minWidthOfPrev = $pseudo.width()/2; //width of headline-col at least the half width of the coltext
			$pseudo.remove();
			
			$(this).prevAll().each(function(index)
			{
				widthOfAllPrev += $(this).outerWidth();				  
			})
			$(this).nextAll().each(function(index)
			{
				widthOfAllNext += $(this).outerWidth();				  
			})

			rightBorder = (entireWidth-(widthOfAllPrev+widthOfAllNext+2));
			leftBorder = (minWidthOfPrev-oldWidth);
		},
		drag: function(event, ui)
		{
			if(leftBorder <= ui.position.left && rightBorder >= ui.position.left)
			{
				$(this).parents("ul.overviewlist_list_headline").children().find("li."+$(this).prev().attr("class")).width(oldWidth+ui.position.left);
				$(this).parents("ul.overviewlist_list_headline").next().children().find("li."+$(this).prev().attr("class")).width(oldWidth+ui.position.left);
				ui.position.left = 0;
			}
			else
			{
				ui.position.left = 0;
			}
		},
		stop: function()
		{
		} 
	});	
}

/*
 * autoresize:
 *	- resize the overviewlist considering the size of the content
 */
overviewlist.prototype.autoresize = function()
{	
	var $headline = $(this.listContainer).children("ul.overviewlist_list_headline").children("li").children("ul").children("li:not(.overviewlist_list_widening)");
	var $content = $(this.listContainer).children("ul.overviewlist_list_content").children("li");

	//var XwidthProLetter = $($headline[0]).width();
	var $pseudo=$("<span style='font-size:15px;'>A</span>").hide();
	$('body').append($pseudo);
	var XwidthProLetter = $pseudo.width();
	//$("."+$($headline[i]).attr("class")).width($pseudo.width());
	$pseudo.remove();
	var totalWidth = 0;
	for (var i=0; i < this.headLineKeys.length; i++) 
	{
		var largestTextLength = 0;
		var largestTextIndex = 0;
		var largestTextElem = null;
		$(this.listContainer).find("li.overviewlist_list_entry_"+this.headLineKeys[i]).each(function(index, value) 
		{
			if(largestTextLength < $(value).text().length)
			{
				largestTextLength = $(value).text().length;
				largestTextElem = value;
			}
		});
	  	
  	var $pseudo=$("<span style='font-size:"+$(largestTextElem).css("font-size")+";'>"+$(largestTextElem).text()+"</span>").hide();
		$('body').append($pseudo);
		var newSize = $pseudo.width();
		$pseudo.remove();
		
		//console.log(newSize)
		$(this.listContainer).find("li.overviewlist_list_entry_"+this.headLineKeys[i]).width(newSize);
	};
	/*
	for(var i=0, j=$headline.length; i<j; i++)
	{
		if(true)
		{
			
			$("."+$($headline[i]).attr("class")).width(($($headline[i]).text().length)*XwidthProLetter);
			var $tmpArray = $($content).children("ul").children("li."+$($headline[i]).attr("class"));
			var largestLength = $($headline[i]).text().length;
			$($tmpArray).each(function(index, value)
			{	
				if($(value).text().length > largestLength)
				{
					$("."+$(value).attr("class")).width(($(value).text().length)*XwidthProLetter);
					largestLength = $(value).text().length;
				}
			});
		}
		else
		{
	  		var $pseudo=$("<span>"+$($headline[i]).text()+"</span>").hide()
			$('body').append($pseudo);
			$("."+$($headline[i]).attr("class")).width($pseudo.width());
			$pseudo.remove();
			var $tmpArray = $($content).children("ul").children("li."+$($headline[i]).attr("class"));
			var largestLength = $($headline[i]).text().length;
			$($tmpArray).each(function()
			{
				
				if($(this).text().length > largestLength)
				{
					console.log("länger");
			  		$pseudo=$("<span>"+$(this).text()+"</span>").hide()
					$('body').append($pseudo);
					$("."+$(this).attr("class")).width($pseudo.width());
					$pseudo.remove();
					largestLength = $(this).text().length;
				}
			});
		}
	  
	};*/
}

/*
 * init:
 *  - the init function which should be called by the loader
 */
var widget_overviewlist_init = function()
{
	//empty
}
