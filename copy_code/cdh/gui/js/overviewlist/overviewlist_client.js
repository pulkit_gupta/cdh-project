/*
 * a widget which creates an overview list with the client data
 *
 * init:
 *  - the init function which should be called by the loader
 */

// objects with demo data
var demoData_overviewlist_client = {
	headline:[
		{
			key : "firstname",
			text: "Vorname"
		},
		{
			key : "lastname",
			text: "Nachname"
		},
		{
			key : "clientnumber",
			text: "Kundennummer"
		},
		{
			key : "zipcode",
			text: "PLZ"
		},
		{
			key : "city",
			text: "Ort"
		},
		{
			key : "adress",
			text: "Adresse"
		}								
	],
	content:[
		{	
			identifier:"client_1",
			firstname:"John",
			lastname:"Doe",
			clientnumber:"1",
			zipcode:"09126",
			city:"Chemnitz",
			adress:"Reichenhainer Straße 70"
		},
		{	
			identifier:"client_2",
			firstname:"Jane",
			lastname:"Doe",
			clientnumber:"2",
			zipcode:"09126",
			city:"Chemnitz",
			adress:"Reichenhainer Straße 70"
		},
		{	
			identifier:"client_3",
			firstname:"John",
			lastname:"Doe",
			clientnumber:"3",
			zipcode:"09126",
			city:"Chemnitz",
			adress:"Straße der Nationen"
		},
		{	
			identifier:"client_4",
			firstname:"Jane",
			lastname:"Doe",
			clientnumber:"4",
			zipcode:"09126",
			city:"Chemnitz",
			adress:"Straße der Nationen"
		}					
	]
}; 

var widget_overviewlist_client_data_head = [
	{
		key : "firstname",
		text: "Vorname"
	},
	{
		key : "lastname",
		text: "Nachname"
	},
	{
		key : "clientnumber",
		text: "Kundennummer"
	},
	{
		key : "zipcode",
		text: "PLZ"
	},
	{
		key : "city",
		text: "Ort"
	},
	{
		key : "adress",
		text: "Adresse"
	}								
];

var widget_overviewlist_client_data_content = [
	{	
		identifier:"client_1",
		firstname:"John",
		lastname:"Doe",
		clientnumber:"1",
		zipcode:"09126",
		city:"Chemnitz",
		adress:"Reichenhainer Straße 70"
	},
	{	
		identifier:"client_2",
		firstname:"Jane",
		lastname:"Doe",
		clientnumber:"2",
		zipcode:"09126",
		city:"Chemnitz",
		adress:"Reichenhainer Straße 70"
	},
	{	
		identifier:"client_3",
		firstname:"John",
		lastname:"Doe",
		clientnumber:"3",
		zipcode:"09126",
		city:"Chemnitz",
		adress:"Straße der Nationen"
	},
	{	
		identifier:"client_4",
		firstname:"Jane",
		lastname:"Doe",
		clientnumber:"4",
		zipcode:"09126",
		city:"Chemnitz",
		adress:"Straße der Nationen"
	}					
];

var ovList = null;
// an unsuded debug method
var widget_overviewlist_client_test = function()
{
	//ovList.changeSize(300, 800);	
}

/*
 * init:
 *  - the init function which should be called by the loader
 */
var widget_overviewlist_client_init = function()
{	
	var testfunction = function(args)
	{
		console.log(args);
	}
	
	// create a new overviewlist
	ovList = new overviewlist(
		{
			target:"#widget_overviewlist_client",
			resizable:true,
			selectFirst:true,
			autoresize:false,
			sortable:true,
			listElemSelectFunction:testfunction,
			listElemSelectFunctionArgs:null,
			listElemUnselectFunction:testfunction,
			listElemUnselectFunctionArgs:null,
		}
	);
	
	// fill the new overviewlist with the data
	ovList.insertHeadLine(overviewlist_demodata_client_head, {callback:null});
	ovList.insertDataArray(overviewlist_demodata_client_content, {callback:widget_overviewlist_client_test});
			

	var testfunction2 = function(args, args2)
	{
		console.log("args");
		console.log(args2);
	}
	
	//ovList.changeListElementSelectFunction(testfunction2);

	/*
	//var headlineKeys = widget_overviewlist_insertHead(demoData_overviewlist_client, $("#widget_overviewlist_client") ,{callback: widget_overviewlist_resizable, initargs: $("#widget_overviewlist_client")});
	//widget_overviewlist_insertData(demoData_overviewlist_client, headlineKeys, $("#widget_overviewlist_client"), {callback: null});	
	
	var headlineKeys = widget_overviewlist_insertHead(demoData_overviewlist_client, $("#widget_overviewlist_client") ,{callback: widget_overviewlist_resizable, initargs: $("#widget_overviewlist_client")});
	//widget_overviewlist_insertData(demoData_overviewlist, headlineKeys, $(".overviewlist_wrapper"), {callback: widget_overviewlist_onclick, initargs: [$(".overviewlist_wrapper"), widget_overviewlist_onclickDEMOfunction, null]});
	widget_overviewlist_insertData(demoData_overviewlist_client, headlineKeys, $("#widget_overviewlist_client"), false,  
	{
		callback: widget_overviewlist_autoresize, 
		initargs: [
					$("#widget_overviewlist_client"), 
						{
							callback: widget_overviewlist_onclick, 
							initargs: 	[
											$("#widget_overviewlist_client"), 
											widget_overviewlist_onclickDEMOfunction, 
											null
										]
						}
					]
	});
	*/
}