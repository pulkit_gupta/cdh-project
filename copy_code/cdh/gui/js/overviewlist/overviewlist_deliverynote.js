/*
 * a widget which creates an overview list with the deliverynote data
 *
 * init:
 *  - the init function which should be called by the loader
 */

// object with demo data
var demoData_overviewlist_deliverynote = {
	headline:[
		{
			key : "deliverynotenumber",
			text: "Lieferscheinnr."
		},
		{
			key : "deliverynotedate",
			text: "Datum"
		},
		{
			key : "clientname",
			text: "Name"
		}
	],
	content:[
		{	
			identifier:"deliverynote_1",
			deliverynotenumber:"1",
			deliverynotedate:"25.05.2013",
			clientname:"Doe, John"
		},
		{	
			identifier:"deliverynote_2",
			deliverynotenumber:"2",
			deliverynotedate:"26.05.2013",
			clientname:"Doe, Jane"
		},
		{	
			identifier:"deliverynote_3",
			deliverynotenumber:"3",
			deliverynotedate:"27.05.2013",
			clientname:"Mustermann"
		},					
	]
}; 

/*
 * init:
 *  - the init function which should be called by the loader
 */
var widget_overviewlist_deliverynote_init = function()
{
	/*
	var headlineKeys = widget_overviewlist_insertHead(demoData_overviewlist_deliverynote, $("#widget_overviewlist_deliverynote") ,{callback: widget_overviewlist_resizable, initargs: $("#widget_overviewlist_deliverynote")});
	//widget_overviewlist_insertData(demoData_overviewlist_deliverynote, headlineKeys, $("#widget_overviewlist_deliverynote"), {callback: null});	
	widget_overviewlist_insertData(demoData_overviewlist_deliverynote, headlineKeys, $("#widget_overviewlist_deliverynote"),  false, 
	{
		callback: widget_overviewlist_autoresize, 
		initargs: [
					$("#widget_overviewlist_deliverynote"), 
						{
							callback: widget_overviewlist_onclick, 
							initargs: 	[
											$("#widget_overviewlist_deliverynote"), 
											widget_overviewlist_onclickDEMOfunction, 
											null
										]
						}
					]
	});*/
	
	// create a new overviewlist
	var ovList = new overviewlist(
		{
			target:"#widget_overviewlist_deliverynote",
			resizable:true,
			selectFirst:true,
			autoresize:true,
			sortable:true
		}
	);
	
	// fill the new overviewlist with the data
	ovList.insertHeadLine(demoData_overviewlist_deliverynote["headline"], {callback:null});
	ovList.insertDataArray(demoData_overviewlist_deliverynote["content"], {callback:null});
}