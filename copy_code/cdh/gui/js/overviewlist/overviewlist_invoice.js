/*
 * a widget which creates an overview list with the invoice data
 *
 * init:
 *  - the init function which should be called by the loader
 */

// object with demo data
var demoData_overviewlist_invoice = {
	headline:[
		{
			key : "invoicenumber",
			text: "Rechnungsnr."
		},
		{
			key : "invoicedate",
			text: "Datum"
		},
		{
			key : "clientname",
			text: "Name"
		},
		{
			key : "invoiceamount",
			text: "Betrag"
		}
	],
	content:[
		{	
			identifier:"invoice_0124",
			invoicenumber:"0124",
			invoicedate:"22.05.2013",
			clientname:"Doe, John",
			invoiceamount:"200€"
		},
		{	
			identifier:"invoice_0125",
			invoicenumber:"0125",
			invoicedate:"23.05.2013",
			clientname:"Doe, Jane",
			invoiceamount:"500€"
		},
		{	
			identifier:"invoice_0126",
			invoicenumber:"0126",
			invoicedate:"24.05.2013",
			clientname:"Mustermann",
			invoiceamount:"20€"
		}
	]
};

/*
 * init:
 *  - the init function which should be called by the loader
 */
var widget_overviewlist_invoice_init = function()
{
	/*
	var headlineKeys = widget_overviewlist_insertHead(demoData_overviewlist_invoice, $("#widget_overviewlist_invoice") ,{callback: widget_overviewlist_resizable, initargs: $("#widget_overviewlist_invoice")});
	//widget_overviewlist_insertData(demoData_overviewlist_invoice, headlineKeys, $("#widget_overviewlist_invoice"), {callback: null});
	widget_overviewlist_insertData(demoData_overviewlist_invoice, headlineKeys, $("#widget_overviewlist_invoice"),  false, 
	{
		callback: widget_overviewlist_autoresize, 
		initargs: [
					$("#widget_overviewlist_invoice"), 
						{
							callback: widget_overviewlist_onclick, 
							initargs: 	[
											$("#widget_overviewlist_invoice"), 
											widget_overviewlist_onclickDEMOfunction, 
											null
										]
						}
					]
	});	*/
	
	// create a new overviewlist
	var ovList = new overviewlist(
		{
			target:"#widget_overviewlist_invoice",
			resizable:true,
			selectFirst:true,
			autoresize:true,
			sortable:true
		}
	);
	
	// fill the new overviewlist with the data
	ovList.insertHeadLine(demoData_overviewlist_invoice["headline"], {callback:null});
	ovList.insertDataArray(demoData_overviewlist_invoice["content"], {callback:null});
}