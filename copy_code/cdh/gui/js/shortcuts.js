/*
 * the masterdata widget, which provides the possibility to add a new masterdata-set
 * 
 */

/*
 * init:
 *  - the init function which should be called by the loader
 *  
 */
var widget_shortcuts_init = function()
{
    // Set the dummy-image as company logo

	//$("#masterdata_logoarea_logoimg").attr("src", "../images/dummy_avatar.png");
	widget_shortcuts_loadElements();

			$(".shortCut_Element").draggable(
				{
				    revert : "invalid", // when not dropped, the item will revert back to its initial position
				    start : function(event, ui)
				    {
					    jQuery(this).addClass('noclick');
				    },
				    drag : function(event, ui)
				    {
					    jQuery(this).parent().css("border-style", "dotted");
					    jQuery(this).css("z-index", "50");
				    },
				    stop : function(event, ui)
				    {
					    jQuery(this).parent().css("border-style", "none");
					    jQuery(this).css("z-index", "1");
					    jQuery(this).removeClass('noclick');

				    }
				});

		$(".shortCutElement_container").droppable(
			        {

			            accept : ".shortCut_Element",
			            tolerance : 'pointer',
			            //hoverClass: "ui-state-active",

			            over : function(event, ui)
			            {
				            if (jQuery(this).children(".shortCut_Element").length == 0)
				            {
					            //jQuery(this).css("border-width", "thin");
					            jQuery(this).css("border-style", "dotted");
				            }
			            },
			            out : function(event, ui)
			            {
				            if (jQuery(this).children(".shortCut_Element").length == 0)
				            {
					            //jQuery(this).css("border-width", "thin");
					           	jQuery(this).css("border-style", "none");
				            }

			            },
			            drop : function(event, ui)
			            {

					            if (jQuery(this).children(".shortCut_Element").length == 0)
					            {
						            jQuery(ui.draggable).parent().css("border-style", "none");
						            jQuery(ui.draggable).css("left", "0px");
						            jQuery(ui.draggable).css("top", "0px");
						            jQuery(ui.draggable).css("z-index", "1");
						            ui.draggable.appendTo(this);
						            jQuery(this).css("border-style", "none");
					            }
					            else
					            {
						            jQuery(ui.draggable).parent().css("border-style", "none");
						            jQuery(this).children().appendTo(jQuery(ui.draggable).parent());
						            jQuery(ui.draggable).css("left", "0px");
						            jQuery(ui.draggable).css("top", "0px");
						            jQuery(ui.draggable).css("z-index", "1");
						            jQuery(ui.draggable).appendTo(this);
					            }


			         	}

		           });



	
	
	//console.log($("#shortCutElement_container"));
};

var widget_shortcuts_getvalues = function()
{
    //$("#nav_tooltip").fadeOut(300);
};




var widget_shortcuts_loadElements = function()
{
	//initialize first line, center Elements
	var shortCutElement_container_width = $(".shortCutElement_container").outerWidth(true);
	var shortcut_line_width = $(".shortcut_line").innerWidth();
	var maxContainer = Math.round($(".shortcut_line").innerWidth()/$(".shortCutElement_container").outerWidth(true));
	var shortCutElement_container_new_margin = parseInt(((shortcut_line_width-(maxContainer*$(".shortCutElement_container").outerWidth()))/maxContainer)/2);
	var first_left_margin = (shortcut_line_width-(shortCutElement_container_new_margin*maxContainer*2)-($(".shortCutElement_container").outerWidth()*maxContainer))/2;
	for (var i=$(".shortcut_line").children().length; i < maxContainer; i++) 
	{
	  	$(".shortCutElement_container").first().clone().appendTo('#shortcut_area_line_1');
	};
	$(".shortCutElement_container").css("margin-left", shortCutElement_container_new_margin+"px");
	$(".shortCutElement_container").css("margin-right", shortCutElement_container_new_margin+"px");
	$(".shortCutElement_container").first().css("margin-left", shortCutElement_container_new_margin+first_left_margin+"px");

	
	var toShow = $("#shortCut_Element_newNote").clone();
	$(toShow).css("display", "block");
	$(".shortCutElement_container").eq(0).append(toShow);
	
	toShow = $("#shortCut_Element_newTask").clone();
	$(toShow).css("display", "block");
	$(".shortCutElement_container").eq(2).append(toShow);
}
