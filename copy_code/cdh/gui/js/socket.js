var websocket_global = {};
var websocket_watcher = null;
var websocket_watcher_interval = 1000;
var websocket_watcher_counter = 1;
var websocket_request_cache = [];

var websocket_stack = [];
var websocket_worker = null;
var websocket_worker_interval;
var websocket_worker_retrycount = 1;

// session
var session_id_get = function(){
    if(localStorage.sessionCookie && (m = /SID=([^ ,;]*)/.exec(localStorage.sessionCookie)))
    {
        if (m[1].length<1 || m[1]=="0")
        {
            while (typeof(loginform)!="function");
            loginform();
        }
    }
    else
    {
        while (typeof(loginform)!="function");
        loginform();
    }
    return localStorage.sessionCookie;
};

var session_id_set = function(cookie){
    if (localStorage.sessionCookie!=cookie)
    {
        localStorage.sessionCookie = cookie;
        if (m = /SID=([^ ,;]*)/.exec(cookie) && m[1]!="0")
        {
            location.reload();
        }
    }
};

var session_id_clear = function(){
    localStorage.sessionCookie = "";
    
};

var session_start = function(){
    websocket_send({
        action : "getsession",
        cookie : session_id_get()
    });
};

// handler
var websocket_handler_error = function(error, data){
    console.log("Error using WebSocket", error, data);
};

var websocket_handler_session = function(jsonData){
    console.log(jsonData);
    if(jsonData.cookie
        && (m = /SID=([^ ,;]*)/.exec(jsonData.cookie)))
    {
        if (m[1].length<1 || m[1]=="0")
        {
            while (typeof(loginform)!="function");
            loginform();
        }
    }
    else
    {
        while (typeof(loginform)!="function");
        loginform();
    }
    session_id_set(jsonData.cookie);
};

// worker

var websocket_worker_error = function(err){
    console.log("There was a error in websocket worker: " + err);
    return -1;
};

var websocket_worker_func = function(){
    try {
        // there are still requests on stack
        // todo check if it does not work after X-tries
        if (websocket_global.readyState == websocket_global.OPEN && websocket_stack.length) {
            websocket_send(websocket_stack.pop());
            websocket_watcher_counter++;
        }
    } catch (e) {
        websocket_handler_error(e, "worker failed");
    }
};

var websocket_worker_start = function(interval){
    websocket_worker_interval = (!interval ? config.socket.workerdelay : interval);

    if (websocket_worker !== null) return -1;

    websocket_worker = window.setInterval(websocket_worker_func, websocket_worker_interval);
};

var websocket_worker_stop = function(){
    if (websocket_worker === null) return -1;
    window.clearInterval(websocket_worker);
    websocket_worker = null;
};

var websocket_worker_setInterval = function(interval){
    websocket_worker_stop();
    websocket_worker_start(interval);
};

// events
var websocket_onmessage = function(event){
    var jsonData = {};
    try {
        jsonData = JSON.parse(event.data);
    } catch (error) {
        console.log("Error parsing JSON", error);
    }

    if (jsonData.response) {
        switch (jsonData.response) {
            case "session":
                console.log(event.data);
                websocket_handler_session(jsonData);
                break;
            case "query":
                Database.onResponse(jsonData);
                break;
            case "fileupload":
            case "filedownload":
                serverfile.handleResponse(jsonData);
                break;
            case "error":
                websocket_handler_error(event, jsonData);
        }
    }
};

var websocket_onclose = function(event){
    websocket_removeEvents();
    websocket_worker_stop();
    Database.setOffline();
    websocket_handler_error(event);
};

var websocket_onopen = function(event){
    Database.setOnline();
    session_start();
    websocket_watcher_counter = 1;
};

var websocket_registerEvents = function(){
    websocket_helper_registerEvent(websocket_global, "message", websocket_onmessage);
    websocket_helper_registerEvent(websocket_global, "close", websocket_onclose);
    websocket_helper_registerEvent(websocket_global, "open", websocket_onopen);
    var i;
    for (i = 0; i < websocket_request_cache.length; i++) {
        websocket_helper_registerEvent(websocket_global, "open", websocket_request_cache[i]);
    }
};

var websocket_removeEvents = function(){
    websocket_helper_removeEvent(websocket_global, "message", websocket_onmessage);
    websocket_helper_removeEvent(websocket_global, "close", websocket_onclose);
    websocket_helper_removeEvent(websocket_global, "open", websocket_onopen);
    var i;
    for (i = 0; i < websocket_request_cache.length; i++) {
        websocket_helper_removeEvent(websocket_global, "open", websocket_request_cache[i]);
    }
};

// helper
var websocket_helper_registerEvent = function(element, event, callback){
    if (element.addEventListener) {
        element.addEventListener(event, callback, false);
    } else {
        element.attachEvent(event, callback);
    }
};

var websocket_helper_removeEvent = function(element, event, callback){
    if (element.removeEventListener) {
        element.removeEventListener(event, callback, false);
    } else {
        element.detachEvent(event, callback);
    }
};

var websocket_watcher_stop = function(){
    if (websocket_watcher !== null) {
        window.clearInterval(websocket_watcher);
        websocket_watcher = null;
    }
};

var websocket_watcher_start = function(interval){
    websocket_watcher = window.setInterval(function(){
        if (websocket_global !== null) {
            // closed unexpectly so reopen
            if ((websocket_global.readyState == websocket_global.CLOSED) && (Database.getStatus() == false)) {
                websocket_watcher_stop();
                websocket_watcher_counter++;
                if (websocket_watcher_counter > 10) {
                    websocket_watcher_counter = 1;
                }
                websocket_init();
            }
        }
    }, interval);
};

// main

var websocket_init = function(){
    try {
        websocket_global = new WebSocket(config.socket.url);
        websocket_registerEvents();
        websocket_watcher_start(websocket_watcher_interval * Math.pow(2, websocket_watcher_counter));
        websocket_worker_start();
    } catch (error) {
        console.log("Error creating WebSocket", error);
    }
};

var websocket_send = function(jsonData){
    var jsonString = "";

    try {
        jsonString = model_helper_stringify(jsonData);
    } catch (error) {
        console.log("Error stringify JSON", error);
    }

    console.log(jsonString);
    if (jsonString.length > 0) {
        if (websocket_global.readyState !== websocket_global.OPEN) {
            /*
             * var request = function() { try { websocket_helper_removeEvent(websocket_global, "open", this); var i; for (i = 0; i < websocket_request_cache.length; i++) { if
             * (websocket_request_cache[i] == this) { websocket_request_cache.splice(i, 1); break; } } websocket_global.send(jsonString); } catch (error) { websocket_handler_error(error, jsonData); //
             * TODO: retry } }; websocket_request_cache.push(request); websocket_helper_registerEvent(websocket_global, "open", request);
             */
            websocket_stack.push(jsonData);
        } else {
            try {
                return websocket_global.send(jsonString) && (websocket_worker_retrycount = 1);
            } catch (e) {
                websocket_stack.push(jsonData);
                websocket_worker_setInterval(++websocket_worker_retrycount * config.socket.workerdelay);
            }
        }
    }
};
