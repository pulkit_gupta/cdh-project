

///////////////////////////////////////////////////////////////////////////////
// we need an event stack for juggling requests, so they not cause lock 
// situations. 
//

var config = require('../conf.json');

var db;

try
{
	db = require('jugglingdb').Schema;
}
catch(e)
{
	db = require('C:/Program Files/nodejs/node_modules/jugglingdb').Schema;
}

var mysql = new db('mysql', {
  host:config.sql.host,
  username : config.sql.user,
  password : config.sql.password,
  database : config.sql.db,
  debug: false
});

var models = {
    "nav_settings": mysql.define(
        'cdh_nav_settings', {
          ident: Number,
          setting: String
        }
      ),
   'clipboard_personal' : mysql.define(
        'cdh_clipboard_personal', {
          ID: Number,
          tableName: String,
          value: Number,
          staffID: Number
        }
      ),
    'user' : mysql.define(
        'cdh_user' , {
          id : Number,
          password_hash: String,
          username : String,
          staffID : Number
        }
      ),
   'staff' : mysql.define(
        'cdh_staff' , {
          staffID : Number,
          name : String,
          telephone: String,
          fax: String,
          email: String
        }
      ),
    'clipboard_public' : mysql.define(
        'cdh_clipboard_public', {
          ID: Number,
          tableName: String,
          value: Number
        }
      ),
    'translation' : mysql.define(
        'cdh_translations', {
          ID : Number,
          language : String,
          module : String,
          placeholder_name: String,
          value_variant: String,
          value : String
        }
      ),
      'customer_main' : mysql.define(
  	        'cdh_customer_main' , {
  	        	id : Number,
  	        	customerID : Number,
  	        	companyname : String,
  	        	nameaffix: String,
  	        	adress: String,
  	        	zipcode: String,
  	        	city: String,
  	        	country: String,
  	        	telephone: String,
  	        	fax: String,
  	        	mobilephone: String,
  	        	email: String,
  	        	website: String,
  	        	comment: String,
  	        	category: String,
  	        	clienttyp: String,
  	        	maincontactID: Number,
  	        	logopath: String
  	        }
  	      )
};

exports.ready = function(){
    return mysql.connected;
};

exports.isModel = function(name){
    return typeof(models[name] !== "undefined");
};

exports.simpleQuery = function(what, transactionId, params, callback){
    models[what].all({where:params},function(e,d){
        callback(transactionId, d);
    }); 
};

//console.log(db,mysql,translations);


