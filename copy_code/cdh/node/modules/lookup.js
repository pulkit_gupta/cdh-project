
DaoRegistry["lookup_settings"] = function(params, callback)
{
    if (sw==true)
    {
	    console.log("params:"+JSON.stringify(params));
	    console.log("callback:"+callback);
	}
	
	if(params.session && (m = /SID=([^ ,;]*)/.exec(params.session)))
    {	    
	    query="FOR u in User \
            LET sess = ( FOR s in Sessions FILTER s.userkey==u._key FILTER s.sessID==\""+m[1]+"\" RETURN s) \
            FILTER sess!=[]\
            RETURN {'key':u._key}";
	    arangodb.query.exec(query, function (err, ret)
	    {
            if (ret.result.length!=1 || ret.result[0].key=="")
                return;
                                  
            query="FOR u in Lookup RETURN {'settings':u.settings}";
            console.log(query);
            arangodb.query.exec(query,function(err,ret)
            {
                console.log(ret);
	            for (i=0; i<ret.result.length; i++) {
		            ret.result[i].className = "LookupSettings";
	            }
	            callback({ "className": "ContentTree", "children": ret.result });
            });
        });
    }
}
