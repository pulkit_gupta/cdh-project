

///////////////////////////////////////////////////////////////////////////////
// Works only with mysql_new !!!! --> replace \node_modules\jugglingdb-mysql\lib\mysql.js with mysql_new.js

var config = require('../conf.json');

var db;

try
{
	db = require('jugglingdb').Schema;
}
catch(e)
{
	db = require('C:/Program Files/nodejs/node_modules/jugglingdb').Schema;
}

var mysql = new db('mysql', {
  host:config.sql.host,
  username : config.sql.user,
  password : config.sql.password,
  database : config.sql.db,
  debug: false
});

var masterdata_models = {
	      'masterdata' : mysql.define(
	    	        'cdh_customer' , {
	    	        	//cdh_customer
	    	        	customerID : Number,
	    	        	clientnumber : Number,
	    	        	//cdh_customer_main
	    	        	mainID : Number,
	    	        	clientnumber : Number,
	    	        	customerID : Number,
	    	        	telephone: String,
	    	        	fax: String,
	    	        	mobilephone: String,
	    	        	email: String,
	    	        	website: String,
	    	        	comment: String,
	    	        	categoryID: Number,
	    	        	clienttyp:String,
	    	        	maincontactID:Number,
	    	        	logopath: String,
	    	        	//cdh_customer_additional
	    	        	additionalID : Number,
	    	        	crmversion: String,
	    	        	syndicatemember: String,
	    	        	exhibitions: String,
	    	        	//cdh_customer_category
	    	        	categoryID : Number,
	    	        	categoryName : String,
	    	        	//cdh_customer_finance
	    	        	financeID : Number,
	    	        	paymentconditionID : Number,
	    	        	deliveryconditionID : Number,
	    	        	shippingtypID : Number,
	    	        	shippingnote : String,
	    	        	debitorennumber : Number,
	    	        	taxnumber : String,
	    	        	accountholder : String,
	    	        	accountnumber : String,
	    	        	blz : String,
	    	        	bank : String,
	    	        	delegate : String,
	    	        	catalogues : String,
	    	        	discount : String,
	    	        	discountnote : String,
	    	        	//cdh_delivery_condition
	    	        	deliveryconditionID : Number,
	    	        	deliverycondition : String,
	    	        	//cdh_payment_condition
	    	        	paymentconditionID : Number,
	    	        	paymentcondition : String,	
	    	        	//cdh_shipping_typ    	        	
	    	        	shippingtypID : Number,
	    	        	shippingtyp : String	
	    	        }
	    	      ),
    	      'masterdata_adress' : mysql.define(
	  	    	        	'cdh_customer_adress' , {
	  	    	        		customerID : Number,
	  	      	        	//cdh_customer_adress
	  		    	        	customerAdressID : Number,
	  		    	        	companyname: String,
	  		    	        	nameaffix: String,
	  		    	        	adress: String,
	  		    	        	zipcode: String,
	  		    	        	city: String,
	  		    	        	country: String,
	  		    	        	deliverynote: String,
	  		    	        	ismainadress: Boolean,
		  	    	        }
		  	    	      ),
    	      'masterdata_person' : mysql.define(
  	    	        	'cdh_customer_person' , {
  	    	        		//cdh_customer_person
  	    	        		personID : Number,
  	    	        		customerID : Number,
  	    	        		salutation : String,
  	    	        		salutationkey : Number,
  	    	        		titleID : Number,
  	    	        		surname : String,
  	    	        		lastname : String,
  	    	        		position : String,
  	    	        		telephone : String,
  	    	        		fax : String,
  	    	        		mobilphone : String,
  	    	        		email : String,
  	    	        		birthday : String,
  	    	        		bithdaymailing : Boolean,
  	    	        		notes : String,
  	    	        		//cdh_customer_title
  	    	        		titleID : Number,
  	    	        		titleKey : String,
  	    	        		titleName : String	
	  	    	        }
	  	    	      )
	};

exports.ready = function(){
    return mysql.connected;
};

exports.isModel = function(name){
    return typeof(masterdata_models[name] !== "undefined");
};

var data_masterdata = null;
var data_masterdata_person = null;
var data_masterdata_adress = null;


var getFinanceObject = function(data)
{
	var finance_data =
	{
		paymentcondition:data.paymentcondition,
		paymentconditionID:data.paymentconditionID,
		deliverycondition:data.deliverycondition,
		deliveryconditionID:data.deliveryconditionID,
		shippingtyp:data.shippingtyp,
		shippingtypID:data.shippingtypID,
		shippingnote:data.shippingnote,
		debitorennumber:data.debitorennumber,
		taxnumber:data.taxnumber,
		accountholder:data.accountholder,
		accountnumber:data.accountnumber,
		blz:data.blz,
		bank:data.bank,
		delegate:data.delegate,
		catalogues:data.catalogues,
		discount:data.discount,
		discountnote:data.discountnote
	};
	return finance_data;
};

var getPersonObject = function(data)
{
	var persons_data = 
				{	
					identifier:data.ID,
					salutation:data.salutation,
					salutationkey:data.salutationkey,
					titlekey:data.titleKey,
					title:data.titleName,
					surname:data.surname,
					lastname:data.lastname,
					position:data.position,
					telephone:data.telephone,
					fax:data.fax,
					mobilphone:data.mobilphone,
					email:data.email,
					birthday:data.birthday,
					bithdaymailing:data.bithdaymailing,
					notes:data.notes
				}
	return persons_data;
};

var getAdressObject = function(data)
{
	var adresses_data = {
		
	 			identifier:data.customerAdressID,
				companyname:data.companyname,
				nameaffix:data.nameaffix,
				adress:data.adress,
				zipcode:data.zipcode,
				city:data.city,
				country:data.country,
				deliverynote:data.deliverynote,
				ismainadress:data.ismainadress
	};
	return adresses_data;
};

var getMainObject = function(data, mainadress)
{
	var main_data = 
	{
			identifier:data.customerID,
			clientnumber:data.clientnumber,
			companyname:mainadress.companyname,
			nameaffix:mainadress.nameaffix,
			adress:mainadress.adress,
			zipcode:mainadress.zipcode,
			city:mainadress.city,
			country:mainadress.country,
			deliverynote:mainadress.deliverynote,	
			telephone:data.telephone,
			fax:data.fax,
			mobilephone:data.mobilephone,
			email:data.email,
			website:data.website,
			comment:data.comment,
			category:data.categoryID,
			categoryID:data.categoryName,
			clienttyp:data.clienttyp,
			maincontactID:data.maincontactID,
			logopath:data.logopath
	};
	return main_data;
};

var getAdditionalObject = function(data)
{
	var additional_data = 
	{	
		crmversion:data[0].crmversion,
		syndicatemember:data[0].syndicatemember,
		exhibitions:data[0].exhibitions
	};
	return additional_data;
};


var buildData_new = function(transactionId, callback)
{
	
	var new_data = new Array();
	
	for ( var i = 0; i < data_masterdata.length; i++) 
	{
		var customer_id = data_masterdata[i].customerID;
		
		var adress_array = new Array();
		var mainAdressID = 0;
		for ( var j = 0; j < data_masterdata_adress.length; j++) 
		{
			if(data_masterdata_adress[j].customerID == customer_id)
			{
				if(data_masterdata_adress[j].ismainadress)
				{
					mainAdressID = j;
				}
				adress_array.push(getAdressObject(data_masterdata_adress[j]));
			}
		}
		
		var person_array = new Array();
		for ( var j = 0; j < data_masterdata_person.length; j++) 
		{
			if(data_masterdata_person[j].customerID == customer_id)
			{
				person_array.push(getPersonObject(data_masterdata_person[j]));
			}
		}
		new_data.push(	
				{
					main: getMainObject(data_masterdata, data_masterdata_adress[j]),
					adresses: adress_array,
					finance: getFinanceObject(data_masterdata),
					contactpersons: person_array,
					additional: getAdditionalObject(data_masterdata)
				})	;
		
	}
	
	
	callback(transactionId, new_data);
}


var buildData = function(transactionId, callback)
{
	
	var adresses_data = new Array();
	var mainadress_id = 0;
	for ( var i = 0; i < data_masterdata_adress.length; i++) 
	{
		if(data_masterdata_adress[i].ismainadress == true)
		{
			mainadress_id = i;
		}
		adresses_data.push(
			{			
	 			identifier:data_masterdata_adress[i].customerAdressID,
				companyname:data_masterdata_adress[i].companyname,
				nameaffix:data_masterdata_adress[i].nameaffix,
				adress:data_masterdata_adress[i].adress,
				zipcode:data_masterdata_adress[i].zipcode,
				city:data_masterdata_adress[i].city,
				country:data_masterdata_adress[i].country,
				deliverynote:data_masterdata_adress[i].deliverynote									
			}
		);
	}
	
	var persons_data = new Array();
	for ( var i = 0; i < data_masterdata_person.length; i++) 
	{
		persons_data.push(
				{	
					identifier:data_masterdata_person[i].ID,
					salutation:data_masterdata_person[i].salutation,
					salutationkey:data_masterdata_person[i].salutationkey,
					titlekey:data_masterdata_person[i].titleKey,
					title:data_masterdata_person[i].titleName,
					surname:data_masterdata_person[i].surname,
					lastname:data_masterdata_person[i].lastname,
					position:data_masterdata_person[i].position,
					telephone:data_masterdata_person[i].telephone,
					fax:data_masterdata_person[i].fax,
					mobilphone:data_masterdata_person[i].mobilphone,
					email:data_masterdata_person[i].email,
					birthday:data_masterdata_person[i].birthday,
					bithdaymailing:data_masterdata_person[i].bithdaymailing,
					notes:data_masterdata_person[i].notes
				}
		);
	}
	var main_data = 
	{
			identifier:data_masterdata[0].customerID,
			clientnumber:data_masterdata[0].clientnumber,
			companyname:data_masterdata_adress[mainadress_id].companyname,
			nameaffix:data_masterdata_adress[mainadress_id].nameaffix,
			adress:data_masterdata_adress[mainadress_id].adress,
			zipcode:data_masterdata_adress[mainadress_id].zipcode,
			city:data_masterdata_adress[mainadress_id].city,
			country:data_masterdata_adress[mainadress_id].country,
			deliverynote:data_masterdata_adress[mainadress_id].deliverynote,	
			telephone:data_masterdata[0].telephone,
			fax:data_masterdata[0].fax,
			mobilephone:data_masterdata[0].mobilephone,
			email:data_masterdata[0].email,
			website:data_masterdata[0].website,
			comment:data_masterdata[0].comment,
			category:data_masterdata[0].categoryID,
			categoryID:data_masterdata[0].categoryName,
			clienttyp:data_masterdata[0].clienttyp,
			maincontactID:data_masterdata[0].maincontactID,
			logopath:data_masterdata[0].logopath
	};
	
	var additional_data = 
	{	
		crmversion:data_masterdata[0].crmversion,
		syndicatemember:data_masterdata[0].syndicatemember,
		exhibitions:data_masterdata[0].exhibitions
	};
	
	var finance_data =
	{
		paymentcondition:data_masterdata[0].paymentcondition,
		paymentconditionID:data_masterdata[0].paymentconditionID,
		deliverycondition:data_masterdata[0].deliverycondition,
		deliveryconditionID:data_masterdata[0].deliveryconditionID,
		shippingtyp:data_masterdata[0].shippingtyp,
		shippingtypID:data_masterdata[0].shippingtypID,
		shippingnote:data_masterdata[0].shippingnote,
		debitorennumber:data_masterdata[0].debitorennumber,
		taxnumber:data_masterdata[0].taxnumber,
		accountholder:data_masterdata[0].accountholder,
		accountnumber:data_masterdata[0].accountnumber,
		blz:data_masterdata[0].blz,
		bank:data_masterdata[0].bank,
		delegate:data_masterdata[0].delegate,
		catalogues:data_masterdata[0].catalogues,
		discount:data_masterdata[0].discount,
		discountnote:data_masterdata[0].discountnote
	};
	var newData = 
	{
		main: main_data,
		adresses: adresses_data,
		finance: finance_data,
		contactpersons: persons_data,
		additional: additional_data
	};
	
	callback(transactionId, newData);
}

  
exports.test = function(what, transactionId, params, callback)
{
	masterdata_models['masterdata'].all(
			{	
				where:params, 
				join:	"LEFT JOIN cdh_customer_main ON cdh_customer.customerID = cdh_customer_main.customerID "+  
						"LEFT JOIN cdh_customer_category ON cdh_customer_main.categoryID  = cdh_customer_category.categoryID "+ 
						"LEFT JOIN cdh_customer_adress ON cdh_customer.customerID = cdh_customer_adress.customerID "+ 
						"LEFT JOIN cdh_customer_person ON cdh_customer.customerID = cdh_customer_person.customerID "+ 
						"LEFT JOIN cdh_customer_title ON cdh_customer_person.titleID = cdh_customer_title.titleID "+ 
						"LEFT JOIN cdh_customer_additional ON cdh_customer.customerID = cdh_customer_additional.customerID "+ 
						"LEFT JOIN cdh_customer_finance ON cdh_customer.customerID = cdh_customer_finance.customerID "+ 
						"LEFT JOIN cdh_payment_condition ON cdh_customer_finance.paymentconditionID = cdh_payment_condition.paymentconditionID "+ 
						"LEFT JOIN cdh_delivery_condition ON cdh_customer_finance.deliveryconditionID = cdh_delivery_condition.deliveryconditionID "+ 
						"LEFT JOIN cdh_shipping_typ ON cdh_customer_finance.shippingtypID = cdh_shipping_typ.shippingtypID "
			},function(e,d)
			{
				console.log(d);
				callback(transactionId, d);
			});
}


exports.getMasterdata = function(what, transactionId, params, callback)
{
	masterdata_models['masterdata'].all(
			{	
				where:params, 
				join:	"LEFT JOIN cdh_customer_main ON cdh_customer.customerID = cdh_customer_main.customerID "+  
						"LEFT JOIN cdh_customer_category ON cdh_customer_main.categoryID  = cdh_customer_category.categoryID "+ 
						"LEFT JOIN cdh_customer_adress ON cdh_customer.customerID = cdh_customer_adress.customerID "+ 
						"LEFT JOIN cdh_customer_additional ON cdh_customer.customerID = cdh_customer_additional.customerID "+ 
						"LEFT JOIN cdh_customer_finance ON cdh_customer.customerID = cdh_customer_finance.customerID "+ 
						"LEFT JOIN cdh_payment_condition ON cdh_customer_finance.paymentconditionID = cdh_payment_condition.paymentconditionID "+ 
						"LEFT JOIN cdh_delivery_condition ON cdh_customer_finance.deliveryconditionID = cdh_delivery_condition.deliveryconditionID "+ 
						"LEFT JOIN cdh_shipping_typ ON cdh_customer_finance.shippingtypID = cdh_shipping_typ.shippingtypID "
			},function(e,d)
			{
				data_masterdata = d;
				masterdata_models['masterdata_person'].all(
				{	
					where:params, 
					join:	"LEFT JOIN cdh_customer_title ON cdh_customer_person.titleID = cdh_customer_title.titleID "
				},function(e2,d2)
				{
					data_masterdata_person = d2;
					masterdata_models['masterdata_adress'].all({where:params },function(e3,d3)
					{
						data_masterdata_adress = d3;
						buildData_new(transactionId, callback);
						
					});
				});
			});
}

//console.log(db,mysql,translations);


