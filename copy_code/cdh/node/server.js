var DEBUGMODE=process.argv.indexOf("-d")>-1;
var fs = require("fs");

// load config from conf.json (return if file not found or invalid)
try {
    config = require("./conf" + (DEBUGMODE ? ".debug" : "") + ".json");
} catch(e) { 
    Logger.error("conf.json not found or invalid: "+e.toString()); 
    process.exit(1);
}

Logger = require("./modules/logger.js");

// catch all uncought exeptions
process.on('uncaughtException', function(err) {
  Logger.error(err.toString());
  process.exit(1);
});

try { 
    var WebSocketServer = require(config.packages.ws).Server; 
} catch(e) { 
    Logger.error("package ws not found or invalid"); 
    process.exit(1);
}

try { 
    var UUID = require(config.packages.uuid); 
} catch(e) { 
    Logger.error("package uuid not found or invalid: "+e); 
    process.exit(1);
}


// setup session manager (return if package not found or invalid)
try { 
    var sess = require(config.packages.session);
} catch (e) {
    Logger.error("package session not found or invalid"); 
    process.exit(1);
}

// setup database (return if package not found or invalid)
try { 
    var arango = require(config.packages.arango); 
} catch(e) { 
    Logger.error("package arango not found or invalid");   
    process.exit(1);
}

/**
 * DaoRegistry (hashmap)
 * - list of query handlers
 * - query handlers must be registered as follows: DaoRegistry[queryName] 
 *   = queryHandlerFunction;
 * - queryHandleFunction takes two parameters: parameters (of query) and callback
 *   - parameters is a JSON object
 *   - callback takes the result of the query as parameter and must be called 
 *     when the query has been processed
 */
DaoRegistry = {};

// import dao modules (skip unavailable or invalid modules)
for (var i = 0; i < config.dao.length; i++) { 
    try { 
        require(config.dao[i]); 
    } catch(e) { 
        Logger.warn("dao module "+dao[i]+" not found or invalid"); 
    } 
}


/**
 * App (singleton)
 * - handles communication flow of server and provides application logic
 * - App.onConnection must be registered to the connection event of the websocket server
 *   - takes the websocket as parameter
 *   - will register App.onMessage to the message event of the websocket
 * - App.onMessage parses the received json message and will call the action handlers
 *   - exceptions thrown by action handlers or the json parser are catched and logged
 *   - server will continue to listen after such exceptions
 * - App.handleSession is an action handler and provides the session cookie
 * - App.handleQuery is an action handler and will check for registered query handlers in DaoRegistry and calls them
 */
var App = new function() {
    arangodb = new arango.Connection;
    arangodb.hostname = config.database.arango.hostname;
    arangodb.username = config.database.arango.username;
    arangodb.password = config.database.arango.password;
    arangodb.use();
    
    this.ws = null;
    
    this.send = function(jsonMsg) { this.ws.send(JSON.stringify(jsonMsg)); };
    
    this.onMessage = function(message) {
        try { var jsonMsg = JSON.parse(message); } catch(e) { Logger.warn("JSON could not be parsed, Original message: \n>>>\n",message,"\n<<<"); return; } Logger.debug("message received: \n>>>\n",message,"\n<<<"); Logger.debug("message parsed to: \n>>>\n",JSON.stringify(jsonMsg, null, 2),"\n<<<");
        if (jsonMsg.action) {
            Logger.debug("message has action: ",jsonMsg.action);
            try {
                switch (jsonMsg.action) {
                    case "getsession": this.handleSession(jsonMsg); break;
                    case "query": this.handleQuery(jsonMsg); break;
                    case "fileupload":this.handleUpload(jsonMsg); break;
                    case "filedownload":this.handleDownload(jsonMsg); break;
                }
            }
            catch(e) { Logger.warn("message could not be processed: \n>>>\n"+e+"\n",JSON.stringify(jsonMsg, null, 2),"\n<<<"); }
        }
    };
    
    this.onConnection = function(ws) { 
        Logger.debug("client connected"); 
        this.ws = ws; 
        var ctx = this; 
        this.ws.addListener("message", function(r) { ctx.onMessage(r); }); 
    };
    
    this.handleSession = function(jsonMsg) { 
        Logger.debug("session cookie: ",jsonMsg.cookie); 
        if (sw)
            console.log(jsonMsg);
            
        sess.lookupOrCreate({headers: {"cookie": jsonMsg.cookie}},0, function (sessionid, opts) {
              if (sessionid==0)
              {
                  // no session id, apparently no username/passwort. => no sessID to return;
                  session=new sess.Session(0, opts);
                  App.send({
                      "response": "session", 
                      "cookie": session.getSetCookieHeaderValue()
                  });
              }
              else
              {
                  if(sess.ownProp(sessions,sessionid))
                  {
                      // if the session exists, use it
                      session=sessions[sessionid];
                  }
                  else
                  {
                      // otherwise create a new session object with that ID, and store it
                      session=new sess.Session(sessionid,opts);
                      sessions[sessionid]=session;
                  }
               
                  // set the time at which the session can be reclaimed
                  session.expiration=(+new Date)+session.lifetime*1000
                  
                  // make sure a timeout is pending for the expired session reaper
                  if(!timeout)
                      timeout=setTimeout(sess.cleanup,60000)
                  
                  App.send({
                      "response": "session", 
                      "cookie": session.getSetCookieHeaderValue()
                  });
              }
            });
                
                
                /*this.send({
                    "response": "session", 
                    "cookie": session.getSetCookieHeaderValue()
                }); */
    };
    
    this.handleQuery = function(jsonMsg) { 
        if (jsonMsg.query && DaoRegistry[jsonMsg.query]) { 
            var ctx = this; 
            DaoRegistry[jsonMsg.query](
                jsonMsg.parameters, 
                function(result) {
                    ctx.send({
                        "response": "query", 
                        "transactionId": jsonMsg.transactionId, 
                        "result": result
                    }); 
                }
            ); 
        } else { 
            Logger.warn("query handler not registered for message: \n>>>\n",JSON.stringify(jsonMsg, null, 2),"\n<<<");
        }
    };
    
    /*
     * handles fileupload
     * creates a new file by uuid in config.uploaddir directory
     * please note: to deliver the file to the user, nginx needs to be setted up
     * to use the deliver files from data dir.
     * sth. like:
     * location /data/ {
     *  alias /path/to/uploaddir;
     *  autoindex off;
     * }
     */
    this.handleUpload = function(jsonMsg){
        var ctx=this;
        if (jsonMsg.filedata){
            var target = config.uploaddir + "/" + UUID.v1() + ".base64.txt";
            var transid=jsonMsg.transactionId;
            fs.stat(target, function(err,stats){
                // if the generated file name alrdy exists, try it again
                if (stats) {
                    this.handleUpload(jsonMsg);
                } else {
                    fs.writeFile(target,jsonMsg.filedata,function(err){
                        if (err) {
                            Logger.error(
                                "Error on writing file '" + target + "' " + err
                            );
                        }else{
                            ctx.send({
                                "response": "fileupload", 
                                "transactionId": transid, 
                                "result": target
                            });
                        }
                    });
                }
            });
        }
    };
    
    /*
     * handles requests for downloads from data dir.
     * THIS IS JUST A FALLBACK SOLUTION IF THE WEBSERVER IS NOT CONFIGURED
     * CORRECTLY!
     */
    this.handleDownload = function(jsonMsg){
        var ctx=this;
        var transid=jsonMsg.transactionId;
        // just allow downloads from uploaddir
        if (jsonMsg.url && jsonMsg.url.indexOf(config.uploaddir) == 0){
            fs.readFile(jsonMsg.url,'utf8', function (err, data) {
                if (err) {
                    Logger.error(err);
                    ctx.send({
                        "response":"filedownload",
                        "transactionId":transid,
                        "result":null,
                        "error":true,
                        "errormsg":(DEBUGMODE ? err.toString() : "File not found")
                    });
                } else {
                    ctx.send({
                        "response":"filedownload",
                        "transactionId":transid,
                        "result":data,
                        "error":false
                    });
                }
            });
        }
    };
}();

// setup websocket server (return if package ws not found or invalid or server port could not be opened)
try { 
    var wss = new WebSocketServer({host: config.server.host, port: config.server.port}); 
} catch(e) { 
    Logger.error("websocket server could not be started"); 
    process.exit(1);
}

wss.addListener('connection', function(ws) { App.onConnection(ws); });
