@echo off

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Install required modules
:: |_ WebSockets
:: |_ Arango.Client
:: |_ UUID
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

call npm -g install ws
call npm -g install https://github.com/kaerus/arango-client/archive/master.tar.gz
call npm -g install node-uuid

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Patch files
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

copy crypto.js.patch %~dp0bin\nodejs\node_modules\arango.client\lib\ext\crypto.js /y