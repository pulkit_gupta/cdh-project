' portable nginx + node.js install script
' based on portable node.js install script v1.0 by Dmitri Rubinstein, MIT License

Dim FSO, stderr, baseDir, binDir, cacheDir, Script, Args
Dim npmVersion, npmPackage, npmPackagePath, npmURL, npmVersionFile
Dim nodePackage, nodePackagePath, nodeURL, nodeExePath
Dim nginxVersion, nginxPackage, nginxPackagePath, nginxURL, nginxExePath, nginxVersionFile

Set FSO = CreateObject("Scripting.FileSystemObject")
Set stderr = FSO.GetStandardStream(2)
baseDir = FSO.GetParentFolderName(Wscript.ScriptFullName)
binDir = FSO.BuildPath(baseDir, "bin")
cacheDir = FSO.BuildPath(baseDir, "cache")
CreateFolderTree(binDir)
CreateFolderTree(cacheDir)

Set args = WScript.Arguments
If args.Named.Exists("npm-version") Then npmVersion = args.Named.Item("npm-version") End If
If args.Named.Exists("nginx-version") Then nginxVersion = args.Named.Item("nginx-version") End If
If FSO.FileExists(FSO.BuildPath(baseDir, "npm.latest")) Then
	Set npmVersionFile = FSO.OpenTextFile(FSO.BuildPath(baseDir, "npm.latest"), 1)
	npmVersion = npmVersionFile.readAll
	npmVersionFile.Close
Else
	If Not npmVersion Then Error "NPM Version not specified", 2 End If
End If
If FSO.FileExists(FSO.BuildPath(baseDir, "nginx.latest")) Then
	Set nginxVersionFile = FSO.OpenTextFile(FSO.BuildPath(baseDir, "nginx.latest"), 1)
	nginxVersion = nginxVersionFile.readAll
	nginxVersionFile.Close
Else
	If Not nginxVersion Then Error "NGINX Version not specified", 2 End If
End If

npmPackage = "npm-" & npmVersion & ".zip"
npmPackagePath = FSO.GetAbsolutePathName(FSO.BuildPath(cacheDir, npmPackage))
npmURL = "http://nodejs.org/dist/npm/" & npmPackage
nodePackage = "node.exe"
nodePackagePath = FSO.GetAbsolutePathName(FSO.BuildPath(cacheDir, nodePackage))
nodeURL = "http://nodejs.org/dist/latest/node.exe"
nodeExePath = FSO.BuildPath(binDir, "nodejs\node.exe")
nginxPackage = "nginx-" & nginxVersion & ".zip"
nginxPackagePath = FSO.GetAbsolutePathName(FSO.BuildPath(cacheDir, nginxPackage))
nginxURL = "http://nginx.org/download/" & nginxPackage
nginxExePath = FSO.BuildPath(binDir, "nginx\nginx.exe")

If Not Download(npmURL, npmPackagePath) Then Error "Could not download URL: " & npmURL, 2 End If
If Not Download(nodeURL, nodePackagePath) Then Error "Could not download URL: " & nodeURL, 2 End If
If Not Download(nginxURL, nginxPackagePath) Then Error "Could not download URL: " & nginxURL, 2 End If

If FSO.FolderExists(FSO.BuildPath(binDir, "nodejs")) Then FSO.DeleteFolder(FSO.BuildPath(binDir, "nodejs")) End If
CreateFolderTree(FSO.BuildPath(binDir, "nodejs"))
ExtractAll npmPackagePath, FSO.BuildPath(binDir, "nodejs")
If FSO.FileExists(FSO.BuildPath(cacheDir, npmPackage)) Then FSO.DeleteFile(FSO.BuildPath(cacheDir, npmPackage)) End If
FSO.CopyFile nodePackagePath, FSO.BuildPath(binDir, "nodejs") & "\"
If FSO.FileExists(FSO.BuildPath(cacheDir, nodePackage)) Then FSO.DeleteFile(FSO.BuildPath(cacheDir, nodePackage)) End If
If FSO.FolderExists(FSO.BuildPath(binDir, "nginx")) Then FSO.DeleteFolder(FSO.BuildPath(binDir, "nginx")) End If
ExtractAll nginxPackagePath, binDir
FSO.MoveFolder FSO.BuildPath(binDir, "\nginx-" & nginxVersion), FSO.BuildPath(binDir, "nginx")
If FSO.FileExists(FSO.BuildPath(cacheDir, nginxPackage)) Then FSO.DeleteFile(FSO.BuildPath(cacheDir, nginxPackage)) End If
If FSO.FolderExists(cacheDir) Then FSO.DeleteFolder(cacheDir) End If

Wscript.Echo "Installation finished"
Wscript.Quit

Function Download(url, path)
    Dim objHTTP
    Set objHTTP = CreateObject( "WinHttp.WinHttpRequest.5.1" )
    objHTTP.Open "GET", url, False
    objHTTP.Send
    If FSO.FileExists(path) Then FSO.DeleteFile(path) End If
    If objHTTP.Status = 200 Then
        Dim objStream
        Set objStream = CreateObject("ADODB.Stream")
        With objStream
            .Type = 1 'adTypeBinary
            .Open
            .Write objHTTP.ResponseBody
            .SaveToFile path
            .Close
        End With
        set objStream = Nothing
    Else
        Error "Could not download: " & url & " : " & objHTTP.Status & " " & objHTTP.StatusText, 2
    End If
    If FSO.FileExists(path) Then Download = True Else Download = False End If
End Function

Sub ExtractAll(strZipFile, strFolder)
	Dim objShell
	Set objShell = CreateObject("Shell.Application")
	If Not FSO.FolderExists(strFolder) Then FSO.CreateFolder(strFolder) End If
	intCount = objShell.NameSpace(strFolder).Items.Count
	Set colItems = objShell.NameSpace(strZipFile).Items
	objShell.NameSpace(strFolder).CopyHere colItems, 256
End Sub

Sub CreateFolderTree(folderPath)
    If Len(FSO.GetParentFolderName(folderPath)) > 0 And Not FSO.FolderExists(FSO.GetParentFolderName(folderPath)) Then CreateFolderTree(FSO.GetParentFolderName(folderPath)) End If
    If Not FSO.FolderExists(folderPath) Then FSO.CreateFolder(folderPath) End If
End Sub

Sub Error(msg, exitCode)
    If (UCase(Left(FSO.GetFileName(Wscript.FullName),7)) = "CSCRIPT") Then stderr.WriteLine "Setup Error: " & msg Else MsgBox msg, vbExclamation, "Setup Error" End If
	Wscript.Quit exitCode
End Sub