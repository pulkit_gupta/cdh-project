@echo off

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Stop server
:: - Will stop ALL node and nginx processes running on the system
:: - If node is still running, force it stop
:: - If node was started via cdh-start, previous step may also stop nginx
:: - If nginx is still running, try to stop it gracefully then forcefully
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

taskkill /F /IM node.exe 2> nul
call %~dp0nginx -s stop
timeout 3 > nul
taskkill /F /IM nginx.exe 2> nul