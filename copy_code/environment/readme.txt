*******************************************************************************
*
* CDH Server Development Edition
* Portable Runtime Environment for Windows
*
*******************************************************************************

Prerequisits
============

- does NOT require admin privileges and can be installed for ANY user

- can be installed or moved to ANY location, but should stay outside of SVN!

- paths with spaces should be avoided or substituted with virtual drive letters
  (use subst Y: "X:\path\with spaces" to assign drive letter Y to the path)

- if executables are present, but result in "path not found" or similar errors,
  virus scanner may need to be configured to allow access

- path to working copy of CDH repository must be specified in cdh-start.bat
  (line 7: set CDH_PATH={X:\path\to\repository})

Install server components from command line
===========================================

server-install [/npm-version:{npm-version}] [/nginx-version:{nginx-version}]

	This script will download:
	- latest node.exe and npm-x.y.z.zip from nodejs.org
	- as well as nginx-x.y.z.zip from nginx.org.
	Latest versions are specified in npm.latest and nginx.latest.
	Other versions of npm and nginx can be specified via parameters.

	Files will be extracted and moved into bin\nodejs and bin\nginx.

	Process may take a while. A message will occur after successful
	installation.

cdh-install

	Will install latest versions of all required modules into
	node_modules repository.

	Must be run before first start of node server.

Start server from command line
==============================

cdh-start

	Sets path to working copy of CDH repository.

	Creates or updates link to web content in bin\nginx\html.

	Starts nginx (decoupled from console).
	
	Runs node server script contained in CDH repository. Console output of
	node is passed to the same console as cdh-start is running in.

Stop server from command line
=============================

cdh-stop

	Preferred method to stop CDH server.
	
	Will stop node and nginx.

	Note: nginx may also manually be stopped via "nginx -s stop".
