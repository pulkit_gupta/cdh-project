#!/bin/bash

#################################################################################
## Install required modules
## |_ WebSockets
## |_ Arango.Client
## |_ UUID
#################################################################################

./npm -g install ws
./npm -g install https://github.com/kaerus/arango-client/archive/master.tar.gz
./npm -g install node-uuid

#################################################################################
## Patch files
#################################################################################

cp crypto.js.patch bin/nodejs/lib/node_modules/arango.client/lib/ext/crypto.js