#!/bin/bash

# dependencies: lsb_release, tar, wget, (make, gcc | grep, xxd), (ar | cpio, rpm2cpio)

# determine architecture
_machine=$(uname -m)
if [[ $_machine =~ "64" ]]
	then MACHINE="x86_64"
	else MACHINE="x86_32"
fi

# determine distribution
_sysdist=$(lsb_release -i -s)
SYSDIST=${_sysdist% *}
_sysmajver=$(lsb_release -r -s)
SYSMAJVER=${_sysmajver%%.*}
SYSCODENAME=$(lsb_release -c -s)

# option variables
opt_preserve_cache=0
opt_force_build_form_source=0

# check arguments
while getopts "a:c:d:hpsv:" flag
do
	case "$flag" in
		a)
			MACHINE=$OPTARG
		;;
		c)
			SYSCODENAME=$OPTARG
		;;
		d)
			SYSDIST=$OPTARG
		;;
		h)
			echo "Usage: server-install.sh [-a ARCH] [-c NAME] [-d ID] [-h] [-p] [-s] [-v VER]"
			echo "  -a assume machine architecture (e.g. x86_32)"
			echo "  -c assume distribution codename (e.g. wheezy)"
			echo "  -d assume distributor id (e.g. Debian)"
			echo "  -h show help"
			echo "  -p preserve cache directory"
			echo "  -s force build from source"
			echo "  -v assume distribution major version (e.g. 7)"
			exit 0
		;;
		p)
			opt_preserve_cache=1
		;;
		s)
			opt_force_build_from_source=1
		;;
		v)
			SYSMAJVER=$OPTARG
		;;
	esac
done

# clean up directories
if [ -d bin ]
	then rm -r bin
fi
mkdir bin
mkdir bin/nginx
if [[ $opt_preserve_cache -eq 0 ]]
	then
		if [ -d cache ]
			then rm -r cache
		fi
fi
if [ ! -d cache ]
	then mkdir cache
fi

# load package list
source packages.latest

# determine package url
pkgurl_varname="nginx_pkgurl_"$SYSDIST"_"$SYSMAJVER"_"$SYSCODENAME"_"$MACHINE
pkgurl=${!pkgurl_varname}
status_binary_package_installed=0

# install nginx
status_nginx_installed=0

# install deb
if [[ $pkgurl ]] && [[ $pkgurl =~ ".deb" ]] && [[ $opt_force_build_from_source -eq 0 ]]
	then
		# download package
		if [ ! -e cache/nginx.deb ]
			then
				echo Fetching binary package: $pkgurl
				wget $pkgurl --quiet --output-document=cache/nginx.deb
			else
				echo "Using existing binary package: cache/nginx.deb"
		fi
		# extract package
		if [ -e cache/nginx.deb ]
			then
				cd cache
				ar x nginx.deb
				cd ..
				if [ -e cache/data.tar.gz ]
					then
						tar -xzf cache/data.tar.gz -C bin/nginx
						status_binary_package_installed=1
				fi
		fi
fi

# install rpm
if [[ $pkgurl ]] && [[ $pkgurl =~ ".rpm" ]] && [[ $opt_force_build_from_source -eq 0 ]]
	then
		# download package
		if [ ! -e cache/nginx.rpm ]
			then
				echo Fetching binary package: $pkgurl
				wget $pkgurl --quiet --output-document=cache/nginx.rpm
			else
				echo "Using existing binary package: cache/nginx.rpm"
		fi
		# extract package
		if [ -e cache/nginx.rpm ]
			then
				cd bin/nginx
				rpm2cpio ../cache/nginx.rpm | cpio -idmvu --no-absolute-filename
				cd ../..
				status_binary_package_installed=1
		fi
fi

# install from source or patch binary
status_source_package_installed=0
if [[ $status_binary_package_installed -eq 0 ]]
	then
		if [ ! -e cache/nginx-src.tar.gz ]
			then
				echo Fetching source package: $nginx_pkgurl_src
				wget $nginx_pkgurl_src --quiet --output-document=cache/nginx-src.tar.gz
			else
				echo "Using existing source package: cache/nginx-src.tar.gz"
		fi
		if [ ! -e cache/pcre.tar.gz ]
			then
				echo Fetching dependency source package: $nginx_pkgurl_pcre
				wget $nginx_pkgurl_pcre --quiet --output-document=cache/pcre.tar.gz
			else
				echo "Using existing dependency source package: cache/pcre.tar.gz"
		fi
		if [ ! -d cache/nginxsrc ]
			then mkdir cache/nginxsrc
		fi
		tar -xzf cache/nginx-src.tar.gz -C cache/nginxsrc
		if [ ! -d cache/pcresrc ]
			then mkdir cache/pcresrc
		fi
		tar -xzf cache/pcre.tar.gz -C cache/pcresrc
		cd cache/pcresrc/pcre*
		pcredir=$PWD
		cd ../../..
		basedir=$PWD
		cd cache/nginxsrc/nginx*
		echo "Building nginx (this step may take several minutes depending on your system)..."
		./configure --prefix=$basedir/bin/nginx --sbin-path=usr/sbin/nginx --conf-path=etc/nginx/nginx.conf --http-log-path=var/log/nginx/access.log --pid-path=var/run/nginx.pid --lock-path=var/run/nginx.lock --error-log-path=var/log/nginx/errord.log --with-pcre=$pcredir &>>$basedir/cache/nginx-make.log
		if [[ $? -eq 0 ]]
			then
				make &>> $basedir/cache/nginx-make.log
				if [[ $? -eq 0 ]]
					then
						make install &>> $basedir/cache/nginx-make.log
						if [[ $? -eq 0 ]]
							then status_source_package_installed=1
						fi
				fi
		fi
		cd ../../..
	else
		# crude patch for nginx binary installations
		patchlocs=($(grep --text -o -z --byte-offset '/var/log/nginx/error.log' bin/nginx/usr/sbin/nginx))
		for ((i=0; i<${#patchlocs[*]}; i++)); do
			offset_ar=${patchlocs[i]}
			offset=${offset_ar%:*}
			echo "var/log/nginx/errord.log" | xxd -p -c 24 -l 24 - | xxd -r -seek $offset -p -c 24 -l 24 - bin/nginx/usr/sbin/nginx
		done
fi

# finalize nginx installation
if [[ $status_binary_package_installed -eq 1 ]] || [[ $status_source_package_installed -eq 1 ]]
	then
		# create cache directory
		if [ ! -d bin/nginx/var/cache ]
			then mkdir bin/nginx/var/cache
		fi
		if [ ! -d bin/nginx/var/cache/nginx ]
			then mkdir bin/nginx/var/cache/nginx
		fi
		# create pid file for nginx
		if [ ! -d bin/nginx/var/run ]
			then mkdir bin/nginx/var/run
		fi
		if [ ! -e bin/nginx/var/run/nginx.pid ]
			then echo "" > bin/nginx/var/run/nginx.pid
		fi
		# move html dir
		if [ -d bin/nginx/html ]
			then
				mkdir bin/nginx/usr/share
				mkdir bin/nginx/usr/share/nginx
				mv bin/nginx/html bin/nginx/usr/share/nginx
		fi
		# if root user change owner of extracted files to root
		if [[ $(id -u) -eq 0 ]]
			then
				chown -R root bin/nginx
				chgrp -R root bin/nginx
		fi
		# set status
		status_nginx_installed=1
	else
		echo "nginx could not be installed"
fi
	
# install node
status_node_installed=0
status_binary_package_installed=0
if [[ $opt_force_build_from_source -eq 0 ]]
	then
		if [[ $MACHINE =~ "64" ]]
			then pkgurl=$nodejs_pkgurl_linux_x86_64
			else pkgurl=$nodejs_pkgurl_linux_x86_32
		fi
		if [ ! -e cache/nodejs.tar.gz ]
			then
				echo Fetching binary package: $pkgurl
				wget $pkgurl --quiet --output-document=cache/nodejs.tar.gz
			else
				echo "Using existing binary package: cache/nodejs.tar.gz"
		fi
		if [ -e cache/nodejs.tar.gz ]
			then
				tar -xzf cache/nodejs.tar.gz -C bin
				if [ -d bin/nodejs ]
					then rm -r bin/nodejs
				fi
				mv bin/node-* bin/nodejs
				status_binary_package_installed=1
		fi
fi
status_source_package_installed=0
if [[ $opt_force_build_from_source -eq 1 ]] || [[ $status_binary_package_installed -eq 0 ]]
	then
		if [ ! -e cache/nodejs-src.tar.gz ]
			then
				echo Fetching source package: $nodejs_pkgurl_src
				wget $nodejs_pkgurl_src --quiet --output-document=cache/nodejs-src.tar.gz
			else
				echo "Using existing source package: cache/nodejs-src.tar.gz"
		fi
		if [ ! -d cache/nodejssrc ]
			then mkdir cache/nodejssrc
		fi
		tar -xzf cache/nodejs-src.tar.gz -C cache/nodejssrc
		basedir=$PWD
		cd cache/nodejssrc/node*
		echo "Building nodejs (this step may take several minutes depending on your system)..."
		./configure --prefix=$basedir/bin/nodejs &>>$basedir/cache/nodejs-make.log
		if [[ $? -eq 0 ]]
			then
				make &>> $basedir/cache/nodejs-make.log
				if [[ $? -eq 0 ]]
					then
						make install &>> $basedir/cache/nodejs-make.log
						if [[ $? -eq 0 ]]
							then status_source_package_installed=1
						fi
				fi
		fi
		cd ../../..
fi

# check if node installation was successful
if [[ $status_binary_package_installed -eq 0 ]] && [[ $status_source_package_installed -eq 0 ]]
	then
		echo "nodejs could not be installed"
	else
		# if root user change owner of extracted files to root
		if [[ $(id -u) -eq 0 ]]
			then
				chown -R root bin/nodejs
				chgrp -R root bin/nodejs
		fi
		# set status
		status_node_installed=1
fi

# remove cache dir
if [[ $opt_preserve_cache -eq 0 ]]
	then rm -r cache
fi

# tell if installation was successful
if [[ $status_nginx_installed -eq 1 ]] && [[ $status_node_installed -eq 1 ]]
	then echo Installation finished
	else echo Installation failed
fi